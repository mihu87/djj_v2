<?php
namespace ClassCover\CyoBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\ORM\EntityManager;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\SchoolBundle\Entity\School;
use FOS\UserBundle\Doctrine\UserManager;
use ClassCover\AppBundle\Entity\User;
use ClassCover\SchoolBundle\Entity\RegisteredTeachers;
use ClassCover\CyoBundle\Entity\Settings;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;
/**
 * Class TeacherService
 * @package ClassCover\CyoBundle\Services
 */
class TeacherService
{
    const DEFAULT_PASSWORD = '123456';
    const DEFAULT_ROLE_TEACHER = 'ROLE_TEACHER';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $firstName
     * @param $lastName
     * @param $email
     * @param $mobile
     * @return Teacher
     */
    public function createTeacher($firstName, $lastName, $email, $mobile)
    {
        /** @var PhoneNumberUtil $phoneUtil */
        $phoneUtil = $this->container->get('libphonenumber.phone_number_util');

        $pN = $phoneUtil->parse($mobile, "AU");
        $mobile = $phoneUtil->format($pN, PhoneNumberFormat::INTERNATIONAL);
        $mobile = str_replace(" ", "", $mobile);

        $doctrine = $this->container->get('doctrine');

        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->createUser();

        $user->setUsername(strtolower($firstName . '.' . $lastName));
        $user->setEmail($email);
        $user->setPlainPassword(self::DEFAULT_PASSWORD);
        $user->addRole(self::DEFAULT_ROLE_TEACHER);
        $user->setEnabled(true);

        $userManager->updateUser($user);

        $teacher = new Teacher();
        $teacher->setFirstName($firstName);
        $teacher->setSurname($lastName);
        $teacher->setEmail($email);
        $teacher->setPhone($mobile);
        $teacher->setUser($user);

        $em->persist($teacher);
        $em->flush();

        $settings = new Settings();
        $settings->setTeacher($teacher);
        $settings->setEmailAvailabilityReminder(0);
        $settings->setEmailBeforeBooking(0);
        $settings->setEmailBreakdown(0);

        $em->persist($settings);
        $em->flush();

        return $teacher;
    }

    /**
     * @param Teacher $teacher
     * @param School $school
     * @param int $rating
     * @return bool
     */
    public function registerToSchool(Teacher $teacher, School $school, $rating = 0)
    {
        $doctrine = $this->container->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        $displayOrder = $doctrine->getRepository('ClassCoverSchoolBundle:RegisteredTeachers')->getLastDisplayOrder($school);

        $pair = new RegisteredTeachers();
        $pair->setSchool($school);
        $pair->setTeacher($teacher);
        $pair->setDisplayOrder($displayOrder);

        $em->persist($pair);
        $em->flush();

        return true;
    }
}
