<?php

namespace ClassCover\CyoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThan;

use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\BookingBundle\Entity\TeacherUnavailability;
use ClassCover\BookingBundle\Entity\TeacherUnavailabilityRepository;
use ClassCover\BookingBundle\Entity\TeacherBooking;
use ClassCover\BookingBundle\Entity\TeacherBookingRepository;
use ClassCover\BookingBundle\Services\SmsBookingService;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class CalendarController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAvailabilityAction(Request $request)
    {
        $doctrine = $this->container->get('doctrine');
        $data = $request->request->all();

        $response = [
            'status' => 'success', 'errors' => [], 'data' => []
        ];

        $constraints = new Collection([
            'start' => [
                new NotBlank()
            ],
            'end' => [
                new NotBlank()
            ],
            'teacherId' => [
                new NotBlank(),
                new GreaterThan(0)
            ]
        ]);

        $data = $request->request->all();

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
            $response['status'] = 'error';

            return new JsonResponse($response, 200, ['Content-type', 'application/json']);
        }

        $dateFrom = new \DateTime($data['start']);
        $dateTo = new \DateTime($data['end']);

        /** @var Teacher $teacher */
        $teacher = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->find($data['teacherId']);

        if ($teacher == null) {
            $response['errors'][] = 'Cannot find teacher entity for id ' . $data['teacherId'];
            return JsonResponse::create($response);
        }

        /** @var TeacherUnavailabilityRepository $uaRepo */
        $uaRepo = $doctrine->getRepository('ClassCoverBookingBundle:TeacherUnavailability');
        $uaLines = $uaRepo->getByInterval($teacher, $dateFrom, $dateTo);

        /** @var TeacherBookingRepository $bookRepo */
        $bookRepo = $doctrine->getRepository('ClassCoverBookingBundle:TeacherBooking');
        $bookLines = $bookRepo->getByInterval($teacher, $dateFrom, $dateTo);


        /** @var TeacherUnavailability $slot */

        $periods = [];
        $bookingPeriods = [];

        $events = [];

        foreach ($uaLines as $slot) {
            $date = $slot->getDate();
            $time = $slot->getTime();
            $dateTime = $date->setTime($time->format('H'), $time->format('i'), $time->format('s'));
            $periods[$slot->getIntegrityId()][] = $dateTime;
        }

        foreach ($bookLines as $slot) {
            $date = $slot->getDate();
            $time = $slot->getTime();
            $dateTime = $date->setTime($time->format('H'), $time->format('i'), $time->format('s'));
            $bookingPeriods[$slot->getIntegrityId()][] = $dateTime;
        }

        foreach ($periods as $k => $period) {

            $start = current($period);
            $end = end($period);

            $sEnd = clone $end;
            $sEnd->modify("+1 hour");

            $events[] = [
                'id' => crc32($k),
                'title' => 'Unavailable',
                'start' => $start->format(\DateTime::ISO8601),
                'end' => $sEnd->format(\DateTime::ISO8601),
                'type' => 'ua',
                'canDelete' => true,
                'integrity' => $k,
                '_id' => $k
            ];
        }

        foreach ($bookingPeriods as $k => $period) {

            $start = current($period);
            $end = end($period);

            $sEnd = clone $end;
            $sEnd->modify("+1 hour");

            $events[] = [
                'id' => crc32($k),
                'title' => 'Booked',
                'start' => $start->format(\DateTime::ISO8601),
                'end' => $sEnd->format(\DateTime::ISO8601),
                'type' => 'booking',
                'canDelete' => false,
                'integrity' => $k,
                '_id' => $k
            ];
        }

        return new JsonResponse($events);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function setUnavailableAction(Request $request)
    {

        $doctrine = $this->container->get('doctrine');

        $data = $request->request->all();

        $response = [
            'status' => 'success', 'errors' => [], 'data' => []
        ];

        $constraints = new Collection([
            'start' => [
                new NotBlank()
            ],
            'end' => [
                new NotBlank()
            ],
            'teacherId' => [
                new NotBlank(),
                new GreaterThan(0)
            ]
        ]);

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
            $response['status'] = 'error';

            return JsonResponse::create($response);
        }

        $dateFrom = new \DateTime($data['start']);
        $dateTo = new \DateTime($data['end']);

        /** @var Teacher $teacher */
        $teacher = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->find($data['teacherId']);
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find(1);

        if ($teacher == null) {
            $response['errors'][] = 'Cannot find teacher entity for id ' . $data['teacherId'];
            return JsonResponse::create($response);
        }

        /** @var SmsBookingService $smsBookingService */
        $smsBookingService = $this->get('sms_booking_service');


        $integrity = $smsBookingService->setTeacherUnavailable($teacher, $school, $dateFrom, $dateTo);

        if ($integrity == false) {
            $response['status'] = 'error';
            $response['errors'] = 'Unable to set teacher unavailable. Check your service.';
            return JsonResponse::create($response);
        }

        $response['integrity'] = $integrity;

        return JsonResponse::create($response);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function setAvailableAction(Request $request)
    {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getEntityManager();

        $data = $request->request->all();

        $response = [
            'status' => 'success', 'errors' => [], 'data' => []
        ];

        $constraints = new Collection([
            'integrity' => [
                new NotBlank()
            ],
            'teacherId' => [
                new NotBlank(),
                new GreaterThan(0)
            ]
        ]);

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
            $response['status'] = 'error';

            return JsonResponse::create($response);
        }

        /** @var Teacher $teacher */
        $teacher = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->find($data['teacherId']);

        if ($teacher == null) {
            $response['errors'][] = 'Cannot find teacher entity for id ' . $data['teacherId'];
            return JsonResponse::create($response);
        }

        /** @var TeacherUnavailabilityRepository $ua */
        $ua = $doctrine
            ->getRepository('ClassCoverBookingBundle:TeacherUnavailability')
            ->findBy(['integrityId' => $data['integrity']]);

        foreach ($ua as $line) {
            $em->remove($line);
            $em->flush();
        }

        return JsonResponse::create($response);
    }
    
}
