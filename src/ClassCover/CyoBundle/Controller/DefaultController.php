<?php

namespace ClassCover\CyoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\Form\Form;
use FOS\UserBundle\Model\User;
use ClassCover\AdminBundle\Entity\Announcement;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ClassCoverCyoBundle:Default:index.html.twig');
    }

    public function userAction(Request $request)
    {
        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        $formFactory = $this->get('fos_user.change_password.form.factory');

        /** @var Form $form */
        $form = $formFactory->createForm();
        $form->setData($user);
        $form->remove('current_password');


        $form->handleRequest($request);

        if ($form->isValid()) {
            $userManager->updateUser($user);
            return $this->redirect($this->generateUrl('class_cover_teacher_user_settings'));
        }

        return $this->render('ClassCoverCyoBundle:Default:password.html.twig', ['form' => $form->createView()]);
    }

    public function getAnnouncementsAction()
    {
        $doctrine = $this->container->get('doctrine');

        $announcements = $doctrine->getRepository('ClassCoverAdminBundle:Announcement')->findBy(['type' => ['teacher', 'all']]);

        $response = ['data' => false];
        /** @var Announcement $last */
        $last = false;
        if (count($announcements)) {
            $last = end($announcements);

            $now = new \DateTime();
            $now = $now->getTimestamp();

            if ($now >= $last->getDateTimeStart()->getTimestamp() && $now <= $last->getDateTimeEnd()->getTimestamp()) {
                $response['data']['heading'] = $last->getHeadLine();
                $response['data']['content'] = $last->getSmsText();
            }
        }

        return JsonResponse::create($response);
    }

}
