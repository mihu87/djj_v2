<?php

namespace ClassCover\CyoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ClassCover\CyoBundle\Entity\Settings;
use ClassCover\CyoBundle\Form\SettingsType;

/**
 * Settings controller.
 *
 */
class SettingsController extends Controller
{

    /**
     * Lists all Settings entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClassCoverCyoBundle:Settings')->findAll();

        return $this->render('ClassCoverCyoBundle:Settings:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Settings entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Settings();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('teacher_settings_show', array('id' => $entity->getId())));
        }

        return $this->render('ClassCoverCyoBundle:Settings:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Settings entity.
     *
     * @param Settings $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Settings $entity)
    {
        $form = $this->createForm(new SettingsType(), $entity, array(
            'action' => $this->generateUrl('teacher_settings_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Settings entity.
     *
     */
    public function newAction()
    {
        $entity = new Settings();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClassCoverCyoBundle:Settings:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Settings entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClassCoverCyoBundle:Settings')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClassCoverCyoBundle:Settings:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Settings entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $teacherId = $this->get('session')->get('teacher')->getId();
        $entity = $em->getRepository('ClassCoverCyoBundle:Settings')->findOneBy(['teacher' => $teacherId]);
        $id = $entity->getId();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClassCoverCyoBundle:Settings:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Settings entity.
    *
    * @param Settings $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Settings $entity)
    {
        $form = $this->createForm(new SettingsType(), $entity, array(
            'action' => $this->generateUrl('teacher_settings_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Settings entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $teacherId = $this->get('session')->get('teacher')->getId();
        $entity = $em->getRepository('ClassCoverCyoBundle:Settings')->findOneBy(['teacher' => $teacherId]);
        $id = $entity->getId();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('teacher_settings_edit', array('id' => $id)));
        }

        return $this->render('ClassCoverCyoBundle:Settings:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Settings entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClassCoverCyoBundle:Settings')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Settings entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('teacher_settings'));
    }

    /**
     * Creates a form to delete a Settings entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('teacher_settings_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
