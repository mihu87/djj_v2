ClassCover.module('TeacherHome', (function($){

    var CONFIG = {
        SCHOOL_TIMES : {
            "morning_start":"06:00",
            "morning_end":"14:00",
            "evening_start":"14:00",
            "evening_end":"22:00",
            "night_start":"22:00",
            "night_end":"06:00",
            "break_times":[]
        },
        BOOKINGS_JSON_REQUEST_CONFIG : {
            success  : teacherAvailabilityRequestSuccessHandler,
            url      : 'json/get-teacher-calendar',
            dataType : 'JSON',
            type     : 'POST'
        }
    };

    function teacherAvailabilityRequestSuccessHandler(response) {

    }

    function initCalendar() {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultView: 'agendaWeek',
            firstDay: 1,
            editable: false,
            selectable: true,
            selectHelper: true,
            slotDuration: '01:00:00',
            height: 'auto',
            lazyFetching: true,
            allDaySlot: false,
            timeFormat: 'H:mm',
            select: function(start, end) {

                var eventData = {
                    title: "Unavailable",
                    start: start,
                    end: end,
                    backgroundColor: '#D11919',
                    overlap: false,
                    editable: false,
                    canDelete: true,
                    allDay: false
                };
                addEventToCalendar(eventData, $('#calendar'));
            },
            events: function(start, end, timezone, callback) {
                $.ajax({
                    url: 'json/get-teacher-calendar',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        start: start.format(),
                        end: end.format(),
                        teacherId : window['teacherId']
                    },
                    success: function(events) {
                        callback(events);
                    }
                });
            },
            eventRender: function(event, element){
                if(event.canDelete) {
                    element.find(".fc-content").prepend( "<span class='close' style='float:right;margin-right:3px;'>&times;</span>" );
                    element.find(".close").click(function() {removeEvent(event, $('#calendar'))});
                }
                if(event.bookingId) {
                    element.addClass('click');
                }
            },
            eventColor: '#D11919',
            columnFormat: {
                month: 'ddd',
                week: 'ddd D/M',
                day: 'dddd D/M'
            }
        });
    }

    /**
     * isOverlapping
     * @param event
     * @param cal
     * @returns {boolean}
     */
    function isOverlapping(event, cal){
        var array = cal.fullCalendar('clientEvents');
        for(var i in array){
            if (event.id != array[i].id && event.end > array[i].start && event.start < array[i].end && event.overlap == false && array[i].overlap == false){
                return true;
            }
        }
        return false;
    }

    /**
     * addEventToCalendar
     * @param event
     * @param calendar
     */
    function addEventToCalendar(event, calendar) {

        if(!isOverlapping(event, calendar)) {

            var params = {};

            params.start = event.start.format("YYYY-MM-DD HH:mm:ss");
            params.end = event.end.format("YYYY-MM-DD HH:mm:ss");
            params.teacherId = window['teacherId'];

            $.ajax({
                url: 'json/set-unavailable',
                type: 'POST',
                dataType: 'JSON',
                data: params,
                success: function(response, status, xhr) {
                    if(response.status == "success") {
                        event.integrity = response.integrity;
                        event._id = response.integrity;
                        calendar.fullCalendar('renderEvent', event, false);
                    }
                }
            });
        }
        calendar.fullCalendar('unselect');
    }

    /**
     * removeEvent
     * @param event
     * @param calendar
     */
    function removeEvent(event, calendar) {

        var params = {};

        params.integrity = event.integrity;
        params.teacherId = window['teacherId'];
        $.ajax({
            url: 'json/set-available',
            type: 'POST',
            dataType: 'JSON',
            data: params,
            success: function(response, status, xhr) {
                if(response.status == "success") {
                    calendar.fullCalendar('removeEvents', event._id);
                }
            }
        });
    }


    /**
     * GoShift App Teacher Module
     * @returns {boolean}
     */
    function initModule() {
        initCalendar();
        return true;
    }

    /**
     * return facade
     */
    return {
        init : initModule
    }

}(jQuery)));