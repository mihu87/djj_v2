<?php

namespace ClassCover\CyoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Settings
 *
 * @ORM\Table(name="teacher_settings")
 * @ORM\Entity(repositoryClass="ClassCover\CyoBundle\Entity\SettingsRepository")
 */
class Settings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\CyoBundle\Entity\Teacher", inversedBy="school")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id", nullable=false)
     */
    protected $teacher;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={ "default"=false })
     */
    protected $emailBeforeBooking;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={ "default"=false })
     */
    protected $emailBreakdown;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={ "default"=false })
     */
    protected $emailAvailabilityReminder;
    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emailBeforeBooking
     *
     * @param boolean $emailBeforeBooking
     *
     * @return Settings
     */
    public function setEmailBeforeBooking($emailBeforeBooking)
    {
        $this->emailBeforeBooking = $emailBeforeBooking;

        return $this;
    }

    /**
     * Get emailBeforeBooking
     *
     * @return boolean
     */
    public function getEmailBeforeBooking()
    {
        return $this->emailBeforeBooking;
    }

    /**
     * Set emailBreakdown
     *
     * @param boolean $emailBreakdown
     *
     * @return Settings
     */
    public function setEmailBreakdown($emailBreakdown)
    {
        $this->emailBreakdown = $emailBreakdown;

        return $this;
    }

    /**
     * Get emailBreakdown
     *
     * @return boolean
     */
    public function getEmailBreakdown()
    {
        return $this->emailBreakdown;
    }

    /**
     * Set emailAvailabilityReminder
     *
     * @param boolean $emailAvailabilityReminder
     *
     * @return Settings
     */
    public function setEmailAvailabilityReminder($emailAvailabilityReminder)
    {
        $this->emailAvailabilityReminder = $emailAvailabilityReminder;

        return $this;
    }

    /**
     * Get emailAvailabilityReminder
     *
     * @return boolean
     */
    public function getEmailAvailabilityReminder()
    {
        return $this->emailAvailabilityReminder;
    }

    /**
     * Set teacher
     *
     * @param \ClassCover\CyoBundle\Entity\Teacher $teacher
     *
     * @return Settings
     */
    public function setTeacher(\ClassCover\CyoBundle\Entity\Teacher $teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \ClassCover\CyoBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }
}
