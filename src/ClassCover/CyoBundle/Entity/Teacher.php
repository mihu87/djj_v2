<?php

namespace ClassCover\CyoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Teacher
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\CyoBundle\Entity\TeacherRepository")
 */
class Teacher
{

    const TEACHER_GENDER_MALE = 1;
    const TEACHER_GENDER_FEMALE = 2;
    const TEACHER_GENDER_NONE = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $additionalEmail;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $photoFile;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressStreet;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressCity;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressState;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressCountry;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressPostCode;

    /**
     * @ORM\Column(type="float", nullable=true, options={ "default"=0 })
     */
    protected $latitude;

    /**
     * @ORM\Column(type="float", nullable=true, options={ "default"=0 })
     */
    protected $longitude;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $alternativePhone;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $gender;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $timezoneId;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={ "default"=NULL })
     */
    protected $createdOn;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\SchoolBundle\Entity\RegisteredTeachers", mappedBy="teacher")
     */
    protected $schools;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\TeacherUnavailability", mappedBy="teacher")
     */
    protected $unavailability;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\TeacherExclusion", mappedBy="teacher")
     */
    protected $exclusions;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\TeacherShiftOffer", mappedBy="teacher")
     */
    protected $offers;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\TeacherBooking", mappedBy="teacher")
     */
    protected $bookings;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={ "default"=false })
     */
    protected $disabled;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\CyoBundle\Entity\Settings", mappedBy="school")
     **/
    protected $settings;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->schools = new \Doctrine\Common\Collections\ArrayCollection();
        $this->unavailability = new \Doctrine\Common\Collections\ArrayCollection();
        $this->exclusions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->offers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bookings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->settings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Teacher
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Teacher
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Teacher
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set additionalEmail
     *
     * @param string $additionalEmail
     *
     * @return Teacher
     */
    public function setAdditionalEmail($additionalEmail)
    {
        $this->additionalEmail = $additionalEmail;

        return $this;
    }

    /**
     * Get additionalEmail
     *
     * @return string
     */
    public function getAdditionalEmail()
    {
        return $this->additionalEmail;
    }

    /**
     * Set photoFile
     *
     * @param string $photoFile
     *
     * @return Teacher
     */
    public function setPhotoFile($photoFile)
    {
        $this->photoFile = $photoFile;

        return $this;
    }

    /**
     * Get photoFile
     *
     * @return string
     */
    public function getPhotoFile()
    {
        return $this->photoFile;
    }

    /**
     * Set addressStreet
     *
     * @param string $addressStreet
     *
     * @return Teacher
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;

        return $this;
    }

    /**
     * Get addressStreet
     *
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     *
     * @return Teacher
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     *
     * @return Teacher
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;

        return $this;
    }

    /**
     * Get addressState
     *
     * @return string
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * Set addressCountry
     *
     * @param string $addressCountry
     *
     * @return Teacher
     */
    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;

        return $this;
    }

    /**
     * Get addressCountry
     *
     * @return string
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    /**
     * Set addressPostCode
     *
     * @param string $addressPostCode
     *
     * @return Teacher
     */
    public function setAddressPostCode($addressPostCode)
    {
        $this->addressPostCode = $addressPostCode;

        return $this;
    }

    /**
     * Get addressPostCode
     *
     * @return string
     */
    public function getAddressPostCode()
    {
        return $this->addressPostCode;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Teacher
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Teacher
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Teacher
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set alternativePhone
     *
     * @param string $alternativePhone
     *
     * @return Teacher
     */
    public function setAlternativePhone($alternativePhone)
    {
        $this->alternativePhone = $alternativePhone;

        return $this;
    }

    /**
     * Get alternativePhone
     *
     * @return string
     */
    public function getAlternativePhone()
    {
        return $this->alternativePhone;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     *
     * @return Teacher
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set timezoneId
     *
     * @param integer $timezoneId
     *
     * @return Teacher
     */
    public function setTimezoneId($timezoneId)
    {
        $this->timezoneId = $timezoneId;

        return $this;
    }

    /**
     * Get timezoneId
     *
     * @return integer
     */
    public function getTimezoneId()
    {
        return $this->timezoneId;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     *
     * @return Teacher
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     *
     * @return Teacher
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set user
     *
     * @param \ClassCover\AppBundle\Entity\User $user
     *
     * @return Teacher
     */
    public function setUser(\ClassCover\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ClassCover\AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add school
     *
     * @param \ClassCover\SchoolBundle\Entity\RegisteredTeachers $school
     *
     * @return Teacher
     */
    public function addSchool(\ClassCover\SchoolBundle\Entity\RegisteredTeachers $school)
    {
        $this->schools[] = $school;

        return $this;
    }

    /**
     * Remove school
     *
     * @param \ClassCover\SchoolBundle\Entity\RegisteredTeachers $school
     */
    public function removeSchool(\ClassCover\SchoolBundle\Entity\RegisteredTeachers $school)
    {
        $this->schools->removeElement($school);
    }

    /**
     * Get schools
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchools()
    {
        return $this->schools;
    }

    /**
     * Add unavailability
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherUnavailability $unavailability
     *
     * @return Teacher
     */
    public function addUnavailability(\ClassCover\BookingBundle\Entity\TeacherUnavailability $unavailability)
    {
        $this->unavailability[] = $unavailability;

        return $this;
    }

    /**
     * Remove unavailability
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherUnavailability $unavailability
     */
    public function removeUnavailability(\ClassCover\BookingBundle\Entity\TeacherUnavailability $unavailability)
    {
        $this->unavailability->removeElement($unavailability);
    }

    /**
     * Get unavailability
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUnavailability()
    {
        return $this->unavailability;
    }

    /**
     * Add exclusion
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherExclusion $exclusion
     *
     * @return Teacher
     */
    public function addExclusion(\ClassCover\BookingBundle\Entity\TeacherExclusion $exclusion)
    {
        $this->exclusions[] = $exclusion;

        return $this;
    }

    /**
     * Remove exclusion
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherExclusion $exclusion
     */
    public function removeExclusion(\ClassCover\BookingBundle\Entity\TeacherExclusion $exclusion)
    {
        $this->exclusions->removeElement($exclusion);
    }

    /**
     * Get exclusions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExclusions()
    {
        return $this->exclusions;
    }

    /**
     * Add offer
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherShiftOffer $offer
     *
     * @return Teacher
     */
    public function addOffer(\ClassCover\BookingBundle\Entity\TeacherShiftOffer $offer)
    {
        $this->offers[] = $offer;

        return $this;
    }

    /**
     * Remove offer
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherShiftOffer $offer
     */
    public function removeOffer(\ClassCover\BookingBundle\Entity\TeacherShiftOffer $offer)
    {
        $this->offers->removeElement($offer);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Add booking
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherBooking $booking
     *
     * @return Teacher
     */
    public function addBooking(\ClassCover\BookingBundle\Entity\TeacherBooking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherBooking $booking
     */
    public function removeBooking(\ClassCover\BookingBundle\Entity\TeacherBooking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * Add setting
     *
     * @param \ClassCover\CyoBundle\Entity\Settings $setting
     *
     * @return Teacher
     */
    public function addSetting(\ClassCover\CyoBundle\Entity\Settings $setting)
    {
        $this->settings[] = $setting;

        return $this;
    }

    /**
     * Remove setting
     *
     * @param \ClassCover\CyoBundle\Entity\Settings $setting
     */
    public function removeSetting(\ClassCover\CyoBundle\Entity\Settings $setting)
    {
        $this->settings->removeElement($setting);
    }

    /**
     * Get settings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSettings()
    {
        return $this->settings;
    }

    public function __toString()
    {
        return $this->firstName . ' ' . $this->surname;
    }
}
