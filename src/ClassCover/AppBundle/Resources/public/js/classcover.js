var ClassCover = (function(){

    var _modules = {};
    var persistModule = function persistModule(name, obj) {
        _modules[name] = obj;
        _modules[name].init();
    };

    var getModules = function() {
        return _modules;
    };

    var checkSchoolAnnouncements = function() {
        $.get(Routing.generate('class_cover_centre_check_announcements'), function(response) {
                if (response.data != false) {
                    var heading = response.data.heading;
                    var content = response.data.content;

                    $('#school-panel-announcement .panel-heading').html(heading);
                    $('#school-panel-announcement .panel-body').html("<p class='text-primary'>" + content + "</p>");
                    $('#school-panel-announcement').show();
                } else {
                    $('#school-panel-announcement').hide();
                }
        });
    };

    var checkTeacherAnnouncements = function() {
        $.get(Routing.generate('class_cover_teacher_check_announcements'), function(response) {
            if (response.data != false) {
                var heading = response.data.heading;
                var content = response.data.content;

                $('#teacher-panel-announcement .panel-heading').html(heading);
                $('#teacher-panel-announcement .panel-body').html("<p class='text-primary'>" + content + "</p>");
                $('#teacher-panel-announcement').show();
            } else {
                $('#teacher-panel-announcement').hide();
            }
        });
    };

    var initSchoolAnnouncementsChecker = function() {
        setInterval(checkSchoolAnnouncements, 5000);
    };

    var initTeacherAnnouncementsChecker = function() {
        setInterval(checkTeacherAnnouncements, 5000);
    };

    return {
        module : persistModule,
        modules : getModules,
        initSchoolAnnouncementsChecker : initSchoolAnnouncementsChecker,
        initTeacherAnnouncementsChecker : initTeacherAnnouncementsChecker
    }

})();