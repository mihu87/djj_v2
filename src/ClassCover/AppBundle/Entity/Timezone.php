<?php

namespace ClassCover\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Timezone
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\AppBundle\Entity\TimezoneRepository")
 */
class Timezone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $displayName;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $utcOffset;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $utcDstOffset;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Timezone
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return Timezone
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set utcOffset
     *
     * @param integer $utcOffset
     *
     * @return Timezone
     */
    public function setUtcOffset($utcOffset)
    {
        $this->utcOffset = $utcOffset;

        return $this;
    }

    /**
     * Get utcOffset
     *
     * @return integer
     */
    public function getUtcOffset()
    {
        return $this->utcOffset;
    }

    /**
     * Set utcDstOffset
     *
     * @param integer $utcDstOffset
     *
     * @return Timezone
     */
    public function setUtcDstOffset($utcDstOffset)
    {
        $this->utcDstOffset = $utcDstOffset;

        return $this;
    }

    /**
     * Get utcDstOffset
     *
     * @return integer
     */
    public function getUtcDstOffset()
    {
        return $this->utcDstOffset;
    }
}
