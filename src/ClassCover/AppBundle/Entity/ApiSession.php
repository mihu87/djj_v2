<?php

namespace ClassCover\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApiSession
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\AppBundle\Entity\ApiSessionRepository")
 */
class ApiSession {

    /**
     * @var string
     *
     * @ORM\Column(name="session_key", type="string")
     * @ORM\Id
     */
    private $sessionKey;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $expirationDate;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $userId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $userType;

    /**
     * @return string
     */
    public function getSessionKey()
    {
        return $this->sessionKey;
    }

    /**
     * @param string $sessionKey
     */
    public function setSessionKey($sessionKey)
    {
        $this->sessionKey = $sessionKey;
    }

    /**
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param \DateTime $expirationDate
     */
    public function setExpirationDate(\DateTime $expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }
}
