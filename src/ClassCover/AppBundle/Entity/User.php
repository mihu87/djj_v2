<?php

namespace ClassCover\AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{

    const USER_CC_TYPE_TEACHER = 0;
    const USER_CC_TYPE_SCHOOL  = 1;
    const USER_CC_TYPE_ADMIN   = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="smallint", nullable=true, options={ "default"=0 })
     */
    protected $type;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
