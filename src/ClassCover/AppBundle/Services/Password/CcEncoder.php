<?php

namespace ClassCover\AppBundle\Services\Password;

use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;

class CcEncoder extends BasePasswordEncoder
{
    public function encodePassword($raw, $salt)
    {
        return md5($raw);
    }

    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $this->comparePasswords($encoded, $this->encodePassword($raw, $salt));
    }
}