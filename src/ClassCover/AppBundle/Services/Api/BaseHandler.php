<?php

namespace ClassCover\AppBundle\Services\Api;

use ClassCover\AppBundle\Entity\ApiSession;
use ClassCover\AppBundle\Exceptions\Api\Exception;
use ClassCover\AppBundle\Exceptions\Api\IncorrectParameterTypeException;
use ClassCover\AppBundle\Exceptions\Api\MissingParameterException;
use ClassCover\AppBundle\Exceptions\Api\NoAccessException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zend\XmlRpc\AbstractValue;

abstract class BaseHandler {

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var ApiSession
     */
    protected $_session      = null;
    protected $_methodParams = array();
    protected $_user_type = "";

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->entityManager = $this->container->get('doctrine')->getEntityManager();
    }

    protected function checkParams($args) {
        $request_method = $args[0];
        $request_params = $args[1][0];
        if(isset($this->_methodParams[$request_method])) {
            $currentMethodParams = $this->_methodParams[$request_method];
            foreach ($currentMethodParams as $name => $prop) {
                if($prop[0] === true && !isset($request_params[$name])) {
                    throw new MissingParameterException($name);
                }
                if(isset($request_params[$name]) && !$this->checkParamType($request_params[$name], $prop[1])) {
                    throw new IncorrectParameterTypeException($name, $prop[1]);
                }

                if($name == "session_key") {
                    /** @var SessionService $service */
                    $service = $this->container->get("api_session_service");
                    $this->_session = $service->getSession($request_params[$name]);
                    if($this->_session->getUserType() != $this->_user_type) {
                        throw new NoAccessException($this->_user_type);
                    }
                }

            }
        }
    }

    protected function checkParamType($value, $type) {

        try {
            $val_type = AbstractValue::getXmlRpcTypeByValue($value);
            if($val_type == $type) {
                return true;
            }
        } catch(Exception $e) {

        }

        return false;
    }

    public function __call($name, $args) {
        if($name != "checkParams") {
            return;
        }
        call_user_func_array(array($this,$name),array($args));
    }

}
