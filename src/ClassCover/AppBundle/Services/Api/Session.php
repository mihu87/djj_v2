<?php

namespace ClassCover\AppBundle\Services\Api;

use Zend\XmlRpc\AbstractValue;
use Zend\XmlRpc\Value\Struct;
use Zend\XmlRpc\Server\Exception as ApiException;

class Session extends BaseHandler {

    protected $_methodParams = array(
        'startSession' => array(
            'email' => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'password' => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'remember' => array(false, AbstractValue::XMLRPC_TYPE_BOOLEAN),
        ),
    );

    /**
     * Start a new session and return the session information
     *
     * @param struct $params Array with the following keys: <b>email</b>, <b>password</b>, <i>remember</i>
     * @return struct|array Array with the following keys: <b>session_key</b>, <b>expiration_date</b>, <b>user_type</b>
     */
    public function startSession($params) {
        $remember = isset($params['remember']) ? $params['remember'] : false;

        /** @var SessionService $service */
        $service = $this->container->get("api_session_service");

        $session = $service->createSession($params['email'], $params['password'], $remember);
        $data = [
            'session_key'     => $session->getSessionKey(),
            'expiration_date' => $session->getExpirationDate()->format("Y-m-d H:i:s"),
            'user_type'       => $session->getUserType()
        ];

        return $data;
    }

}