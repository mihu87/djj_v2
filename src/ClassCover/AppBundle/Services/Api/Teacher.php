<?php

namespace ClassCover\AppBundle\Services\Api;

use ClassCover\AppBundle\Exceptions\Api\IncorrectParameterTypeException;
use ClassCover\AppBundle\Exceptions\Api\IncorrectParameterValueException;
use ClassCover\AppBundle\Exceptions\Api\MissingParameterException;
use ClassCover\CyoBundle\Entity\Teacher as TeacherEntity;
use ClassCover\SchoolBundle\Entity\School;
use ClassCover\SchoolBundle\Entity\RegisteredTeachers;

use Zend\XmlRpc\AbstractValue;
use Zend\XmlRpc\Value\Struct;

use FOS\UserBundle\Doctrine\UserManager;
use ClassCover\AppBundle\Entity\User;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;

class Teacher extends BaseHandler
{
    protected $_methodParams = array(
        'getProfile' => array(
            'session_key' => array(true, AbstractValue::XMLRPC_TYPE_STRING),
        ),
        'getTeachingLevels' => array(
            'session_key'           => array(true, AbstractValue::XMLRPC_TYPE_STRING),
        ),
        'getTeachingSubjects' => array(
            'session_key'           => array(true, AbstractValue::XMLRPC_TYPE_STRING),
        ),
        'getAvailability' => array(
            'session_key' => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'start_date'  => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'end_date'    => array(true, AbstractValue::XMLRPC_TYPE_STRING),
        ),
        'getRegisteredSchools' => array(
            'session_key' => array(true, AbstractValue::XMLRPC_TYPE_STRING),
        ),
        'setAvailability' => array(
            'session_key'           => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'dates'                 => array(true, AbstractValue::XMLRPC_TYPE_ARRAY),
            'mask'                  => array(true, AbstractValue::XMLRPC_TYPE_ARRAY),
        ),
        'saveProfile' => array(
            'session_key'           => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'first_name'            => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'surname'               => array(true, AbstractValue::XMLRPC_TYPE_STRING),
           // 'accreditation'         => array(false, AbstractValue::XMLRPC_TYPE_STRING),
            'address_street'        => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'address_city'          => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'address_state'         => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'address_postcode'      => array(true, AbstractValue::XMLRPC_TYPE_STRING),
           // 'address_country'       => array(true, AbstractValue::XMLRPC_TYPE_STRING),
           // 'latitude'              => array(false, AbstractValue::XMLRPC_TYPE_STRING),
           // 'longitude'             => array(false, AbstractValue::XMLRPC_TYPE_STRING),
            'contact_number'        => array(true, AbstractValue::XMLRPC_TYPE_STRING),
            'alternative_contact_number' => array(false, AbstractValue::XMLRPC_TYPE_STRING),
           // 'show_profile'          => array(false, AbstractValue::XMLRPC_TYPE_STRING),
            //'birthday'              => array(false, AbstractValue::XMLRPC_TYPE_STRING),
            //'experience'            => array(false, AbstractValue::XMLRPC_TYPE_STRING),
            //'documents'             => array(false, AbstractValue::XMLRPC_TYPE_ARRAY),
            //'levels'                => array(true, AbstractValue::XMLRPC_TYPE_ARRAY),
           // 'subjects'              => array(false, AbstractValue::XMLRPC_TYPE_ARRAY),
            //'reference'             => array(false, AbstractValue::XMLRPC_TYPE_ARRAY),
            //'removed_references'    => array(false, AbstractValue::XMLRPC_TYPE_STRING),
        )
    );

    protected $_user_type = "teacher";

    /**
     * Get the teacher's availability in a given interval
     *
     * @param struct $params Array with the following keys: <b>session_key</b>, <b>start_date</b>, <b>end_date</b>
     * @return struct Returns a list with the availability items in the given interval.
     * The availability item is an array with the following keys: <b>date</b>, <b>repeat_key</b>, <b>status_am</b>,
     * <b>status_pm</b>, <b>notes_am</b>, <b>notes_pm</b>, <b>show_notes</b>, <b>booked_am</b>, <b>booked_pm</b>, <b>readonly</b>
     * @throws IncorrectParameterTypeException
     */
    public function getAvailability($params)
    {
        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');

        $start_date = strtotime($params['start_date']);
        if($start_date === false) {
            throw new IncorrectParameterTypeException('start_date', "string");
        }
        $start_date = new \DateTime($params['start_date']);

        $end_date = strtotime($params['end_date']);
        if($end_date === false) {
            throw new IncorrectParameterTypeException('end_date', "string");
        }

        $end_date = new \DateTime($params['end_date']);


        /** @var User $user */
        $user = $userManager->findUserBy(['id' => $this->_session->getUserId()]);
        /** @var TeacherEntity $teacher */
        $teacher = $this->entityManager->getRepository("ClassCoverCyoBundle:Teacher")->findOneBy(['user' => $user]);

        $service = $this->container->get('teacher_availability_service');

        $ua = $service->getRepresentedTeacherUaDataByInterval($start_date, $end_date, $teacher);
        $books = $service->getRepresentedTeacherBookingDataByInterval($start_date, $end_date, $teacher);

        $data = [];

        $dx = clone $start_date;
        while($dx <= $end_date) {

            $qDate = $dx->format("Y-m-d");

            $item['status'] = '111111111111111111111111';
            $item['repeat_key'] = '';
            $item['readonly'] = false;
            $item['booking'] = '';
            $item['date'] = $qDate;

            foreach ($ua as $u) {
                if ($u['date'] == $qDate) {
                    if (!empty($u['indexes'])) {
                        $pieces = explode(",", $u['indexes']);
                        foreach($pieces as $k => $i) {
                            $item['status'][$i] = 0;
                        }
                    }
                }
            }

            foreach ($books as $b) {
                if ($b['date'] == $qDate) {
                    if (!empty($b['indexes'])) {
                        $pieces = explode(",", $b['indexes']);
                        $item['booking'] = [];
                        foreach($pieces as $k => $i) {
                            $item['status'][$i] = 0;
                            $item['booking'][$i] = [
                                'name' => $b['name'],
                                'id' => $b['id']
                            ];
                        }
                    }
                }
            }

            if ($item['status'] != '111111111111111111111111') {
                $data[] = $item;
            }

            $dx->modify("+1 day");
        }

        return $data;
    }

    /**
     * Get the teacher's profile data
     *
     * @param struct $params Array with the following keys: <b>session_key</b>
     * @return struct Returns an associtive array representing the teacher profile, with the following keys:<br />
     * <b>first_name</b> - string - the first named<br />
     * <b>surname</b> - string - surname<br />
     * <b>email</b> - string - email address<br />
     * <b>accreditation</b> - string - accreditation number<br />
     * <b>address_street</b> - string - address, street<br />
     * <b>address_city</b> - string - address, city<br />
     * <b>address_state</b> - string - address, state<br />
     * <b>address_postcode</b> - string - address, postcode<br />
     * <b>latitude</b> - string - address's latitude<br />
     * <b>longitude</b> - string - address's longitude<br />
     * <b>contact_number</b> - string - contact number<br />
     * <b>alternative_contact_number</b> - string - alternative contact number<br />
     * <b>show_profile</b> - string - show profile to schools that the teacher is not registered with<br />
     * <b>birthday</b> - string - birth day<br />
     * <b>experience</b> - string - experience description<br />
     * <b>documents</b> - array - teacher's uploaded documents<br />
     * <b>levels</b> - array - list of teaching levels (id values)<br />
     * <b>subjects</b> - array - list of teaching subjects (id values)<br />
     * <b>reference</b> - array - list of reference structures. The reference structure has the following keys: <b>id</b>, <b>name</b>, <b>contact</b>
     *
     */
    public function getProfile($params)
    {
        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserBy(['id' => $this->_session->getUserId()]);
        /** @var TeacherEntity $teacher */
        $teacher = $this->entityManager->getRepository("ClassCoverCyoBundle:Teacher")->findOneBy(['user' => $user]);
        /** @var PhoneNumberUtil $phoneUtil */
        $phoneUtil = $this->container->get('libphonenumber.phone_number_util');

        $nationalPhoneNo = '';
        $nationalAlternativePhoneNo = '';

        if (!empty($teacher->getPhone())) {
            try {
                $teacherPhoneNo = $phoneUtil->parse($teacher->getPhone(), "AU");
                $nationalPhoneNo = $phoneUtil->format($teacherPhoneNo, PhoneNumberFormat::NATIONAL);
                $nationalPhoneNo = str_replace(" ", "", $nationalPhoneNo);
            } catch(NumberParseException $e) {
                //
            }
        }

        if (!empty($teacher->getAlternativePhone())) {
            try {
                $teacherAlternativePhoneNo = $phoneUtil->parse($teacher->getAlternativePhone(), "AU");
                $nationalAlternativePhoneNo = $phoneUtil->format($teacherAlternativePhoneNo, PhoneNumberFormat::NATIONAL);
                $nationalAlternativePhoneNo = str_replace(" ", "", $nationalAlternativePhoneNo);
            } catch(NumberParseException $e) {
                //
            }

        }

        $result = array();
        $result['first_name'] = $teacher->getFirstName();
        $result['surname'] = $teacher->getSurname();
        $result['email'] = $teacher->getEmail();
        $result['accreditation'] = '';
        $result['address_street'] = $teacher->getAddressStreet();
        $result['address_city'] = $teacher->getAddressCity();
        $result['address_state'] = $teacher->getAddressState();
        $result['address_postcode'] = $teacher->getAddressPostCode();
        $result['address_country'] = $teacher->getAddressCountry();
        $result['latitude'] = $teacher->getLatitude();
        $result['longitude'] = $teacher->getLongitude();
        $result['contact_number'] = $nationalPhoneNo;
        $result['alternative_contact_number'] = $nationalAlternativePhoneNo;
        $result['show_profile'] = "yes";
        $result['birthday'] = '';
        $result['experience'] = '';
        $result['levels'] = [];
        $result['subjects'] = [];
        $result['reference'] = array();

        return $result;
    }

    /**
     * Get the list of the school's where the teacher is registered
     * @param struct $params Array with the following keys: <b>session_key</b>
     * @return struct An array with a list of school items. Each school item is an associative array
     * with the following keys: <b>id</b>, <b>name</b>, <b>contact</b>, <b>phone</b>, <b>unavailable</b>,
     * <b>has_upcomming_bookings</b>
     */
    public function getRegisteredSchools($params)
    {
        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserBy(['id' => $this->_session->getUserId()]);
        /** @var TeacherEntity $teacher */
        $teacher = $this->entityManager->getRepository("ClassCoverCyoBundle:Teacher")->findOneBy(['user' => $user]);

        $result = [];
        $parsedSchools = [];

        $registeredTeachers = $this->entityManager->getRepository("ClassCoverSchoolBundle:RegisteredTeachers")->findBy(['teacher' => $teacher]);

        if (!empty($registeredTeachers)) {
            /** @var RegisteredTeachers $regTeacher */
            foreach($registeredTeachers as $regTeacher) {
                if (!in_array($regTeacher->getSchool()->getId(), $parsedSchools)) {
                    $result[] = [
                        'id' => (string) $regTeacher->getSchool()->getId(),
                        'name' => $regTeacher->getSchool()->getName(),
                        'phone' => $regTeacher->getSchool()->getPhone()
                    ];
                }
            }
        }

        return $result;
    }

    public function setAvailability($params)
    {
        if (count($params['dates']) <= 0) {
            throw new IncorrectParameterValueException('dates', 'array');
        }
        foreach ($params['dates'] as $val) {
            if (strtotime($val) == false) {
                throw new IncorrectParameterValueException('dates', 'array of dates in yyyy-mm-dd format');
            }
        }
        if (count($params['mask']) <= 0) {
            throw new IncorrectParameterValueException('mask', 'array');
        }
        foreach ($params['mask'] as $val) {
            if (strlen($val) != 24 || !preg_match('/\A[_10]{24}\Z/sim', $val)) {
                throw new IncorrectParameterValueException('mask', 'array of mask containing 1, 0 or _');
            }
        }

        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserBy(['id' => $this->_session->getUserId()]);
        /** @var TeacherEntity $teacher */
        $teacher = $this->entityManager->getRepository("ClassCoverCyoBundle:Teacher")->findOneBy(['user' => $user]);

        $service = $this->container->get('teacher_availability_service');

        $result = true;

        foreach ($params['dates'] as $key => $date) {
            if (!$result) {
                break;
            }
            if (isset($params['mask'][$key])) {
                $mask = $params['mask'][$key];
                $result = $service->editRepresentedAvailability($date, $mask, $teacher);
            } else {
                $result = false;
            }
        }

        return $result;
    }

    public function saveProfile($params)
    {
        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserBy(['id' => $this->_session->getUserId()]);
        /** @var TeacherEntity $teacher */
        $teacher = $this->entityManager->getRepository("ClassCoverCyoBundle:Teacher")->findOneBy(['user' => $user]);

        /** @var PhoneNumberUtil $phoneUtil */
        $phoneUtil = $this->container->get('libphonenumber.phone_number_util');

        $internationalPhone = '';

        try {
            $pN = $phoneUtil->parse($params['contact_number'], "AU");
            $internationalPhone = $phoneUtil->format($pN, PhoneNumberFormat::INTERNATIONAL);
            $internationalPhone = str_replace(" ", "", $internationalPhone);
        } catch (NumberParseException $ex) {
            //
        }

        $internationalAlternativePhone = '';

        if (!empty($params['alternative_contact_number'])) {
            try {
                $paN = $phoneUtil->parse($params['alternative_contact_number'], "AU");
                $internationalAlternativePhone = $phoneUtil->format($paN, PhoneNumberFormat::INTERNATIONAL);
                $internationalAlternativePhone = str_replace(" ", "", $internationalAlternativePhone);
            } catch (NumberParseException $ex) {
                //
            }
        }

        $teacher->setFirstName($params['first_name']);
        $teacher->setSurname($params['surname']);
        $teacher->setAddressStreet($params['address_street']);
        $teacher->setAddressCity($params['address_city']);
        $teacher->setAddressState($params['address_state']);
        $teacher->setAddressPostCode($params['address_postcode']);
        $teacher->setAddressCountry('Australia');
        $teacher->setPhone($internationalPhone);
        $teacher->setAlternativePhone($internationalAlternativePhone);

        $this->entityManager->persist($teacher);
        $this->entityManager->flush();

        return true;
    }

    /**
     * Get the list of teaching levels
     *
     * @param struct $params Array with the following keys: <b>session_key</b>
     * @return struct Returns a list of teaching levels structures. Structure has the following keys:<br />
     * <b>id</b> - string - the id of the teaching level<br />
     * <b>name</b> - string - name of the teaching level
     */
    public function getTeachingLevels($params)
    {
        //todo : implement in the future if needed
       return [];
    }

    /**
     * Get the list of teaching subjects
     *
     * @param struct $params Array with the following keys: <b>session_key</b>
     * @return struct Returns a list of teaching subjects structures. Structure has the following keys:<br />
     * <b>id</b> - string - the id of the teaching subject<br />
     * <b>name</b> - string - name of the teaching subject<br />
     * <b>short_name</b> - string - abbreviation of teaching subject
     */
    public function getTeachingSubjects($params)
    {
        //todo : implement in the future if needed
        return [];
    }
}