<?php

namespace ClassCover\AppBundle\Services\Api;

use ClassCover\AppBundle\Entity\ApiSession;
use ClassCover\AppBundle\Entity\User;
use ClassCover\AppBundle\Exceptions\Api\InvalidSessionCredentialsException;
use ClassCover\AppBundle\Exceptions\Api\InvalidSessionKeyException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SessionService {
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->entityManager = $this->container->get('doctrine')->getEntityManager();
    }

    /**
     * @param $email
     * @param $password
     * @param bool|false $remember
     * @return ApiSession
     * @throws InvalidSessionCredentialsException
     */
    public function createSession($email, $password, $remember = false)
    {
        $token = new UsernamePasswordToken($email, $password, "main", array("ROLE_TEACHER"));

        try {
            $this->container->get('security.authentication.manager')->authenticate($token);
            /** @var $user User*/
            $user = $this->entityManager->getRepository("ClassCoverAppBundle:User")->findOneBy(array('email' => $email));
            $datetime = new \DateTime();
            if($remember) {
                $datetime->add(new \DateInterval("P365D"));
            } else {
                $datetime->add(new \DateInterval("P1D"));
            }
            $this->container->get("security.token_storage")->setToken($token);
            $session = new ApiSession();
            $session->setExpirationDate($datetime);
            $session->setUserId($user->getId());
            $session->setUserType("teacher");

            $unique_key = false;

            $key = "";

            while($unique_key == false) {
                $temp_key = sha1($user->getEmail().$user->getId().microtime(true).$_SERVER['REMOTE_ADDR'].rand(0, 1000));
                $result = $this->entityManager->getRepository("ClassCoverAppBundle:ApiSession")->find($temp_key);
                if(count($result) == 0) {
                    $key = $temp_key;
                    $unique_key = true;
                }
            }

            $session->setSessionKey($key);

            $this->entityManager->persist($session);
            $this->entityManager->flush($session);

            return $session;
        } catch(\Exception $e) {
            throw new InvalidSessionCredentialsException();
        }

    }

    /**
     * @param $sessionKey
     * @return null|object
     * @throws InvalidSessionKeyException
     */
    public function getSession($sessionKey)
    {
        $result = $this->entityManager->getRepository("ClassCoverAppBundle:ApiSession")->find($sessionKey);
        if($result) {
            return $result;
        } else {
            throw new InvalidSessionKeyException();
        }
    }

}