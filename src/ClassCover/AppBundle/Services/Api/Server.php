<?php

namespace ClassCover\AppBundle\Services\Api;

use ClassCover\AppBundle\Exceptions\Api\Exception;
use ClassCover\AppBundle\Exceptions\Api\Fault;
use \Zend\Server\Method;

class Server extends \Zend\XmlRpc\Server {
    /**
     * Dispatch method
     *
     * @param  Method\Definition $invokable
     * @param  array $params
     * @return mixed
     */
    protected function _dispatch(Method\Definition $invokable, array $params)
    {
        $callback = $invokable->getCallback();
        $type     = $callback->getType();

        if ('function' == $type) {
            $function = $callback->getFunction();
            return call_user_func_array($function, $params);
        }

        $class  = $callback->getClass();
        $method = $callback->getMethod();

        if ('static' == $type) {
            return call_user_func_array(array($class, $method), $params);
        }

        $object = $invokable->getObject();
        if (!is_object($object)) {
            $invokeArgs = $invokable->getInvokeArguments();
            if (!empty($invokeArgs)) {
                $reflection = new \ReflectionClass($class);
                $object     = $reflection->newInstanceArgs($invokeArgs);
            } else {
                $object = new $class;
            }
        }
        
        if($object instanceof BaseHandler) {
            call_user_func_array(array($object, 'checkParams'), array('method' => $method, 'params' => $params));
        }

        return call_user_func_array(array($object, $method), $params);
    }

    /**
     * Raise an xmlrpc server fault
     *
     * @param string|\Exception $fault
     * @param int $code
     * @return Fault
     */
    public function fault($fault = null, $code = 404)
    {
        if (!$fault instanceof \Exception) {
            $fault = (string) $fault;
            if (empty($fault)) {
                $fault = 'Unknown Error';
            }
            $fault = new \Exception($fault, $code);
        }

        return Fault::getInstance($fault);
    }
}