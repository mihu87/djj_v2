<?php
namespace ClassCover\AppBundle\Exceptions\Api;


class Fault extends \Zend\XmlRpc\Server\Fault {

    public function __construct(\Exception $e)
    {
        $found = false;
        foreach (array_keys(static::$faultExceptionClasses) as $class) {
            if ($e instanceof $class) {
                $found = true;
                break;
            }
        }

        if(!$found) {
            error_log($e);
        }

        parent::__construct($e);
    }

    /**
     * Return Fault instance
     *
     * @param Exception $e
     * @return Fault
     */
    public static function getInstance(\Exception $e)
    {
        return new self($e);
    }
}