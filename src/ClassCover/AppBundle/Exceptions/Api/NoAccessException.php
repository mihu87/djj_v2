<?php

namespace ClassCover\AppBundle\Exceptions\Api;

class NoAccessException extends Exception {

    public function __construct($namespace) {

        $this->code = parent::API_NO_ACCESS_TO_NAMESPACE;
        $this->message = "You do not have access to the ".$namespace." calls";
    }
}