<?php

namespace ClassCover\AppBundle\Exceptions\Api;

class InvalidSessionCredentialsException extends Exception {

    public function __construct() {

        $this->code = parent::SESSION_INVALID_SESSION_CREDENTIALS;
        $this->message = "Invalid session credentials";
    }
}