<?php

namespace ClassCover\AppBundle\Exceptions\Api;

class MissingParameterException extends Exception {

    public function __construct($missing_parameter) {

        parent::__construct();

        $this->code = parent::PARAMS_MISSING_PARAMETER;
        $this->message = "Missing parameter: ".$missing_parameter;
    }
}