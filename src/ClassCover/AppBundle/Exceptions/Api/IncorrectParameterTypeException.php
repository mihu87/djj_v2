<?php

namespace ClassCover\AppBundle\Exceptions\Api;

class IncorrectParameterTypeException extends Exception {

    public function __construct($parameter, $expected_type) {

        $this->code = parent::PARAMS_INCORRECT_PARAMETER_TYPE;
        $this->message = "Incorrect type for paramter ".$parameter.". Expecting ".$expected_type;
    }
}