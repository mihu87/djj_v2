<?php

namespace ClassCover\AppBundle\Exceptions\Api;

class IncorrectParameterValueException extends Exception {

    public function __construct($parameter, $expected_format) {

        $this->code = parent::PARAMS_INCORRECT_PARAMETER_VALUE;
        $this->message = "Incorrect value for paramter ".$parameter.". Expected value format ".$expected_format;
    }
}