<?php

namespace ClassCover\AppBundle\Exceptions\Api;

class InvalidSessionKeyException extends Exception {

    public function __construct() {

        $this->code = parent::SESSION_INVALID_SESSION_KEY;
        $this->message = "Invalid session key";
    }
}