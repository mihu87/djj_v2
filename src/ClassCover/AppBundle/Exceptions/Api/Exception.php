<?php

namespace ClassCover\AppBundle\Exceptions\Api;

use Zend\XmlRpc\Server\Exception\RuntimeException;

class Exception extends RuntimeException {

    const API_NO_ACCESS_TO_NAMESPACE           = 1000;
    
    const PARAMS_MISSING_PARAMETER             = 2000;
    const PARAMS_INCORRECT_PARAMETER_TYPE      = 2001;
    const PARAMS_INCORRECT_PARAMETER_VALUE     = 2002;
    
    const SESSION_INVALID_SESSION_CREDENTIALS  = 3000;
    const SESSION_INVALID_SESSION_KEY          = 3001;
}