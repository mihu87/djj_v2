<?php

namespace ClassCover\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use ClassCover\AppBundle\Services\Api\Server;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\SecurityExtraBundle\Annotation\Secure;
use FOS\UserBundle\Model\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use ClassCover\SchoolBundle\Entity\School;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\SchoolBundle\Entity\AdditionalUsers;
use Symfony\Component\HttpFoundation\Request;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use ClassCover\BookingBundle\Entity\TeacherUnavailability;
use ClassCover\BookingBundle\Entity\TeacherUnavailabilityRepository;
use Doctrine\ORM\EntityManager;
use ClassCover\AppBundle\Services\Api\Session as ApiSession;
use ClassCover\AppBundle\Services\Api\Teacher as ApiTeacher;
use Zend\XmlRpc\Server\Fault;

class DefaultController extends Controller
{

    /**
     * @Secure(roles="ROLE_ADMIN, ROLE_TEACHER, ROLE_SCHOOL, ROLE_SCHOOL_ADDITIONAL_USER")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Session $session */
        $session = $this->get('session');

        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $roles = $user->getRoles();

        if ($user->hasRole('ROLE_ADMIN')) {

            return $this->redirectToRoute('class_cover_admin_homepage');

        } elseif ($user->hasRole('ROLE_SCHOOL')) {
            /** @var School $school */
            $school = $em->getRepository('ClassCoverSchoolBundle:School')->findOneBy(['user' => $user->getId()]);

            if ($school !== null) {
                $session->set('school', $school);
                $session->set('loggedUserName', $school->getName());
            }

            return $this->redirectToRoute('class_cover_school_homepage');

        } elseif ($user->hasRole('ROLE_SCHOOL_ADDITIONAL_USER')) {
            /** @var AdditionalUsers $schoolUser */
            $schoolUser = $em->getRepository('ClassCoverSchoolBundle:AdditionalUsers')
                ->findOneBy(['user' => $user->getId()]);

            if ($schoolUser != null) {
                $name = $schoolUser->getFirstName() . ' ' .
                    $schoolUser->getSurname() . '@' .
                    $schoolUser->getSchool()->getShortName();

                $session->set('school', $schoolUser->getSchool());
                $session->set('loggedUserName', $name);
            }

            return $this->redirectToRoute('class_cover_school_homepage');

        } elseif($user->hasRole('ROLE_TEACHER')) {

            /** @var Teacher $teacher */
            $teacher = $em->getRepository('ClassCoverCyoBundle:Teacher')->findOneBy(['user' => $user->getId()]);

            if ($teacher !== null) {
                $session->set('teacher', $teacher);
                $session->set('loggedUserName', $teacher->getFirstName() . ' ' . $teacher->getSurname());
            }

            return $this->redirectToRoute('class_cover_teacher_homepage');

        }

        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function teacherAvailabilityEditAction(Request $request)
    {
        $logger = $this->get('logger');

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var \ClassCover\BookingBundle\Services\InterceptorService $interceptorService */
        $interceptorService = $this->get('requests_interceptor_service');
        $interceptorService->recordIncomingRequest($request->request->all(), $request->query->all(), $request->attributes->all(), 'none');

        $data = $request->request->all();

        $data = $request->request->all();
        $teacherEmail = strtolower($data['email']);
        $mask = $data['mask'];
        $date = $data['date'];

        /** @var TeacherRepository $teacherRepo */
        $teacherRepo = $em->getRepository('ClassCoverCyoBundle:Teacher');

        /** @var Teacher $teacher */
        $teacher = $teacherRepo->findOneBy(['email' => $teacherEmail]);

        /** @var TeacherRepository $schoolRepo */
        $schoolRepo = $em->getRepository('ClassCoverSchoolBundle:School');
        /** @var School $school */
        $school = $schoolRepo->find(1);

        $uaCellsAdd = [];
        $uaCellsRemove = [];

        /** @var TeacherUnavailabilityRepository $uaRepo */
        $uaRepo = $em->getRepository('ClassCoverBookingBundle:TeacherUnavailability');

        for ($i=0; $i<strlen($mask); $i++) {

            $dateTime = new \DateTime($date);
            $dateTime->setTime($i, 0, 0);

            if ($mask[$i] == "0") {
                $uaCellsAdd[] = $dateTime;
            } else if ($mask[$i] == "1") {
                $uaCellsRemove[] = $dateTime;
            }
        }

        $integrity = microtime() . $teacher->getId();

        foreach ($uaCellsAdd as $cell) {

            /** @var TeacherUnavailability $uaCell */
            $uaCell = new TeacherUnavailability();
            $uaCell->setDate($cell);
            $uaCell->setTime($cell);
            $uaCell->setSchool($school);
            $uaCell->setTeacher($teacher);
            $uaCell->setIntegrityId($integrity);
            $uaCell->setAdjacentInterval(0);

            $em->persist($uaCell);
            $em->flush();

           // $logger->info($cell->format("Y-m-d H:i:s"));
        }

        foreach ($uaCellsRemove as $cell) {

            $cells = $uaRepo->findBy(['teacher' => $teacher, 'date' => $cell, 'time' => $cell]);

            if ($cells == null) {
                continue;
            }

            foreach ($cells as $c) {
                $em->remove($c);
                $em->flush();
            }
        }

        return JsonResponse::create("DONE");
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function apiAction(Request $request) {
        Fault::attachFaultException('ClassCover\AppBundle\Exceptions\Api\Exception');
        $server = new Server();
        $server->setClass(new ApiSession($this->container), 'session');
        $server->setClass(new ApiTeacher($this->container), 'teacher');

        return new Response($server->handle($request));
    }

    public function authLoginAction()
    {
        return $this->redirectToRoute('fos_user_security_login');
    }
}
