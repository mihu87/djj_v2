<?php

namespace ClassCover\BookingBundle\Command;

use ClassCover\BookingBundle\Entity\TeacherOvertimeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class OvertimeCommand
 * @package ClassCover\BookingBundle\Command
 */
class OvertimeCommand extends ContainerAwareCommand
{

    private $referenceDateTime = "2015-06-29 00:00:00";
    private $timezone = "Australia/NSW";

    protected function configure()
    {
        $this->setName('overtime:reset')->setDescription('Reset overtime records.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        /** @var TeacherOvertimeRepository $repo */
        $repo = $em->getRepository('ClassCoverBookingBundle:TeacherOvertime');

        $roster = [
            '2015-07-26',
            '2015-08-23',
            '2015-09-20',
            '2015-10-18',
            '2015-11-15',
            '2015-12-13',
            '2016-01-10',
            '2016-02-07',
            '2016-03-06',
            '2016-04-03',
            '2016-05-01',
            '2016-05-29',
            '2016-06-26',
            '2016-07-24',
            '2016-08-21',
            '2016-09-18',
            '2016-10-16',
            '2016-11-13',
            '2016-12-11',
            '2017-01-08',
            '2017-02-05',
        ];

        $output->writeln("Overtime reset...");

        $today = new \DateTime();
        $strDay = $today->format("Y-m-d");
        $strTime = $today->format('H:i:s');

        if (in_array($strDay, $roster)) {
            if ($strTime == "23:59:59") {
                $output->writeln("Current day ({$strDay}) is in roster and it's midnight ({$strTime}). Voting reset overtime counters.");
                $repo->resetOvertimeAll();
            } else {
                $output->writeln("Current day ({$strDay}) is in roster period but passed midnight ({$strTime}). Voting to keep overtime counters.");
            }
        } else {
            $output->writeln("Current day ({$strDay}) it's not in the roster period. Voting to keep overtime counters.");
        }

        $em->flush();
    }

    protected function validDateTime(\DateTime $dateTime)
    {
        $reference = new \DateTime($this->referenceDateTime, new \DateTimeZone($this->timezone));

        $valid = false;
        while($reference <= $dateTime) {
            if($reference == $dateTime) {
                $valid = true;
                break;
            }

            $reference->add(new \DateInterval("P28D"));
        }
        return $valid;
    }

}