<?php

namespace ClassCover\BookingBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ClassCover\CyoBundle\Services\AdminService;

class CreateAdminCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('admin:create')
            ->setDescription('Creates administrator and associated user. Usage admin:create [firstName] [lastName] [adminEmail] [adminMobile].')
            ->addArgument(
                'firstName',
                InputArgument::REQUIRED,
                'Admin First Name. Eg: Peter'
            )
            ->addArgument(
                'lastName',
                InputArgument::REQUIRED,
                'Admin Last Name. Eg: Carpenter'
            )
            ->addArgument(
                'adminEmail',
                InputArgument::REQUIRED,
                'Admin Email. Eg: peter.carpenter@djj.nsw.gov.au.'
            )
            ->addArgument(
                'adminMobile',
                InputArgument::REQUIRED,
                'Admin Mobile Phone Number (International format). Eg: +61428419891.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var AdminService $adminService */
        $adminService = $this->getContainer()->get('admin_service');

        $result = $adminService->createAdmin(
            $input->getArgument('firstName'),
            $input->getArgument('lastName'),
            $input->getArgument('adminEmail'),
            $input->getArgument('adminMobile')
        );

        if ($result) {
            $output->writeln('Admin Created');
        }
    }
}