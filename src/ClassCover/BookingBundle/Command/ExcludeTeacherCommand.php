<?php

namespace ClassCover\BookingBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ClassCover\BookingBundle\Services\SmsBookingService;

class ExcludeTeacherCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('teacher:exclude')
            ->setDescription('Book a Teacher on a specified interval. Usage teacher:book [teacherEmail] [school] [start] [end].')
            ->addArgument(
                'teacherEmail',
                InputArgument::REQUIRED,
                'Teacher Email, Eg : peter.carpenter@djj.nsw.gov.au.'
            )
            ->addArgument(
                'school',
                InputArgument::REQUIRED,
                'School name, Eg : Reiby JJC. Use school full qualified name.'
            )
            ->addArgument(
                'start',
                InputArgument::REQUIRED,
                'Start Date, Eg : 2010-05-14 for full day or 2010-05-14 14:30:00 for specific time in a day.'
            )
            ->addArgument(
                'end',
                InputArgument::REQUIRED,
                'End Date, Eg : 2010-05-14 for full day or 2010-05-14 14:30:00 for specific time in a day.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var SmsBookingService $service */
        $service = $this->getContainer()->get('sms_booking_service');

        $args = $input->getArguments();
        unset($args['command']);

        $result = $service->setTeacherExcluded($args);

        if ($result->status == false) {
            $output->writeln($result->errors);
            exit(100);
        } else {
            $output->writeln($result->messages);
            exit(0);
        }
    }
}

