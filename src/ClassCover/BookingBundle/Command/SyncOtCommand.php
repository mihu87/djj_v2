<?php

namespace ClassCover\BookingBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use ClassCover\BookingBundle\Entity\TeacherUnavailabilityRepository;
use ClassCover\BookingBundle\Services\SmsBookingService;
/**
 * Class SyncOtCommand
 * @package ClassCover\BookingBundle\Command
 */
class SyncOtCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('ot:sync')->setDescription('Reset overtime records.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');

        /** @var SmsBookingService $service */
        $service = $container->get('sms_booking_service');

        $i = 0;

        do {
            $service->fillTeacherOvertimeTable();
            ++$i;
            sleep(1);
        } while($i<50);

    }

}