<?php

namespace ClassCover\BookingBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ClassCover\SchoolBundle\Services\SchoolService;

class CreateSchoolCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('school:create')
            ->setDescription('Creates a school and associated user. Usage school:create [name] [shortName] [schoolEmail] [schoolPhone].')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Teacher First Name. Eg: Peter'
            )
            ->addArgument(
                'shortName',
                InputArgument::REQUIRED,
                'Teacher Last Name. Eg: Carpenter'
            )
            ->addArgument(
                'schoolEmail',
                InputArgument::REQUIRED,
                'School Email. Eg: peter.carpenter@djj.nsw.gov.au.'
            )
            ->addArgument(
                'schoolPhone',
                InputArgument::REQUIRED,
                'School Mobile Phone Number (International format). Eg: +61428419891.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var SchoolService $schoolService */
        $schoolService = $this->getContainer()->get('school_service');

        $result = $schoolService->createSchool(
            $input->getArgument('name'),
            $input->getArgument('shortName'),
            $input->getArgument('schoolEmail'),
            $input->getArgument('schoolPhone')
        );

        if ($result) {
            $output->writeln('School Created');
        }
    }
}