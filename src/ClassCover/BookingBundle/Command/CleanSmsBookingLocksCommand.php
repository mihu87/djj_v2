<?php

namespace ClassCover\BookingBundle\Command;

use ClassCover\BookingBundle\Entity\Booking;
use ClassCover\BookingBundle\Services\SmsBookingService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\BookingBundle\Entity\SmsRequestList;
use ClassCover\BookingBundle\Entity\SmsRequestListRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
/**
 * Class CleanSmsBookingLocksCommand
 * @package ClassCover\BookingBundle\Command
 */
class CleanSmsBookingLocksCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('locks:clean')->setDescription('Clean Sms Booking Request Locks.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $service = $this->getContainer()->get('sms_booking_service');

        $requestsCount = count($service->getOpenRequests());

        if ($requestsCount > 0) {
            $output->writeln("There are bookings in progress, voting to prevent making any changes.");
        } else {
            $output->writeln("No bookings in progress, deleting lock files create withing 24 hours");

            $finder = new Finder();
            $finder->files()->in($this->getContainer()->getParameter('sms_lock_path'));

            /** @var SplFileInfo $file */
            foreach ($finder as $file) {
                $time = intval((time()-$file->getMTime())/3600);
                if ($time>=24) {
                    unlink($file->getRealPath());
                }
            }
        }
    }

}