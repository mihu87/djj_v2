<?php

namespace ClassCover\BookingBundle\Command;

use ClassCover\BookingBundle\Services\SmsBookingService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use Doctrine\ORM\EntityManager;
/**
 * Class SmsCommand
 * @package ClassCover\BookingBundle\Command
 */
class SmsInstantCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('sms:instant')->setDescription('Start SMS Sending.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $service = $this->getContainer()->get('sms_booking_service');

        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $teachers = $em->getRepository('ClassCoverCyoBundle:Teacher')->findAll();

        foreach ($teachers as $cyo) {
            //var_dump($cyo->getId());

            if ($cyo->getId() == 35) {
                continue;
            }

            $smsContent = "goShift was updated overnight with a number of enhancements. All passwords had to be refreshed, so please use password 123456 for the next time you login and then change it via Settings. Check your email for more info. Q’s? https://classcoverdjj.freshdesk.com or peter.carpenter@classcover.com.au. NO REPLY";

            $service->sendMessage($cyo->getPhone(), $smsContent);

            $output->write("sms sent to : " . $cyo->getFirstName() . "\n");
        }
    }

}