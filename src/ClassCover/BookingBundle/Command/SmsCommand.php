<?php

namespace ClassCover\BookingBundle\Command;

use ClassCover\BookingBundle\Services\SmsBookingService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class SmsCommand
 * @package ClassCover\BookingBundle\Command
 */
class SmsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('sms:sender')->setDescription('Start SMS Sender.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $time = date("Y-m-d H:i:00");
        $refTime = new \DateTime($time);
        $output->writeln("Parsing list on {$time}");
        $this->getContainer()->get('sms_booking_service')->processSendList($refTime);
    }

}