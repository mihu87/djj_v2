<?php

namespace ClassCover\BookingBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ClassCover\BookingBundle\Services\SmsBookingService;

use ClassCover\SchoolBundle\Entity\SchoolRepository;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use ClassCover\BookingBundle\Entity\TeacherBookingRepository;
use ClassCover\BookingBundle\Entity\BookingRepository;
use ClassCover\BookingBundle\Entity\SmsRequestList;

use ClassCover\BookingBundle\Entity\Booking;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\SchoolBundle\Entity\School;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\ConstraintViolation;

use Symfony\Component\Validator\Constraints\EqualTo;

class SetUnavailableTeacherCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('unavailable')
            ->setDescription('Set Teacher unavailable on a specified interval. Usage teacher:book [teacherEmail] [school] [start] [end].')
            ->addArgument(
                'teacherEmail',
                InputArgument::REQUIRED,
                'Teacher Email, Eg : peter.carpenter@djj.nsw.gov.au.'
            )
            ->addArgument(
                'for',
                InputArgument::REQUIRED,
                'for keyword'
            )
            ->addArgument(
                'schoolName',
                InputArgument::REQUIRED,
                'School name, Eg : Reiby JJC. Use school full qualified name.'
            )
            ->addArgument(
                'from',
                InputArgument::REQUIRED,
                'from keyword'
            )
            ->addArgument(
                'start',
                InputArgument::REQUIRED,
                'Start Date, Eg : 2010-05-14 for full day or 2010-05-14 14:30:00 for specific time in a day.'
            )
            ->addArgument(
                'to',
                InputArgument::REQUIRED,
                'to keyword'
            )
            ->addArgument(
                'end',
                InputArgument::REQUIRED,
                'End Date, Eg : 2010-05-14 for full day or 2010-05-14 14:30:00 for specific time in a day.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        /** @var SmsBookingService $service */
        $service = $this->getContainer()->get('sms_booking_service');

        $args = $input->getArguments();
        unset($args['command']);

        $errors = [];

        /** @var TeacherRepository $teacherRepo */
        $teacherRepo = $doctrine->getRepository('ClassCoverCyoBundle:Teacher');
        /** @var Teacher $teacher */
        $teacher = $teacherRepo->findOneBy(['email' => $args['teacherEmail']]);

        /** @var TeacherRepository $schoolRepo */
        $schoolRepo = $doctrine->getRepository('ClassCoverSchoolBundle:School');
        /** @var School $school */
        $school = $schoolRepo->findOneBy(['name' => $args['schoolName']]);

        if ($teacher == null) {
            $errors[] = 'Teacher not found for ' . $args['teacherEmail'];
        }

        if ($school == null) {
            $errors[] = 'School not found for ' . $args['schoolName'];
        }

        $constraints = new Collection([
            'teacherEmail' => [new NotBlank()],
            'schoolName' => [new NotBlank()],
            'start' => [new DateTime()],
            'end' => [new DateTime()],
            'for' => [new EqualTo(['value' => 'for'])],
            'from' => [new EqualTo(['value' => 'from'])],
            'to' => [new EqualTo(['value' => 'to'])],
        ]);

        /** @var ConstraintViolationListInterface $violations */
        $violations = $this->getContainer()->get('validator')->validateValue($args, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var ConstraintViolation $violation */
                $errors[] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
        }

        $dateFrom = new \DateTime($args['start']);
        $dateTo = new \DateTime($args['end']);

        if (count($errors)>0) {
            $output->writeln($errors);
            exit(1);
        }

        $set = $service->setTeacherUnavailable($teacher, $school, $dateFrom, $dateTo);

        $output->writeln('Guess it set now');

    }
}

