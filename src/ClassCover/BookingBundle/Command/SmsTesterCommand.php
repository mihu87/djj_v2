<?php

namespace ClassCover\BookingBundle\Command;

use ClassCover\BookingBundle\Services\SmsBookingService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class SmsTesterCommand.php
 * @package ClassCover\BookingBundle\Command
 */
class SmsTesterCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('test:sender')->setDescription('Start SMS Sender.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $time = "2015-12-18 17:50:00";
        $refTime = new \DateTime($time);
        $output->writeln("Parsing list on {$time}");
        $smsRequests = $this->getContainer()->get('sms_booking_service')->getSendListSpecificTime($refTime);

        foreach ($smsRequests as $k => $v) {
            var_dump($v->getId());
            var_dump($v->getSendTime()->format("Y-m-d H:i:s"));
         //   var_dump($v->getSendTime()->modify("+1 minutes")->format("Y-m-d H:i:s"));
        }
    }

}