<?php

namespace ClassCover\BookingBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class SyncBookingsCommand
 * @package ClassCover\BookingBundle\Command
 */
class SyncBookingsCommand extends ContainerAwareCommand
{


    protected function configure()
    {
        $this->setName('bookings:sync')->setDescription('Sync Bookings.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');

        $user = 'root';
        $password = 'Pasadena200';

        $dbh = new \PDO('mysql:dbname=djj;host=djj.c4b8lt04j5pr.ap-southeast-2.rds.amazonaws.com', $user, $password);
        $dbhCC = new \PDO('mysql:dbname=djj_cc;host=djj.c4b8lt04j5pr.ap-southeast-2.rds.amazonaws.com', $user, $password);

        $d1 = '2015-12-11';
        $d2 = '2015-12-11';

        $SQL = "select
            tb.booking_id,
            s.name,
            t.email,
            tb.date,
            b.date_time_start,
            b.date_time_end,
            b.shift_type,
            GROUP_CONCAT(HOUR(time)) as 'lines',
            '________________________' as 'mask'
            from
            teacher_booking tb JOIN booking b ON b.id = tb.booking_id JOIN school s ON s.id = tb.school_id JOIN teacher t ON t.id = tb.teacher_id
            where tb.adjacent_interval = 0 and b.booking_status = 'completed'
            GROUP BY tb.booking_id, tb.date;
        ";

        //and DATE(b.date_time_start) IN('{$d1}', '{$d2}')

        $query = $dbh->query($SQL);
        $results = $query->fetchAll(\PDO::FETCH_ASSOC);

        $books = [];

        foreach ($results as &$result) {

            $sqlSchool = "select su.school_id, su.user_id from school s JOIN school_users su ON su.school_id = s.id WHERE s.name = '" . $result['name'] . "';";
            $sqlTeacher = "select * from users where email = '" . $result['email'] . "';";

            $schoolResult = $dbhCC->query($sqlSchool);
            $teacherId = $dbhCC->query($sqlTeacher)->fetchColumn();

            $schoolId = $schoolResult->fetchColumn(0);
            $schoolUserId = $schoolResult->fetchColumn(1);

            if (!$schoolId || !$teacherId || !$schoolUserId) {
                continue;
            }

            $lines = explode(",", $result['lines']);

            foreach ($lines as $v) {
                $result['mask'][$v] = 1;
            }

            $timeFrom = new \DateTime($result['date_time_start']);
            $timeTo = new \DateTime($result['date_time_end']);

            $books[$result["booking_id"]]['school_id'] = (int) $schoolId;
            $books[$result["booking_id"]]['school_user_id'] = (int) $schoolUserId;
            $books[$result["booking_id"]]['teacher_id'] = (int) $teacherId;
            $books[$result["booking_id"]]['shift_type'] = $result['shift_type'];
            $books[$result["booking_id"]]['time_from'] = $timeFrom->format("H:i");
            $books[$result["booking_id"]]['time_to'] =  $timeTo->format("H:i");

            $books[$result["booking_id"]]['dates'][] = [
                'date' => $result['date'],
                'availability_mask' => $result['mask']
            ];
        }

        $dbhCC->exec("truncate table school_bookings");
        $dbhCC->exec("truncate table school_bookings_dates");
        //$dbhCC->exec("DELETE FROM teacher_availability");

        foreach ($books as $book) {

            $cBooking = "INSERT INTO school_bookings (school_id, school_user_id, teacher_id, shift_type, time_from, time_to, verbal_confirmation_received, school_absence_reason_id, leave_approved) VALUES" .
                " (" . $book['school_id'] . ", " . $book['school_user_id'] . ", " . $book['teacher_id'] . ", '" . $book['shift_type'] . "', '" . $book['time_from'] . "', '" . $book['time_to'] . "', 1,1,'yes')";

            $dbhCC->exec($cBooking);

            $id = $dbhCC->lastInsertId();

            foreach ($book['dates'] as $sbd) {

                $cBd = "INSERT INTO school_bookings_dates(school_booking_id, date, availability_mask) VALUES" .
                    " (" . $id . ", '" . $sbd['date'] . "', '" . $sbd['availability_mask'] . "');";

                $maskAv = $sbd['availability_mask'];

                $maskAv = str_replace("1", "0", $maskAv);
                $maskAv = str_replace("_", "1", $maskAv);

                $s = "SELECT * FROM teacher_availability WHERE user_id = '" . $book['teacher_id'] . "' AND date = '" . $sbd['date'] . "'";
                $q = $dbhCC->query($s);
                $rq = $q->fetchAll(\PDO::FETCH_ASSOC);

                if (!empty($rq)) {
                    foreach ($rq as $state) {
                        $maskAv = $maskAv & $state['status'];
                    }

                    $cbU = "UPDATE teacher_availability SET `status` = '" . $maskAv . "' WHERE user_id = '" . $book['teacher_id'] . "' AND date = '" . $sbd['date'] . "'";
                    $dbhCC->exec($cbU);
                } else {
                    $cBa = "INSERT INTO teacher_availability (`user_id`, `date`, `status`) VALUES (" . $book['teacher_id'] . ", '" . $sbd['date'] . "', '" . $maskAv . "');";
                    $dbhCC->exec($cBa);
                }

                $dbhCC->exec($cBd);

            }
        }
    }

}