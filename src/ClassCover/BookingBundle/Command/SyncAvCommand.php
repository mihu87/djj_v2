<?php

namespace ClassCover\BookingBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use ClassCover\BookingBundle\Entity\TeacherUnavailabilityRepository;
use Symfony\Component\Console\Input\InputOption;
/**
 * Class SyncAvCommand
 * @package ClassCover\BookingBundle\Command
 */
class SyncAvCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('av:sync')
            ->setDescription('Reset overtime records.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $teacherSubject = $this->getContainer()->get('doctrine')->getRepository("ClassCoverCyoBundle:Teacher")->find(17);

        $start_date = new \DateTime('2015-12-14');
        $end_date = new \DateTime('2015-12-21');

        $service = $this->getContainer()->get('teacher_availability_service');

        $ua = $service->getRepresentedTeacherUaDataByInterval($start_date, $end_date, $teacherSubject);
        $books = $service->getRepresentedTeacherBookingDataByInterval($start_date, $end_date, $teacherSubject);

        $data = [];

        $dx = clone $start_date;
        while($dx <= $end_date) {

            $qDate = $dx->format("Y-m-d");

            $item['status'] = '111111111111111111111111';
            $item['repeat_key'] = '';
            $item['readonly'] = false;
            $item['booking'] = '';

            foreach ($ua as $u) {
                if ($u['date'] == $qDate) {
                    if (!empty($u['indexes'])) {
                        $pieces = explode(",", $u['indexes']);
                        foreach($pieces as $k => $i) {
                            $item['status'][$i] = 0;
                        }
                    }
                }
            }

            foreach ($books as $b) {
                if ($b['date'] == $qDate) {
                    if (!empty($b['indexes'])) {
                        $pieces = explode(",", $b['indexes']);
                        $item['booking'] = [];
                        foreach($pieces as $k => $i) {
                            $item['status'][$i] = 0;
                            $item['booking'][$i] = [
                                'name' => $b['name'],
                                'id' => $b['id']
                            ];
                        }
                    }
                }
            }

            $data[] = $item;

            $dx->modify("+1 day");
        }

        var_dump($data);
    }
}