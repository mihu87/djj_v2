<?php

namespace ClassCover\BookingBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ClassCover\CyoBundle\Services\TeacherService;
use ClassCover\SchoolBundle\Entity\School;
use ClassCover\CyoBundle\Entity\Teacher;

class CreateTeacherCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('teacher:create')
            ->setDescription('Creates a teacher and associated user. Usage teacher:create [firstName] [lastName] [teacherEmail] [teacherMobile].')
            ->addArgument(
                'firstName',
                InputArgument::REQUIRED,
                'Teacher First Name. Eg: Peter'
            )
            ->addArgument(
                'lastName',
                InputArgument::REQUIRED,
                'Teacher Last Name. Eg: Carpenter'
            )
            ->addArgument(
                'teacherEmail',
                InputArgument::REQUIRED,
                'Teacher Email. Eg: peter.carpenter@djj.nsw.gov.au.'
            )
            ->addArgument(
                'teacherMobile',
                InputArgument::REQUIRED,
                'Teacher Mobile Phone Number (International format). Eg: +61428419891.'
            )
            ->addArgument(
                'schoolName',
                InputArgument::OPTIONAL,
                'If specified and school is found by name, teacher will be registered to that school.'
            )
            ->addArgument(
                'rating',
                InputArgument::OPTIONAL,
                'If specified it will add this rating for the school where teacher is registered.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var TeacherService $teacherService */
        $teacherService = $this->getContainer()->get('teacher_service');
        $doctrine = $this->getContainer()->get('doctrine');

        $result = $teacherService->createTeacher(
            $input->getArgument('firstName'),
            $input->getArgument('lastName'),
            $input->getArgument('teacherEmail'),
            $input->getArgument('teacherMobile')
        );

        if ($result == true && !empty($input->hasArgument('schoolName'))) {
            /** @var School $school */
            $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->findOneByName(trim($input->getArgument('schoolName')));
            /** @var Teacher $teacher */
            $teacher = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->findOneByEmail(trim($input->getArgument('teacherEmail')));

            if($school !== null && $teacher !== null) {

                $rating = 0;

                if ($input->hasArgument('rating')) {
                    $rating = $input->getArgument('rating');
                }
                $teacherService->registerToSchool($teacher, $school, $rating);
            }
        }

        if ($result) {
            $output->writeln('Teacher Created');
        }
    }
}