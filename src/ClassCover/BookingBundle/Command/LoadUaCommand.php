<?php

namespace ClassCover\BookingBundle\Command;

use ClassCover\BookingBundle\Entity\TeacherBooking;
use ClassCover\BookingBundle\Entity\TeacherOvertimeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use ClassCover\BookingBundle\Services\SmsBookingService;

use ClassCover\SchoolBundle\Entity\SchoolRepository;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use ClassCover\BookingBundle\Entity\TeacherBookingRepository;
use ClassCover\BookingBundle\Entity\BookingRepository;
use ClassCover\BookingBundle\Entity\SmsRequestList;

use ClassCover\BookingBundle\Entity\Booking;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\SchoolBundle\Entity\School;
use Symfony\Component\Console\Input\InputArgument;
use ClassCover\BookingBundle\Entity\TeacherUnavailability;
/**
 * Class LoadUaCommand
 * @package ClassCover\BookingBundle\Command
 */
class LoadUaCommand extends ContainerAwareCommand
{


    protected function configure()
    {
        $this->setName('ua:load')
            ->setDescription('Load Bulk UA.')
            ->addArgument(
                'offset',
                InputArgument::REQUIRED,
                '--'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $offset = $input->getArgument('offset');

        $container = $this->getContainer();
        $doctrine = $container->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        /** @var SmsBookingService $service */
        $service = $this->getContainer()->get('sms_booking_service');

        /** @var TeacherRepository $teacherRepo */
        $teacherRepo = $doctrine->getRepository('ClassCoverCyoBundle:Teacher');

        /** @var TeacherRepository $schoolRepo */
        $schoolRepo = $doctrine->getRepository('ClassCoverSchoolBundle:School');
        /** @var School $school */
        $school = $schoolRepo->find(1);

        //$seeds = array_slice($this->getSeed(), $offset, 400);
        $seeds = $this->getSeed();

        $avd = array();

        $uaDates = [];
        foreach ($seeds as $seed) {

            if ($seed['email'] == 'peter.carpenter@classcover.com.au') {
                continue;
            }

            for ($i=0; $i<strlen($seed['status']); $i++) {
                $timeFull = $i . ':00:00';
                if ($seed['status'][$i] == "0") {
                    $uaDates[$seed['email']][] = new \DateTime($seed['date'] . ' ' . $timeFull);
                }
            }
        }

        sleep(1);
        unset($seeds);

        $ic = 0;
        foreach($uaDates as $teacherEmail => $cells) {

            /** @var Teacher $teacher */
            $teacher = $teacherRepo->findOneBy(['email' => $teacherEmail]);

            if ($teacher == null) {
                continue;
            }

            $integrity_id = microtime() . $teacher->getId();
            foreach($cells as $cell) {

                /** @var TeacherUnavailability $bookingCell */
                $uaCell = new TeacherUnavailability();
                $uaCell->setDate($cell);
                $uaCell->setTime($cell);
                $uaCell->setSchool($school);
                $uaCell->setTeacher($teacher);
                $uaCell->setIntegrityId($integrity_id);
                $uaCell->setAdjacentInterval(0);
                $em->persist($uaCell);

                if ($ic % 50 == 0) {
                    $em->flush();
                }

                $output->writeln("Processing " . $teacher->getId() . ' ' . $cell->format("Y-m-d H:i:s"));
            }
        }

    }

    protected function getSeed($o = 0)
    {
        $UnknownTable = array(
            array( // row #0
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #1
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #2
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #3
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #4
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #5
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #6
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #7
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #8
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #9
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #10
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #11
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #12
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #13
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #14
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #15
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #16
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '111111110000000011111100',
            ),
            array( // row #17
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #18
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '111111111111111111000000',
            ),
            array( // row #19
                'email' => 'alexander.soloviev@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #20
                'email' => 'ronald.chittick@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #21
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #22
                'email' => 'darren.james@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #23
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000111',
            ),
            array( // row #24
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000001111111100',
            ),
            array( // row #25
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '111111001111110000000011',
            ),
            array( // row #26
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '111111100000000001111111',
            ),
            array( // row #27
                'email' => 'karen.dooms@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000111111111111110',
            ),
            array( // row #28
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000011111111111111100',
            ),
            array( // row #29
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000111100000000100',
            ),
            array( // row #30
                'email' => 'Andrea.Ramirez@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #31
                'email' => 'judith.martin@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '111111111111110000000011',
            ),
            array( // row #32
                'email' => 'jason.kerkhof@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #33
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '111111111000000001111111',
            ),
            array( // row #34
                'email' => 'shawn.mckinley@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '111111110000000011111111',
            ),
            array( // row #35
                'email' => 'Rikky.Messer@djj.nsw.gov.au',
                'date' => '2015-11-17',
                'status' => '111111000000001111111111',
            ),
            array( // row #36
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #37
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #38
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #39
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #40
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #41
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #42
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #43
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #44
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #45
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #46
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #47
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #48
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #49
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #50
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #51
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #52
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '111111000000000000000011',
            ),
            array( // row #53
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #54
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #55
                'email' => 'alexander.soloviev@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #56
                'email' => 'ronald.chittick@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #57
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #58
                'email' => 'darren.james@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #59
                'email' => 'peter.jensen@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #60
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '111111000000000000000111',
            ),
            array( // row #61
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000111111111111111100',
            ),
            array( // row #62
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '111111001111111111111111',
            ),
            array( // row #63
                'email' => 'karen.dooms@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '111111111111111111111110',
            ),
            array( // row #64
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000111111111111111100',
            ),
            array( // row #65
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000001111111111111100',
            ),
            array( // row #66
                'email' => 'Andrea.Ramirez@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #67
                'email' => 'jason.kerkhof@djj.nsw.gov.au',
                'date' => '2015-11-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #68
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #69
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #70
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #71
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #72
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #73
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #74
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #75
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #76
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #77
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #78
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #79
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #80
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #81
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #82
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #83
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #84
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '111111111111111111111100',
            ),
            array( // row #85
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000001111111111',
            ),
            array( // row #86
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000011111',
            ),
            array( // row #87
                'email' => 'alexander.soloviev@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #88
                'email' => 'ronald.chittick@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #89
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000111111111111111100',
            ),
            array( // row #90
                'email' => 'darren.james@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #91
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '111111000000000000000111',
            ),
            array( // row #92
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '111111001111111111111111',
            ),
            array( // row #93
                'email' => 'karen.dooms@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '111111111111111111111110',
            ),
            array( // row #94
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000111111111111111100',
            ),
            array( // row #95
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '100000111111111111111100',
            ),
            array( // row #96
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '001000001111111111111101',
            ),
            array( // row #97
                'email' => 'Andrea.Ramirez@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #98
                'email' => 'judith.martin@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #99
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '111111111111111111000011',
            ),
            array( // row #100
                'email' => 'jason.kerkhof@djj.nsw.gov.au',
                'date' => '2015-11-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #101
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #102
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #103
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #104
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '111111111111111111111110',
            ),
            array( // row #105
                'email' => 'judith.martin@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #106
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #107
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #108
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #109
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #110
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #111
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #112
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #113
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #114
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #115
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #116
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #117
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #118
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #119
                'email' => 'michael.zambounis@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '111111111111111111110000',
            ),
            array( // row #120
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '111111111111111100000000',
            ),
            array( // row #121
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000011111111111',
            ),
            array( // row #122
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #123
                'email' => 'darren.james@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #124
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '111111111111110000000000',
            ),
            array( // row #125
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '111111000000001111111111',
            ),
            array( // row #126
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #127
                'email' => 'karen.dooms@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '111111111111111111111110',
            ),
            array( // row #128
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000111111111111111111',
            ),
            array( // row #129
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000111111111111111000',
            ),
            array( // row #130
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000001111111111111110',
            ),
            array( // row #131
                'email' => 'Andrea.Ramirez@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #132
                'email' => 'jason.kerkhof@djj.nsw.gov.au',
                'date' => '2015-11-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #133
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #134
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #135
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000001111111111111111111',
            ),
            array( // row #136
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #137
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #138
                'email' => 'judith.martin@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #139
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #140
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #141
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #142
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #143
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #144
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #145
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #146
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #147
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #148
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #149
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #150
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #151
                'email' => 'michael.zambounis@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000111111111111111111111',
            ),
            array( // row #152
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '111111000000000000000000',
            ),
            array( // row #153
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #154
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000111111111111111100',
            ),
            array( // row #155
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #156
                'email' => 'jennifer.latu@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '111111111111110000000000',
            ),
            array( // row #157
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #158
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000001000000000100010001',
            ),
            array( // row #159
                'email' => 'Andrea.Ramirez@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000001000000000000000',
            ),
            array( // row #160
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000111111110000000000',
            ),
            array( // row #161
                'email' => 'jason.kerkhof@djj.nsw.gov.au',
                'date' => '2015-11-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #162
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000111111111111111100',
            ),
            array( // row #163
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #164
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000001111111111111111111',
            ),
            array( // row #165
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #166
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #167
                'email' => 'judith.martin@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #168
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #169
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #170
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #171
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #172
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #173
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #174
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #175
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #176
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #177
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #178
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #179
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #180
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '111111111111111111111100',
            ),
            array( // row #181
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000011111111111',
            ),
            array( // row #182
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #183
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #184
                'email' => 'jennifer.latu@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000001111111111111',
            ),
            array( // row #185
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #186
                'email' => 'karen.dooms@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '111111111111111111111110',
            ),
            array( // row #187
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '010010001111111111111110',
            ),
            array( // row #188
                'email' => 'Andrea.Ramirez@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #189
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #190
                'email' => 'jason.kerkhof@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #191
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-11-22',
                'status' => '111111111111111111111111',
            ),
            array( // row #192
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #193
                'email' => 'shawn.mckinley@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #194
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #195
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #196
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #197
                'email' => 'judith.martin@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000001111111111',
            ),
            array( // row #198
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #199
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #200
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #201
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #202
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #203
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #204
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #205
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #206
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #207
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #208
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #209
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #210
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '111111000000111111111100',
            ),
            array( // row #211
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #212
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #213
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #214
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '111111111111111111111111',
            ),
            array( // row #215
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '111111000000001111111111',
            ),
            array( // row #216
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #217
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000101001010100',
            ),
            array( // row #218
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000111111111111000000',
            ),
            array( // row #219
                'email' => 'jason.kerkhof@djj.nsw.gov.au',
                'date' => '2015-11-23',
                'status' => '000000000000000000011111',
            ),
            array( // row #220
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #221
                'email' => 'shawn.mckinley@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #222
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #223
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #224
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #225
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #226
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #227
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #228
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #229
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #230
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #231
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #232
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #233
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #234
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #235
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #236
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #237
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '111111111111111111111100',
            ),
            array( // row #238
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #239
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #240
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #241
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '111111111111111111110000',
            ),
            array( // row #242
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '111111001111111111111111',
            ),
            array( // row #243
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000001111111111111100',
            ),
            array( // row #244
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000111111111111000000',
            ),
            array( // row #245
                'email' => 'peter.jensen@djj.nsw.gov.au',
                'date' => '2015-11-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #246
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #247
                'email' => 'shawn.mckinley@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #248
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #249
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #250
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #251
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #252
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #253
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #254
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #255
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #256
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #257
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #258
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #259
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #260
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #261
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #262
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #263
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '111111111111111111111100',
            ),
            array( // row #264
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000111',
            ),
            array( // row #265
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #266
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #267
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #268
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '111111001111111111111111',
            ),
            array( // row #269
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000100001111111111111100',
            ),
            array( // row #270
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-25',
                'status' => '000000111111111111111100',
            ),
            array( // row #271
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #272
                'email' => 'shawn.mckinley@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #273
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #274
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #275
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #276
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #277
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #278
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #279
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #280
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #281
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #282
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #283
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #284
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #285
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #286
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #287
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #288
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '111111111111111100000000',
            ),
            array( // row #289
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '111111100000001111111111',
            ),
            array( // row #290
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000111111111111111100',
            ),
            array( // row #291
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #292
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000001111',
            ),
            array( // row #293
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '111111001111111111111111',
            ),
            array( // row #294
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '001010001111111111111100',
            ),
            array( // row #295
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000111111111111111100',
            ),
            array( // row #296
                'email' => 'peter.jensen@djj.nsw.gov.au',
                'date' => '2015-11-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #297
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #298
                'email' => 'shawn.mckinley@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #299
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #300
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #301
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #302
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '111111111111111111111110',
            ),
            array( // row #303
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #304
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #305
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #306
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #307
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #308
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #309
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #310
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #311
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #312
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #313
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #314
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #315
                'email' => 'michael.zambounis@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '111111111111111111110000',
            ),
            array( // row #316
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '111111000000000000000000',
            ),
            array( // row #317
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #318
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #319
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000000000000000000011',
            ),
            array( // row #320
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '111111000000001111111111',
            ),
            array( // row #321
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '100000001111111111111100',
            ),
            array( // row #322
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-27',
                'status' => '000000111111111111000000',
            ),
            array( // row #323
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000011111111100',
            ),
            array( // row #324
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #325
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #326
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #327
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #328
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #329
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #330
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #331
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #332
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #333
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #334
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #335
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #336
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #337
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #338
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #339
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #340
                'email' => 'michael.zambounis@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000111111111111111111111',
            ),
            array( // row #341
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '111111000000011111111100',
            ),
            array( // row #342
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000011111111',
            ),
            array( // row #343
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #344
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #345
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '111111000000000000000011',
            ),
            array( // row #346
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '010000100101010100010010',
            ),
            array( // row #347
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-28',
                'status' => '000000111111111111000000',
            ),
            array( // row #348
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000111111111111111100',
            ),
            array( // row #349
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #350
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #351
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #352
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #353
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #354
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #355
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #356
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #357
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #358
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #359
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #360
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #361
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #362
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #363
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #364
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #365
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '111111111111111111111100',
            ),
            array( // row #366
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #367
                'email' => 'brett.hugo@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000111111111111111100',
            ),
            array( // row #368
                'email' => 'govind.rai@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '111111111111110000000000',
            ),
            array( // row #369
                'email' => 'linda.stone@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000001111111111111100',
            ),
            array( // row #370
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-11-29',
                'status' => '000000000000000111111100',
            ),
            array( // row #371
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #372
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #373
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #374
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #375
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #376
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '111111111111111111111111',
            ),
            array( // row #377
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #378
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #379
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #380
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #381
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #382
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #383
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #384
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #385
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #386
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #387
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '111111111111111111111100',
            ),
            array( // row #388
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '111111000000001111111111',
            ),
            array( // row #389
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-11-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #390
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #391
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #392
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #393
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #394
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #395
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #396
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #397
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #398
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #399
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #400
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #401
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #402
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #403
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #404
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #405
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #406
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '111111111111111111111100',
            ),
            array( // row #407
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '111111111111111111000000',
            ),
            array( // row #408
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '111111001111111111111111',
            ),
            array( // row #409
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #410
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #411
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #412
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #413
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #414
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #415
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #416
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #417
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #418
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #419
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #420
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #421
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #422
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #423
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #424
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #425
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #426
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '111111111111111111111100',
            ),
            array( // row #427
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #428
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '111111001111111111111111',
            ),
            array( // row #429
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #430
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #431
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #432
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #433
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #434
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #435
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #436
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #437
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #438
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #439
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #440
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #441
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #442
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #443
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #444
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #445
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #446
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '111111111111111111111100',
            ),
            array( // row #447
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000001111',
            ),
            array( // row #448
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '111111001111111111111111',
            ),
            array( // row #449
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #450
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #451
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #452
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #453
                'email' => 'judith.martin@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '111111111111111111111111',
            ),
            array( // row #454
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #455
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #456
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #457
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #458
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #459
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #460
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #461
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #462
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #463
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #464
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #465
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #466
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #467
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '111111111111111100000000',
            ),
            array( // row #468
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '111111000000001111111111',
            ),
            array( // row #469
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #470
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000011111111100',
            ),
            array( // row #471
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #472
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #473
                'email' => 'judith.martin@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #474
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #475
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #476
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #477
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #478
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #479
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #480
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #481
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #482
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #483
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #484
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #485
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #486
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #487
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '111111000000011111111100',
            ),
            array( // row #488
                'email' => 'wilson.ng@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '111111111000011111111111',
            ),
            array( // row #489
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-05',
                'status' => '000000000000000000000111',
            ),
            array( // row #490
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000111111111111111100',
            ),
            array( // row #491
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #492
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #493
                'email' => 'judith.martin@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #494
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #495
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #496
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #497
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #498
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #499
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #500
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #501
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #502
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #503
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #504
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #505
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #506
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #507
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-06',
                'status' => '111111111111111111111100',
            ),
            array( // row #508
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #509
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #510
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #511
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #512
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #513
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #514
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #515
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #516
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #517
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #518
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #519
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #520
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #521
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #522
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #523
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #524
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '111111000000111111111100',
            ),
            array( // row #525
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '111111000000001111111111',
            ),
            array( // row #526
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #527
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #528
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #529
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #530
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #531
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #532
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #533
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #534
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #535
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #536
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #537
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #538
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #539
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #540
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #541
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #542
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #543
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #544
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '111111111111111111111100',
            ),
            array( // row #545
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '111111001111111111111111',
            ),
            array( // row #546
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000011111',
            ),
            array( // row #547
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #548
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #549
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #550
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #551
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #552
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #553
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #554
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #555
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #556
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #557
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #558
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #559
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #560
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #561
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #562
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #563
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #564
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #565
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '111111111111111111111100',
            ),
            array( // row #566
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '111111001111111111111111',
            ),
            array( // row #567
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #568
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #569
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #570
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #571
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '111111111111110000000000',
            ),
            array( // row #572
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #573
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #574
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #575
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #576
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #577
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #578
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #579
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #580
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #581
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #582
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #583
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #584
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #585
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '111111001111111111111111',
            ),
            array( // row #586
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-10',
                'status' => '000000000000001111111111',
            ),
            array( // row #587
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #588
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #589
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #590
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '111111000000000000000000',
            ),
            array( // row #591
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #592
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #593
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #594
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #595
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #596
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #597
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #598
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #599
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #600
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #601
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #602
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #603
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #604
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '111111111111110000000000',
            ),
            array( // row #605
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '111111000000001111111111',
            ),
            array( // row #606
                'email' => 'blake.arnold@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '111111111111111000001111',
            ),
            array( // row #607
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-11',
                'status' => '000000000000011111111111',
            ),
            array( // row #608
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000011111111100',
            ),
            array( // row #609
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #610
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000001111111111111111111',
            ),
            array( // row #611
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '111111000000111111111100',
            ),
            array( // row #612
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '111111111111111000000000',
            ),
            array( // row #613
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #614
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #615
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #616
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #617
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #618
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #619
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #620
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #621
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #622
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #623
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #624
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #625
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #626
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-12',
                'status' => '000000111111111111111111',
            ),
            array( // row #627
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000111111111111111100',
            ),
            array( // row #628
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #629
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000001111111111111111111',
            ),
            array( // row #630
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #631
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #632
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #633
                'email' => 'shabee.syed@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #634
                'email' => 'brian.hall@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #635
                'email' => 'raafat.najjar@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #636
                'email' => 'nathan.derkinderen@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #637
                'email' => 'olga.valenzuela@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #638
                'email' => 'luke.randall@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #639
                'email' => 'glen.fardell@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #640
                'email' => 'hayley.meca@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #641
                'email' => 'david.haywood@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #642
                'email' => 'olivia.adam@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #643
                'email' => 'aaron.royall@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #644
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #645
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #646
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #647
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #648
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #649
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #650
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-14',
                'status' => '111111000000001111111111',
            ),
            array( // row #651
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #652
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #653
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #654
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #655
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #656
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #657
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-15',
                'status' => '111111001111111111111111',
            ),
            array( // row #658
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #659
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #660
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #661
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #662
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #663
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #664
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-16',
                'status' => '111111001111111111111111',
            ),
            array( // row #665
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #666
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #667
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #668
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #669
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #670
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #671
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-17',
                'status' => '111111001111111111111111',
            ),
            array( // row #672
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-17',
                'status' => '000000000000001111111111',
            ),
            array( // row #673
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #674
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #675
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #676
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #677
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #678
                'email' => 'olivia.mccrow@djj.nsw.gov.au',
                'date' => '2015-12-18',
                'status' => '111111000000001111111111',
            ),
            array( // row #679
                'email' => 'jonny.george@djj.nsw.gov.au',
                'date' => '2015-12-18',
                'status' => '000000000000011111111111',
            ),
            array( // row #680
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-18',
                'status' => '000000111111111000000000',
            ),
            array( // row #681
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-19',
                'status' => '000000000000011111111100',
            ),
            array( // row #682
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #683
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-19',
                'status' => '000001111111111111111111',
            ),
            array( // row #684
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #685
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #686
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #687
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-20',
                'status' => '000000111111111111111100',
            ),
            array( // row #688
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #689
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-20',
                'status' => '000001111111111111111111',
            ),
            array( // row #690
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #691
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-20',
                'status' => '111111111111111111111100',
            ),
            array( // row #692
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #693
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #694
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #695
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #696
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #697
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-21',
                'status' => '111111111111111111111111',
            ),
            array( // row #698
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-21',
                'status' => '111111111111111111111100',
            ),
            array( // row #699
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #700
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #701
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #702
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #703
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #704
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-22',
                'status' => '111111111111111111111111',
            ),
            array( // row #705
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-22',
                'status' => '111111111111111111111100',
            ),
            array( // row #706
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-22',
                'status' => '000000011111111111111100',
            ),
            array( // row #707
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #708
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #709
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #710
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #711
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-23',
                'status' => '111111111111111111111111',
            ),
            array( // row #712
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-23',
                'status' => '111111111111111111111100',
            ),
            array( // row #713
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-23',
                'status' => '000000111111111111111100',
            ),
            array( // row #714
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #715
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #716
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #717
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-24',
                'status' => '111111111111111111111100',
            ),
            array( // row #718
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #719
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-24',
                'status' => '111111111111111111111111',
            ),
            array( // row #720
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-24',
                'status' => '000000111111110000000000',
            ),
            array( // row #721
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #722
                'email' => 'Rikky.Messer@djj.nsw.gov.au',
                'date' => '2015-12-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #723
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #724
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #725
                'email' => 'robert.hughes@djj.nsw.gov.au',
                'date' => '2015-12-25',
                'status' => '111111000000011111111111',
            ),
            array( // row #726
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #727
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-12-25',
                'status' => '111111111111111000000000',
            ),
            array( // row #728
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #729
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #730
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-26',
                'status' => '000000000000011111111100',
            ),
            array( // row #731
                'email' => 'Rikky.Messer@djj.nsw.gov.au',
                'date' => '2015-12-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #732
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #733
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #734
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #735
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-12-26',
                'status' => '000000000000000000000111',
            ),
            array( // row #736
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #737
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-26',
                'status' => '000001111111111111111100',
            ),
            array( // row #738
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-27',
                'status' => '000000111111111111111100',
            ),
            array( // row #739
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #740
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-27',
                'status' => '000001111111111111111111',
            ),
            array( // row #741
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #742
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #743
                'email' => 'emma-louise.dawson@djj.nsw.gov.au',
                'date' => '2015-12-27',
                'status' => '000000111111111111111100',
            ),
            array( // row #744
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #745
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #746
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #747
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #748
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #749
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #750
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #751
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #752
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #753
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #754
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #755
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #756
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #757
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #758
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #759
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2015-12-31',
                'status' => '000000000000000000000000',
            ),
            array( // row #760
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2015-12-31',
                'status' => '000000000000000000000000',
            ),
            array( // row #761
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2015-12-31',
                'status' => '111111111111111100000000',
            ),
            array( // row #762
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2015-12-31',
                'status' => '000000000000000000000000',
            ),
            array( // row #763
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2015-12-31',
                'status' => '000000000000000000000000',
            ),
            array( // row #764
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2015-12-31',
                'status' => '000000000000000000000000',
            ),
            array( // row #765
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2016-01-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #766
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #767
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-01-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #768
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2016-01-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #769
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2016-01-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #770
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2016-01-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #771
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2016-01-02',
                'status' => '000000000000011111111100',
            ),
            array( // row #772
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #773
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2016-01-02',
                'status' => '000001111111111111111111',
            ),
            array( // row #774
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2016-01-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #775
                'email' => 'Nick.Batty@djj.nsw.gov.au',
                'date' => '2016-01-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #776
                'email' => 'john.alivio@djj.nsw.gov.au',
                'date' => '2016-01-03',
                'status' => '000000111111111111111100',
            ),
            array( // row #777
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #778
                'email' => 'james.chea@djj.nsw.gov.au',
                'date' => '2016-01-03',
                'status' => '000001111111111111111111',
            ),
            array( // row #779
                'email' => 'samantha.lobb@djj.nsw.gov.au',
                'date' => '2016-01-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #780
                'email' => 'scott.hill@djj.nsw.gov.au',
                'date' => '2016-01-04',
                'status' => '111111111111111111111111',
            ),
            array( // row #781
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #782
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #783
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #784
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #785
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #786
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #787
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-01-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #788
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #789
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-01-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #790
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #791
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #792
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #793
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #794
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #795
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #796
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #797
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #798
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #799
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #800
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #801
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #802
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #803
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #804
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #805
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #806
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #807
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #808
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #809
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #810
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-01-31',
                'status' => '000000000000000000000000',
            ),
            array( // row #811
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-01',
                'status' => '111111000000001111111111',
            ),
            array( // row #812
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2016-02-01',
                'status' => '111111111111111111111111',
            ),
            array( // row #813
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-02',
                'status' => '111111000000001111111111',
            ),
            array( // row #814
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2016-02-02',
                'status' => '111111111111111111111111',
            ),
            array( // row #815
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-03',
                'status' => '111111000000001111111111',
            ),
            array( // row #816
                'email' => 'matthew.dunn@djj.nsw.gov.au',
                'date' => '2016-02-03',
                'status' => '111111111111111111111111',
            ),
            array( // row #817
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-04',
                'status' => '111111000000001111111111',
            ),
            array( // row #818
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-05',
                'status' => '111111000000001111111111',
            ),
            array( // row #819
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #820
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #821
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #822
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #823
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #824
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #825
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #826
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #827
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #828
                'email' => 'Mitchell.Crawford@djj.nsw.gov.au',
                'date' => '2016-02-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #829
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #830
                'email' => 'Mitchell.Crawford@djj.nsw.gov.au',
                'date' => '2016-02-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #831
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #832
                'email' => 'Mitchell.Crawford@djj.nsw.gov.au',
                'date' => '2016-02-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #833
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #834
                'email' => 'Mitchell.Crawford@djj.nsw.gov.au',
                'date' => '2016-02-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #835
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #836
                'email' => 'Mitchell.Crawford@djj.nsw.gov.au',
                'date' => '2016-02-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #837
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #838
                'email' => 'Mitchell.Crawford@djj.nsw.gov.au',
                'date' => '2016-02-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #839
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #840
                'email' => 'Mitchell.Crawford@djj.nsw.gov.au',
                'date' => '2016-02-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #841
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #842
                'email' => 'Mitchell.Crawford@djj.nsw.gov.au',
                'date' => '2016-02-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #843
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #844
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #845
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #846
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #847
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #848
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #849
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #850
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-02-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #851
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-01',
                'status' => '000000000000000000000000',
            ),
            array( // row #852
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-02',
                'status' => '000000000000000000000000',
            ),
            array( // row #853
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-03',
                'status' => '000000000000000000000000',
            ),
            array( // row #854
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-04',
                'status' => '000000000000000000000000',
            ),
            array( // row #855
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #856
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-05',
                'status' => '000000000000000000000000',
            ),
            array( // row #857
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #858
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-06',
                'status' => '000000000000000000000000',
            ),
            array( // row #859
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #860
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-07',
                'status' => '000000000000000000000000',
            ),
            array( // row #861
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #862
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-08',
                'status' => '000000000000000000000000',
            ),
            array( // row #863
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #864
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-09',
                'status' => '000000000000000000000000',
            ),
            array( // row #865
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #866
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-10',
                'status' => '000000000000000000000000',
            ),
            array( // row #867
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #868
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-11',
                'status' => '000000000000000000000000',
            ),
            array( // row #869
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #870
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-12',
                'status' => '000000000000000000000000',
            ),
            array( // row #871
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #872
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-13',
                'status' => '000000000000000000000000',
            ),
            array( // row #873
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #874
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-14',
                'status' => '000000000000000000000000',
            ),
            array( // row #875
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #876
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-15',
                'status' => '000000000000000000000000',
            ),
            array( // row #877
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #878
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-16',
                'status' => '000000000000000000000000',
            ),
            array( // row #879
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #880
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-17',
                'status' => '000000000000000000000000',
            ),
            array( // row #881
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #882
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-18',
                'status' => '000000000000000000000000',
            ),
            array( // row #883
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #884
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-19',
                'status' => '000000000000000000000000',
            ),
            array( // row #885
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #886
                'email' => 'katherine.draper@djj.nsw.gov.au',
                'date' => '2016-03-20',
                'status' => '000000000000000000000000',
            ),
            array( // row #887
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-21',
                'status' => '000000000000000000000000',
            ),
            array( // row #888
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-22',
                'status' => '000000000000000000000000',
            ),
            array( // row #889
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-23',
                'status' => '000000000000000000000000',
            ),
            array( // row #890
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-24',
                'status' => '000000000000000000000000',
            ),
            array( // row #891
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-25',
                'status' => '000000000000000000000000',
            ),
            array( // row #892
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-26',
                'status' => '000000000000000000000000',
            ),
            array( // row #893
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-27',
                'status' => '000000000000000000000000',
            ),
            array( // row #894
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-28',
                'status' => '000000000000000000000000',
            ),
            array( // row #895
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-29',
                'status' => '000000000000000000000000',
            ),
            array( // row #896
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-30',
                'status' => '000000000000000000000000',
            ),
            array( // row #897
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-03-31',
                'status' => '000000000000000000000000',
            ),
            array( // row #898
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-01',
                'status' => '111111111111111111111111',
            ),
            array( // row #899
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-02',
                'status' => '111111111111111111111111',
            ),
            array( // row #900
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-03',
                'status' => '111111111111111111111111',
            ),
            array( // row #901
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-04',
                'status' => '111111111111111111111111',
            ),
            array( // row #902
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-05',
                'status' => '111111111111111111111111',
            ),
            array( // row #903
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-06',
                'status' => '111111111111111111111111',
            ),
            array( // row #904
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-07',
                'status' => '111111111111111111111111',
            ),
            array( // row #905
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-08',
                'status' => '111111111111111111111111',
            ),
            array( // row #906
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-09',
                'status' => '111111111111111111111111',
            ),
            array( // row #907
                'email' => 'peter.carpenter@classcover.com.au',
                'date' => '2016-04-10',
                'status' => '111111111111111111111111',
            ),
        );

        return $UnknownTable;
    }

}