<?php

namespace ClassCover\BookingBundle\Command;

use ClassCover\BookingBundle\Entity\Booking;
use ClassCover\BookingBundle\Services\SmsBookingService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\BookingBundle\Entity\SmsRequestList;
use ClassCover\BookingBundle\Entity\SmsRequestListRepository;
use Doctrine\ORM\EntityManager;
/**
 * Class SmsCommand
 * @package ClassCover\BookingBundle\Command
 */
class CheckUnfilledBookingsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('bookings:check')->setDescription('Check Bookings.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $service = $this->getContainer()->get('sms_booking_service');

        /** @var SmsRequestListRepository $smsRequestsRepo */
        $smsRequestsRepo = $em->getRepository('ClassCoverBookingBundle:SmsRequestList');

        $now = new \DateTime();
        $openBookings = $em->getRepository("ClassCoverBookingBundle:Booking")->getUnfilledBookings();

        if (count($openBookings)) {
            /** @var Booking $booking */
            foreach($openBookings as $booking) {
                $timeNow = time();
                $timeStart = $booking->getDateTimeStart()->getTimestamp();

                $difference = ($timeStart-$timeNow);
                if ($difference<300) {
                    if (count($booking->getRequests())) {

                        /** @var SmsRequestList $item */
                        foreach($booking->getRequests() as $item) {
                            if ($item->getSmsStatus() == 'sent') {

                                if ($item->getResponse() == "none") {
                                    $item->setResponse('unanswered');
                                    if ($item->getParsed()>0) {
                                        $service->setTeacherExcluded($item->getBooking(), $item->getTeacher(), $item->getSchool());
                                    }
                                }
                            } else {
                                $item->setSmsStatus('cancelled');
                                $item->setParsed(2);
                            }

                            $em->flush($item);
                        }

                    }

                    $booking->setBookingStatus(Booking::BOOKING_STATUS_UNFILLED);
                    $em->flush($booking);
                }

            }
        }
    }

}