<?php

namespace ClassCover\BookingBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM as ORM;

use Symfony\Component\Validator\Constraints\Collection;

use ClassCover\BookingBundle\Services\SmsBookingService;


class CreateBookingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('booking:create')
            ->setDescription('Create a booking Reference. Usage booking:create [school] [startDateTime] [endDateTime] [type] [shiftType] [status] [numTeachers].')
            ->addArgument(
                'school',
                InputArgument::REQUIRED,
                'School Name, Eg : Reiby'
            )
            ->addArgument(
                'startDateTime',
                InputArgument::REQUIRED,
                'Start datetime, Eg : 2015-10-14 14:00:00'
            )
            ->addArgument(
                'endDateTime',
                InputArgument::REQUIRED,
                'End datetime, Eg : 2015-10-14 14:00:00'
            )
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'Booking Type, Eg : sms | [sms or manual]'
            )
            ->addArgument(
                'shiftType',
                InputArgument::REQUIRED,
                'Shift type, Eg : new | [new,pending,completed,unfilled,deleted]'
            )
            ->addArgument(
                'status',
                InputArgument::REQUIRED,
                'Booking status, Eg : new | [new,pending,completed,unfilled,deleted]'
            )
            ->addArgument(
                'numTeachers',
                InputArgument::REQUIRED,
                'Number of teachers, Eg : 1,2,3...Any number'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var SmsBookingService $service */
        $service = $this->getContainer()->get('sms_booking_service');

        $args = $input->getArguments();
        unset($args['command']);

        var_dump($args);
    }

}
