<?php

namespace ClassCover\BookingBundle\Mappers;


use ClassCover\BookingBundle\Entity\TeacherUnavailability;
use ClassCover\BookingBundle\Entity\TeacherBooking;
use ClassCover\BookingBundle\Entity\TeacherExclusion;
use ClassCover\BookingBundle\Entity\TeacherShiftOffer;

class TeacherAvailabilityRepresentation
{
    /**
     * @var
     */
    protected $id;

    /**
     * @var null
     */
    protected $teacher = null;

    /**
     * @var
     */
    protected $dateTime;

    /**
     * @var array
     */
    protected $dayHoursMask = [];

    const STATUS_AVAILABLE   = 0;
    const STATUS_UNAVAILABLE = 1;
    const STATUS_BOOKED      = 2;
    const STATUS_EXCLUDED    = 3;
    const STATUS_OFFERED     = 4;


    /**
     * @param $info
     */
    public function __construct($info) {

        $this->dayHoursMask = array_fill(0, 24, ['bit' => [0], 'entityIds' => []]);

        if (!empty($info)) {
            foreach ($info as $line) {
                /**
                 * @var TeacherUnavailability|TeacherBooking|TeacherExclusion|TeacherShiftOffer $line
                 */
                $timeIndex = (int) $line->getTime()->format('H');

                if ($line instanceof TeacherUnavailability) {
                    $this->setMask($timeIndex, self::STATUS_UNAVAILABLE, -1);
                } elseif ($line instanceof TeacherBooking) {
                    if ($line->getAdjacentInterval() != 1) {
                        $this->setMask($timeIndex, self::STATUS_BOOKED, $line->getBooking()->getId());
                    }
                } elseif ($line instanceof TeacherExclusion) {
                    $this->setMask($timeIndex, self::STATUS_EXCLUDED, -1);
                } elseif ($line instanceof TeacherShiftOffer) {
                    if ($line->getAdjacentInterval() != 1) {
                        $this->setMask($timeIndex, self::STATUS_OFFERED, -1);
                    }
                }
            }
        }
    }

    /**
     * @param $timeIndex
     * @param $value
     * @param $entityId
     * @return $this
     */
    protected function setMask($timeIndex, $value, $entityId) {

        if ($this->dayHoursMask[$timeIndex]['bit'][0] == 0) {
            $this->dayHoursMask[$timeIndex]['bit'][0] = $value;
        } else {
           // $this->dayHoursMask[$timeIndex]['bit'][1] = $value;
        }

        $this->dayHoursMask[$timeIndex]['entityIds'][] = $entityId;

        return $this;
    }

    /**
     * @return array
     */
    public function getRepresentation()
    {
        return $this->dayHoursMask;
    }
}