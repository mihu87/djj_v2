<?php

namespace ClassCover\BookingBundle\BusinessLogic\Conditions;

use ClassCover\BookingBundle\Entity\TeacherOvertimeRepository;
use ClassCover\SchoolBundle\Entity\School;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ClassCover\CyoBundle\Entity\Teacher;

class IsOvertime
{
    /**
     * @var Teacher
     */
    protected $teacher;
    protected $school;
    protected $container;

    /**
     * @var TeacherOvertimeRepository
     */
    protected $repository;
    protected $overtimeThreshold;

    public function __construct(ContainerInterface $container, Teacher $teacher, School $school)
    {

        $this->container = $container;
        $this->teacher = $teacher;
        $this->school = $school;
        $this->repository = $this->container->get('doctrine')->getRepository('ClassCoverBookingBundle:TeacherOvertime');

        $overtimeThreshold = $school->getOvertimeThreshold();

        if (!$overtimeThreshold) {
            $this->overtimeThreshold = $this->container->getParameter('overtime_threshold');
        } else {
            $this->overtimeThreshold = $overtimeThreshold;
        }
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param $timeFrom
     * @param $timeTo
     * @return bool
     */
    public function evaluate($dateFrom, $dateTo, $timeFrom, $timeTo)
    {
        $from = new \DateTime($dateFrom);
        $to = new \DateTime($dateTo);

        $timeFromArr = explode(":", $timeFrom);
        $timeToArr = explode(":", $timeTo);

        $from->setTime($timeFromArr[0], $timeFromArr[1], 0);
        $to->setTime($timeToArr[0], $timeToArr[1], 0);

        $currentDuration  = ($to->getTimestamp()-$from->getTimestamp())/3600;
        $currentDuration += $this->getCurrentHours();

        return  $currentDuration > $this->overtimeThreshold;
    }

    public function getCurrentHours()
    {
        return $this->repository->getOvertimeHours($this->teacher);
    }
}