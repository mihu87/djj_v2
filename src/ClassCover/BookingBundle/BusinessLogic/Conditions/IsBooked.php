<?php

namespace ClassCover\BookingBundle\BusinessLogic\Conditions;

use Symfony\Component\DependencyInjection\ContainerInterface;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\BookingBundle\Entity\TeacherBookingRepository;

class IsBooked {

    protected $teacher;
    protected $container;

    public function __construct(ContainerInterface $container, Teacher $teacher) {
        $this->container = $container;
        $this->teacher = $teacher;
    }

    public function evaluate($dateFrom, $dateTo, $timeFrom, $timeTo) {

        $from = new \DateTime($dateFrom);
        $to = new \DateTime($dateTo);

        $timeFromArr = explode(":", $timeFrom);
        $timeToArr = explode(":", $timeTo);

        $from->setTime($timeFromArr[0], $timeFromArr[1], 0);
        $to->setTime($timeToArr[0], $timeToArr[1], 0);

        $doctrine = $this->container->get('doctrine');
        /** @var TeacherBookingRepository $bookingRepository */
        $bookingRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherBooking');
        $bookingData = $bookingRepository->getBookingsDataOnInterval($from, $to, $this->teacher);

        if (!empty($bookingData)) {
            return true;
        }

        return false;
    }

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return bool
     */
    public function check(\DateTime $startDate, \DateTime $endDate)
    {
        $startDate->setTime($startDate->format('H'), $startDate->format('i'), 0);
        $endDate->setTime($endDate->format('H'), $endDate->format('i'), 0);

        $doctrine = $this->container->get('doctrine');
        /** @var TeacherBookingRepository $bookingRepository */
        $bookingRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherBooking');
        $bookingData = $bookingRepository->getBookingsDataOnInterval($startDate, $endDate, $this->teacher);

        if (!empty($bookingData)) {
            return true;
        }

        return false;
    }
}