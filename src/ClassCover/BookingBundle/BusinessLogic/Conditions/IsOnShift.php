<?php

namespace ClassCover\BookingBundle\BusinessLogic\Conditions;

use Symfony\Component\DependencyInjection\ContainerInterface;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\BookingBundle\Entity\TeacherBookingRepository;
use ClassCover\BookingBundle\Entity\TeacherBooking;

class IsOnShift {

    protected $teacher;
    protected $container;

    public function __construct(ContainerInterface $container, Teacher $teacher) {
        $this->container = $container;
        $this->teacher = $teacher;
    }

    public function evaluate() {

        $doctrine = $this->container->get('doctrine');
        /** @var TeacherBookingRepository $repo */
        $repo = $doctrine->getRepository('ClassCoverBookingBundle:TeacherBooking');

        return $repo->getShiftEndForCurrentTime($this->teacher);
    }
}