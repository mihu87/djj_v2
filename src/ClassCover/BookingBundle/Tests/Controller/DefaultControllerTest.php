<?php

namespace ClassCover\BookingBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultControllerTest extends WebTestCase
{
    /**
     * @Covers \ClassCover\BookingBundle\Controller\DefaultController::indexAction
     */
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/booking');
        $this->assertTrue($crawler->filter('html:contains("GoShift BookingModule")')->count() > 0);
    }

    /**
     * @Covers \ClassCover\BookingBundle\Controller\DefaultController::indexAction
     */
    public function testGetTeachersAvailableForSmsBookingAjax()
    {
        $headers = ['CONTENT_TYPE' => 'application/x-www-form-urlencoded'];
        $params = [
            'dateFrom'       => '21 Oct, 2015',
            'dateTo'         => '21 Oct, 2015',
            'format'         => 'display/html',
            'isMultiple'     => '0',
            'shiftTypeId'    => '1',
            'shiftTypeName'  => 'Morning',
            'teachersToBook' => '1',
            'timeFrom'       => '06:00',
            'timeTo'         => '14:00',
            'userId'         => '1'
        ];

        $client = static::createClient();
        $client->request('POST', '/booking/get-teachers-available-for-sms-booking', $params, [], $headers);
        $response = $client->getResponse();
        $this->assertTrue($response->getStatusCode() == 200, 'Testing if the response is ok');
        $this->assertJson($response->getContent(), 'Testing if the response is json');
    }
}
