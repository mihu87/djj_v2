<?php

namespace ClassCover\BookingBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Class SmsSenderService
 * @package ClassCover\BookingBundle\Services
 */
class SmsSenderService
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
}