<?php

namespace ClassCover\BookingBundle\Services;

use ClassCover\BookingBundle\BusinessLogic\Conditions\IsOnShift;
use ClassCover\BookingBundle\BusinessLogic\Conditions\IsOvertime;
use ClassCover\BookingBundle\Entity\TeacherExclusion;
use ClassCover\BookingBundle\Entity\TeacherOvertimeRepository;
use ClassCover\BookingBundle\Entity\TeacherShiftOffer;
use ClassCover\BookingBundle\Entity\TeacherUnavailability;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query;
use Jhg\NexmoBundle\Model\Sms;
use SebastianBergmann\Comparator\Book;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ClassCover\SchoolBundle\Entity\School;
use ClassCover\SchoolBundle\Entity\RegisteredTeachers;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\BookingBundle\Entity\TeacherBooking;
use ClassCover\BookingBundle\Entity\TeacherShiftOfferRepository;
use ClassCover\BookingBundle\Entity\TeacherBookingRepository;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use ClassCover\BookingBundle\Entity\KeywordCounterRepository;
use ClassCover\BookingBundle\Entity\KeywordCounter;
use ClassCover\BookingBundle\Entity\Booking;
use ClassCover\BookingBundle\Entity\BookingRepository;
use Doctrine\ORM\EntityRepository;

use ClassCover\BookingBundle\BusinessLogic\Conditions\IsUnavailable;
use ClassCover\BookingBundle\BusinessLogic\Conditions\IsBooked;
use ClassCover\BookingBundle\BusinessLogic\Conditions\IsExcluded;
use ClassCover\BookingBundle\BusinessLogic\Conditions\IsOffered;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Validator\Constraints\Collection;

use ClassCover\BookingBundle\Entity\SmsRequestList;

use ClassCover\BookingBundle\Entity\SmsRequestListRepository;
use ClassCover\BookingBundle\Entity\TeacherExclusionRepository;
use ClassCover\SchoolBundle\Entity\AdditionalUsers;
use ClassCover\BookingBundle\Entity\SmsHistory;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpKernel\KernelInterface;

use Monolog\Logger;
/**
 * Class SmsBookingService
 * @package ClassCover\BookingBundle\Services
 */
class SmsBookingService {


    const SMS_DECISION_BOOKING_REQUEST_ACCEPT  = 'Y';
    const SMS_DECISION_BOOKING_REQUEST_DECLINE = 'N';

    const TEACHER_STATUS_INTERVAL_STRING       = 'PT1H';
    const TEACHER_STATUS_INTERVAL     = '1 hour';
    const TEACHER_STATUS_DEFAULT_ADJACENT_HOURS = 8;
    const TEACHER_STATUS_DEFAULT_NO_ADJACENT_HOURS = 0;

    const SMS_REQUEST_PARSED_OK = 1;
    const SMS_REQUEST_PARSED_CANCELLED = 2;

    /**
     * @var array
     */
    static $allowedSmsResponseDecisionChars = [
      self::SMS_DECISION_BOOKING_REQUEST_ACCEPT,
      self::SMS_DECISION_BOOKING_REQUEST_DECLINE,
    ];

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->entityManager = $this->container->get('doctrine')->getManager();
        $this->logger = $this->container->get('logger');
    }

    /**
     * @param $dateFrom
     * @param $timeFrom
     * @return array
     */
    public function getDefaultScheduledTimes($dateFrom, $timeFrom)
    {
        $data = [0];

        $start = $dateFrom;
        $startTime = explode(":", $timeFrom);

        $time = new \DateTime($start);
        $time->setTime($startTime[0], $startTime[1], 0);

        $timeNow = new \DateTime();

        $offset = $time->getTimestamp() - $timeNow->getTimestamp();

        if ($offset >= 28800) { // 8 hours
            $data = [60, 30, 15, 10];
        } elseif ($offset < 28800 && $offset >= 21600) { // 8 hours and 6 hours
            $data = [40, 30, 15, 10];
        } elseif ($offset < 21600 && $offset >= 10800) { // 6 hours and 3 hours
            $data = [30, 20, 10, 5];
        } elseif ($offset < 10800 && $offset >= 3600) { // 3 hours and 1 hours
            $data = [10, 5, 2];
        } elseif ($offset < 3600 && $offset >= 900) { // 1 hour and 15 mins
            $data = [2];
        }

        return $data;
    }

    /**
     * @param $schoolId
     * @param $dateFrom
     * @param $dateTo
     * @param $timeFrom
     * @param $timeTo
     * @param int $teachersToBook
     * @return array|bool
     */
    public function getTeachersAvailabilityForSmsBooking($schoolId, $dateFrom, $dateTo, $timeFrom, $timeTo, $teachersToBook = 0)
    {
        $list = [
            'available' => [],
            'unavailable' => [],
            'excluded' => [],
            'overtime' => []
        ];
        
        /** @var School $school */
        $school = $this->entityManager->getRepository('ClassCoverSchoolBundle:School')->find($schoolId);

        if (empty($school)) {
            return false;
        }

        $registeredTeachers = $school->getTeachers();

        if ($registeredTeachers->count() == 0) {
            return false;
        }

        $scheduledTimes = $this->getDefaultScheduledTimes($dateFrom, $timeFrom);

        /** @var RegisteredTeachers $registeredTeacher */
        foreach ($registeredTeachers as $registeredTeacher) {

            if ($registeredTeacher->getRemoved() > 0 || $registeredTeacher->getSuspended() > 0) {
                continue;
            }

            /** @var Teacher $teacher */
            $teacher = $registeredTeacher->getTeacher();

            $isUnavailable = (new IsUnavailable($this->container, $teacher))
                ->evaluate($dateFrom, $dateTo, $timeFrom, $timeTo);

            $isBooked = (new IsBooked($this->container, $teacher))
                ->evaluate($dateFrom, $dateTo, $timeFrom, $timeTo);

            $isOffered = (new IsOffered($this->container, $teacher))
                ->evaluate($dateFrom, $dateTo, $timeFrom, $timeTo);

            $isExcluded = (new IsExcluded($this->container, $teacher))
                ->evaluate($dateFrom, $dateTo, $timeFrom, $timeTo);

            $onShift = (new IsOnShift($this->container, $teacher))
                ->evaluate();

            $overtime = (new IsOvertime($this->container, $teacher, $school))
                ->evaluate($dateFrom, $dateTo, $timeFrom, $timeTo);

            $overtimeHours = (new IsOvertime($this->container, $teacher, $school))
                ->getCurrentHours();

            $subList = 'available';

            if ($isUnavailable || $isBooked || $isOffered) {
                $subList = 'unavailable';
            }

            if ($isExcluded) {
                $subList = 'excluded';
            }

            if ($overtime) {
                $subList = "overtime";
            }

            $delay = 0;

            $row = [
                'name' => $teacher->getFirstName() . ' ' . $teacher->getSurname(),
                'phone' => $teacher->getPhone(),
                'email' => $teacher->getEmail(),
                'user_id' => $teacher->getUser()->getId(),
                'internal_id' => $teacher->getId(),
                'photo_file' => $teacher->getPhotoFile(),
                'default_delay' => $delay,
                'on_shift' => $onShift,
                'overtime_hours' => $overtimeHours
            ];

            $list[$subList][] = $row;
        }

        $x = 0;
        if (count($list['available'])) {
            for ($i=$teachersToBook; $i<count($list['available']); $i++) {

                if (isset($scheduledTimes[$x])) {
                    $list['available'][$i]['default_delay'] = $scheduledTimes[$x];
                } else {
                    $list['available'][$i]['default_delay'] = end($scheduledTimes);
                }

                ++$x;
            }
        }

        if (count($list['overtime'])) {
            for ($i=$teachersToBook; $i<count($list['overtime']); $i++) {

                if (isset($scheduledTimes[$x])) {
                    $list['overtime'][$i]['default_delay'] = $scheduledTimes[$x];
                } else {
                    $list['overtime'][$i]['default_delay'] = end($scheduledTimes);
                }

                ++$x;
            }
        }

        return $list;
    }

    /**
     * @param $teacherId
     * @param $dateFrom
     * @param $dateTo
     * @param $timeFrom
     * @param $timeTo
     * @return bool|int true - if available , false if not and -1 on failure if teacher not found
     */
    public function getTeacherAvailabilityForManualBooking($teacherId, $dateFrom, $dateTo, $timeFrom, $timeTo)
    {
        /** @var Teacher $teacher */
        $teacher = $this->entityManager->getRepository('ClassCoverCyoBundle:Teacher')->find($teacherId);

        // On failure return -1
        if ($teacher == null) {
            return -1;
        }

        $isUnavailable = (new IsUnavailable($this->container, $teacher))
            ->evaluate($dateFrom, $dateTo, $timeFrom, $timeTo);

        $isBooked = (new IsBooked($this->container, $teacher))
            ->evaluate($dateFrom, $dateTo, $timeFrom, $timeTo);

        $isOffered = (new IsOffered($this->container, $teacher))
            ->evaluate($dateFrom, $dateTo, $timeFrom, $timeTo);

        $isExcluded = (new IsExcluded($this->container, $teacher))
            ->evaluate($dateFrom, $dateTo, $timeFrom, $timeTo);

        /*$onShift = (new IsOnShift($this->container, $teacher))
            ->evaluate();*/

        if ($isUnavailable || $isBooked || $isOffered || $isExcluded) {
            return false;
        }

        return true;
    }

    /**
     * @param School $school
     * @param ArrayCollection $teachers
     * @param \DateTime $from
     * @param \DateTime $to
     * @param int $requestedTeachers
     * @param array $info
     * @param string $shiftType
     * @param string $type
     * @param string $status
     * @return \stdClass
     */
    public function createBookingRequest(School $school, ArrayCollection $teachers, \DateTime $from, \DateTime $to, $requestedTeachers = 0, array $info = [], $shiftType = Booking::SHIFT_TYPE_SPECIAL, $type = Booking::BOOKING_TYPE_SMS, $status = Booking::BOOKING_STATUS_NEW) {

        $messages = [];
        $errors   = [];

        if ($requestedTeachers <= 0) {
            $errors[] = 'Requested teachers number should be greater than 0';
        }

        if (!in_array($status, Booking::$allowedBookingStatuses)) {
            $errors[] = 'Booking status not allowed [' . $status . ']';
        }

        if (!in_array($type, Booking::$allowedBookingTypes)) {
            $errors[] = 'Booking type not allowed [' . $type . ']';
        }

        if (!in_array($shiftType, Booking::$allowedShiftTypes)) {
            $errors[] = 'Booking shift type not allowed [' . $shiftType . ']';
        }

        if (count($errors)>0) {
            return $this->getProcessingStatus($messages, $errors);
        }

        $keywordRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:KeywordCounter');

        do {
            $postFixNumber = substr(number_format(time() * rand(), 0, '', ''), 0, 4);
            $keyword = $keywordRepo->findOneBy(['keywordPostfixNumber' => $postFixNumber]);
        } while($keyword instanceof KeywordCounter);

        $schoolShortName    = $school->getShortName();
        $acceptKeyword      = self::SMS_DECISION_BOOKING_REQUEST_ACCEPT . $postFixNumber;
        $declineKeyword     = self::SMS_DECISION_BOOKING_REQUEST_DECLINE . $postFixNumber;
        $smsShiftType = ucfirst($shiftType);

        $smsDay             = $from->format("j/n");
        $smsTime            = $from->format("H:i") . '-' . $to->format("H:i");

        if ($from->format('Y-m-d') != $to->format('Y-m-d')) {
            $smsDay.= '-' . $to->format("j/n");
        }

        $smsText = sprintf($this->container->getParameter('booking_request_sms_format'),
            $smsShiftType,
            $schoolShortName,
            $smsDay,
            $smsTime,
            $acceptKeyword,
            $declineKeyword
        );

        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        /** @var Booking $booking */
        $booking = new Booking();
        $booking->setSchool($school);
        $booking->setDateTimeStart($from);
        $booking->setDateTimeEnd($to);
        $booking->setBookingType($type);
        $booking->setBookingStatus($status);
        $booking->setShiftType($shiftType);
        $booking->setTeachersRequested($requestedTeachers);
        $booking->setTeachersBooked(0);
        $booking->setSmsText($smsText);
        $booking->setUser($user);

        if ($type == Booking::BOOKING_TYPE_SMS) {

            $referenceTime = new \DateTime('NOW');

            /** @var Teacher $teacher */
            foreach ($teachers as $k => $teacher) {

                $delay = 0;
                $exclusion = false;

                if (!empty($info)) {
                    $teacherInfo = $info['teacherList'][$k];
                    if ($teacherInfo['teacherId'] == $teacher->getId()) {
                        $delay = $teacherInfo['delay'];
                        if (isset($teacherInfo['exclusion'])) {
                            $exclusion = true;
                        }
                    }
                }

                $sendTime = clone $referenceTime;
                $sendTime->modify("+{$delay} minutes");
                $sendTime->setTime($sendTime->format('H'), $sendTime->format('i'), 0);

                /** @var SmsRequestList $smsRequestList */
                $smsRequestList = new SmsRequestList();
                $smsRequestList->setSchool($school);
                $smsRequestList->setTeacher($teacher);
                $smsRequestList->setBooking($booking);
                $smsRequestList->setDelayMinutes($delay);
                $smsRequestList->setAcceptKeyword($acceptKeyword);
                $smsRequestList->setDeclineKeyword($declineKeyword);
                $smsRequestList->setResponse('none');
                $smsRequestList->setSmsStatus('pending');
                $smsRequestList->setSkipped(0);
                $smsRequestList->setToNumber($teacher->getPhone());
                $smsRequestList->setSmsText($smsText);
                $smsRequestList->setNetwork('nexmo');
                $smsRequestList->setSendTime($sendTime);

                if ($exclusion) {
                    $smsRequestList->setExcluded(1);
                }

                $booking->addRequest($smsRequestList);

                $referenceTime = $sendTime;
            }
        }

        $usedKeyword = new KeywordCounter();
        $usedKeyword->setBooking($booking);
        $usedKeyword->setKeywordPostfixNumber($postFixNumber);
        $usedKeyword->setKeywordPostfixString($postFixNumber);

        $booking->setKeyword($usedKeyword);

        $this->entityManager->persist($booking);
        $this->entityManager->flush();

        $this->sendMessage("+61410484446", "New Booking: " . $booking->getId() . "/" . $booking->getSchool()->getName() . "/" . $booking->getBookingType());
        $this->sendMessage("+447470888421", "New Booking: " . $booking->getId() . "/" . $booking->getSchool()->getName() . "/" . $booking->getBookingType());

        $this->processSendList();

        if ($type == Booking::BOOKING_TYPE_MANUAL) {

            $booking->setBookingStatus(Booking::BOOKING_STATUS_COMPLETED);
            $this->entityManager->persist($booking);
            $this->entityManager->flush();

            $allTeachers = $teachers->toArray();
            $teacher = array_shift($allTeachers);

            $this->setTeacherBooked($booking, $teacher, $school);
        }

        return $this->getProcessingStatus($messages, [], $booking);
    }

    /**
     * @param Booking $booking
     */
    public function cancelBooking(Booking $booking)
    {
        $messages = ['Exempt', 'Booking was cancelled. Casual exempted.'];

        /** @var SmsRequestList $smsRequest */
        foreach ($booking->getRequests() as $smsRequest) {

            $this->clearTeacherOfferingCells($smsRequest->getBooking(), $smsRequest->getTeacher(), $smsRequest->getSchool());

            if ($smsRequest->getSmsStatus() == "pending") {
                $smsRequest->setSmsStatus("cancelled");
                $smsRequest->setObservations(json_encode($messages));
                $smsRequest->setSkipped(1);
            }
        }

        $teacherBookingCells = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherBooking')
            ->findBy(['booking' => $booking]);

        $exclusionBookingCells = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherExclusion')
            ->findBy(['booking' => $booking]);

        $bookedTeachers = [];

        if (count((array) $teacherBookingCells)) {
            /** @var TeacherBooking $cell */
            foreach($teacherBookingCells as $cell) {
                $bookedTeachers[$cell->getTeacher()->getId()] = $cell->getTeacher();
                $this->entityManager->remove($cell);
                $this->entityManager->flush();
            }
        }

        if (count((array) $exclusionBookingCells)) {
            foreach($exclusionBookingCells as $cell) {
                $this->entityManager->remove($cell);
                $this->entityManager->flush();
            }
        }
        
        $booking->setBookingStatus(Booking::BOOKING_STATUS_CANCELLED);

        if (count($bookedTeachers)) {
            /** @var TeacherOvertimeRepository $otRepository */
            $otRepository = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherOvertime');
            $duration = ($booking->getDateTimeEnd()->getTimestamp() - $booking->getDateTimeStart()->getTimestamp())/3600;

            foreach ($bookedTeachers as $teacher) {
                $otRepository->substractOverTime($teacher, $duration);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @param Booking $booking
     * @param School $school
     * @param Teacher $teacher
     * @param array $data
     * @return bool
     */
    public function updateTeacherBooking(Booking $booking, School $school, Teacher $teacher, array $data)
    {
        /** @var Booking $booking */
        $teacherBookings = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherBooking')
            ->findBy(['school' => $school, 'booking' => $booking, 'teacher' => $teacher]);

        /** @var TeacherBooking $bookingLine */
        foreach ($teacherBookings as $bookingLine) {
            $this->entityManager->remove($bookingLine);
            $this->entityManager->flush();
        }

        /** @var TeacherOvertimeRepository $otRepository */
        $otRepository = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherOvertime');
        $duration = ($booking->getDateTimeEnd()->getTimestamp() - $booking->getDateTimeStart()->getTimestamp()) / 3600;
        $otRepository->substractOverTime($teacher, $duration);

        $timeFrom   = explode(":", $data['timeFrom']);
        $timeTo     = explode(":", $data['timeTo']);

        $dateTimeFrom = (new \DateTime($data['dateFrom']))->setTime($timeFrom[0], $timeFrom[1], 0);
        $dateTimeTo = (new \DateTime($data['dateTo']))->setTime($timeTo[0], $timeTo[1], 0);

        $booking->setDateTimeStart($dateTimeFrom);
        $booking->setDateTimeEnd($dateTimeTo);
        $booking->setShiftType($data['shiftTypeName']);

        $this->entityManager->persist($booking);
        $this->entityManager->flush();

        $this->setTeacherBooked($booking, $teacher, $school);

        return true;
    }

    /**
     * @param array $content
     * @return bool
     */
    public function handleSmsBookingResponseRequest(array $content)
    {
        /** @var SmsRequestListRepository $smsRequestRepository */
        $smsRequestRepository = $this->entityManager->getRepository('ClassCoverBookingBundle:SmsRequestList');

        $contentKey = $this->getProviderResponseFields();
        $decision   = null;
        $phone      = null;
        $keyword    = null;
        $errors     = [];

        if (array_key_exists($contentKey['smsContent'], $content) && !empty($content[$contentKey['smsContent']])) {

            $smsContent = trim($content[$contentKey['smsContent']]);
            if (preg_match('/\D{1}\d{3}/', $smsContent) && (strlen($smsContent) == 5 || strlen($smsContent) == 4)) {
                $keyword = (int) filter_var($smsContent, FILTER_SANITIZE_NUMBER_INT);
                $decision = strtoupper($smsContent[0]);
            } else {
                $errors[] = $this->container->getParameter('booking_process_errors')['keyword_format'];
            }
        }

        if (!array_key_exists($contentKey['smsFromNumber'], $content) || empty($content[$contentKey['smsFromNumber']])) {
            $errors[] = $this->container->getParameter('booking_process_errors')['from_number_value_not_found'];
        } else {
            $phone = $content[$contentKey['smsFromNumber']];
            if (!strpos("+", $phone)) {
                $phone = "+".$phone;
            }
        }

        if (empty($decision)) {
            $errors[] = $this->container->getParameter('booking_process_errors')['decision_letter_not_parsed'];
        }

        if (empty($phone)) {
            $errors[] = $this->container->getParameter('booking_process_errors')['phone_number_not_parsed'];
        }

        if (empty($keyword)) {
            $errors[] = $this->container->getParameter('booking_process_errors')['keyword_not_parsed'];
        }

        if (!in_array($decision, self::$allowedSmsResponseDecisionChars)) {
            $errors[] = sprintf($this->container->getParameter('booking_process_errors')['incorrect_decision_letter'], $decision);
        }


        /** @var Teacher $teacher */
        $teacher = $this->getTeacherEntityByPhoneNumber($phone);
        /** @var Booking $booking */
        $booking = $this->getBookingEntityByKeyword($keyword);

        if ($booking == null) {
            $errors[] = sprintf(
                $this->container->getParameter('booking_process_errors')['booking_entity_not_found'],
                $keyword
            );
        }


        if ($teacher == null) {
            $errors[] = sprintf(
                $this->container->getParameter('booking_process_errors')['teacher_entity_not_found'],
                $phone
            );
        }

        if (count($errors)>0) {
            return false;
        } else {
            /** @var SmsRequestList $smsRequest */
            $smsRequest = $smsRequestRepository->findOpenRequestByBookingAndTeacher($teacher, $booking);

            if ($smsRequest == null) {

                /** @var SmsRequestList $closedSmsRequest */
                $closedSmsRequest = $smsRequestRepository->findCompletedRequestByBookingAndTeacher($teacher, $booking);

                if (!empty($closedSmsRequest)) {
                    $lockFile = vsprintf("%s/%s.lock", [$this->container->getParameter('sms_lock_path'), $closedSmsRequest->getId()]);

                    $fp = fopen($lockFile, "a+");
                    if(flock($fp, LOCK_EX)) {
                        $result = $this->processSmsRequestDecisionWhenBookingClosed($closedSmsRequest, $decision);
                        touch($lockFile);
                    }
                    flock($fp, LOCK_UN);
                    fclose($fp);

                    return true;
                }

                $errors[] = sprintf(
                    $this->container->getParameter('booking_process_errors')['sms_entity_not_found'],
                    $teacher->getId(),
                    $booking->getId()
                );
                return false;
            }
        }



        $result = null;

        $lockFile = vsprintf("%s/%s.lock", [$this->container->getParameter('sms_lock_path'), $smsRequest->getId()]);
        $fp = fopen($lockFile, "a+");
        if(flock($fp, LOCK_EX)) {
            $result = $this->processSmsRequestDecision($smsRequest, $decision);
            touch($lockFile);
        }
        flock($fp, LOCK_UN);
        fclose($fp);

        return $result;
    }

    /**
     * @return array
     */
    public function getOpenRequests()
    {
        $openRequests = $this->entityManager
            ->getRepository('ClassCoverBookingBundle:Booking')->findBy([
                'bookingStatus' => Booking::$allowedBookingTeacherStatuses
        ]);

        return $openRequests;
    }

    /**
     * @param SmsRequestList $smsRequest
     * @return bool
     */
    protected function checkAndRemoveExclusion(SmsRequestList $smsRequest)
    {
        $timeStart = $smsRequest->getBooking()->getDateTimeStart();
        $timeEnd = $smsRequest->getBooking()->getDateTimeEnd();

        /** @var TeacherExclusionRepository $exclusionRepository */
        $exclusionRepository = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherExclusion');
        $exclusionData = $exclusionRepository->getExclusionsDataOnInterval($timeStart, $timeEnd, $smsRequest->getTeacher());

        if (count($exclusionData)) {

            $bookingRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:Booking');
            $exclusionData = array_shift($exclusionData);

            $bookingForExclusion = $bookingRepo->find($exclusionData['booking_id']);

            if ($bookingForExclusion !== null) {

                $ez = $exclusionRepository->findBy([
                    'booking' => $bookingForExclusion,
                    'teacher' => $smsRequest->getTeacher(),
                    'school' => $smsRequest->getSchool()]
                );

                foreach ($ez as $exclusion) {
                    $this->entityManager->remove($exclusion);
                    $this->entityManager->flush();
                }
            }
        }

        return true;
    }

    protected function handleClosedAcceptedBooking()
    {

    }

    /**
     * @param SmsRequestList $smsRequest
     * @param $decision
     * @return bool
     */
    protected function processSmsRequestDecisionWhenBookingClosed(SmsRequestList $smsRequest, $decision)
    {
        if ($decision == self::SMS_DECISION_BOOKING_REQUEST_ACCEPT) {
            /** @var TeacherExclusionRepository $exclusionRepository */
            $exclusionRepository = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherExclusion');
            $exclusions = $exclusionRepository->findBy(['booking' => $smsRequest->getBooking()]);

            if (!empty($exclusions)) {
                foreach($exclusions as $exclusion) {
                    $this->entityManager->remove($exclusion);
                    $this->entityManager->flush();
                }
            }

            $smsRequest->setResponse('accepted');
            $smsRequest->setResponseTime(new \DateTime('NOW'));
            $this->entityManager->flush($smsRequest);

            $this->sendMessage($smsRequest->getToNumber(), "Thanks for your reply, however unfortunately this shift has already been taken.");
        }

        return false;
    }
    /**
     * @param SmsRequestList $smsRequest
     * @param $decision
     * @return bool
     */
    public function processSmsRequestDecision(SmsRequestList $smsRequest, $decision)
    {
        $processed = false;

        if (!in_array($smsRequest->getBooking()->getBookingStatus(), Booking::$allowedBookingTeacherStatuses)) {
            return false;
        }

        if ($decision == self::SMS_DECISION_BOOKING_REQUEST_ACCEPT) {
            
            if ($smsRequest->getParsed() == -3) {
                $this->checkAndRemoveExclusion($smsRequest);
            }

            $this->setTeacherBooked(
                $smsRequest->getBooking(),
                $smsRequest->getTeacher(),
                $smsRequest->getSchool()
            );

            $smsRequest->setResponse('booked');
            $smsRequest->setResponseTime(new \DateTime());

            $smsRequest->getBooking()->incrementBookedTeacers();

            $this->entityManager->persist($smsRequest);
            $this->entityManager->flush();

            $bookingDate = $smsRequest->getBooking()->getDateTimeStart()->format('d M Y');
            $shiftTime = $smsRequest->getBooking()->getDateTimeStart()->format("H:i") . ' - ' .
                            $smsRequest->getBooking()->getDateTimeEnd()->format("H:i");

            $bookingDateMail = $smsRequest->getBooking()->getDateTimeStart()->format('D, d/m');
            if ($smsRequest->getBooking()->getDateTimeStart()->format('Y-m-d') !== $smsRequest->getBooking()->getDateTimeEnd()->format('Y-m-d')) {
                $bookingDate = $smsRequest->getBooking()->getDateTimeStart()->format('d M Y') . ' - ' . $smsRequest->getBooking()->getDateTimeEnd()->format('d M Y');
                $bookingDateMail = $smsRequest->getBooking()->getDateTimeStart()->format('D, d/m') . ' - ' . $smsRequest->getBooking()->getDateTimeEnd()->format('D, d/m');
            }

            $schoolEmail = strtolower($smsRequest->getSchool()->getEmail());
            $casualEmail = strtolower($smsRequest->getTeacher()->getEmail());

            /** @var AdditionalUsers $userCreated */
            $userCreated = $this->entityManager->getRepository('ClassCoverSchoolBundle:AdditionalUsers')->findOneBy([
                'user' => $smsRequest->getBooking()->getUser(),
                'school' => $smsRequest->getSchool()
            ]);

            $subject = "Booking Confirmation " . $bookingDateMail . ", " . $shiftTime;

            if ($userCreated == null) {
                $userCreated = $smsRequest->getSchool()->getName();
            } else {
                $userCreated = $userCreated->getFirstname() . ' ' . $userCreated->getSurname();
            }

            $processed = true;

            // Booking Confirmation Email To School
            $schoolEmailMessage = \Swift_Message::newInstance()
                ->setSubject("Casual " . $subject)
                ->setFrom('goShift@classcover.com.au')
                ->setTo($schoolEmail)
                ->addBcc('reiby@classcover.com.au')
                ->setPriority(1)
                ->setBody(
                    $this->container->get('templating')->render('ClassCoverBookingBundle:Emails:centre-booking-confirmation.html.twig', [
                        'bookingDate' => $bookingDate,
                        'shiftType' => $smsRequest->getBooking()->getShiftType(),
                        'shiftTime' => $shiftTime,
                        'bookedCasualName' => $smsRequest->getTeacher()->getFirstName() . ' ' . $smsRequest->getTeacher()->getSurname(),
                        'bookedBy' => $userCreated,
                        'bookingRef' => $smsRequest->getBooking()->getId()
                    ]),
                    'text/html'
                );

            $casualMailMessage = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom('goShift@classcover.com.au')
               // ->addCc('Sekhar.Govindasamy@djj.nsw.gov.au')
                ->setTo($casualEmail)
                ->setPriority(1)
                ->setBody(
                    $this->container->get('templating')->render('ClassCoverBookingBundle:Emails:casual-booking-confirmation.html.twig', [
                        'casualFirstName' => $smsRequest->getTeacher()->getFirstName(),
                        'bookingDate' => $bookingDate,
                        'shiftType' => $smsRequest->getBooking()->getShiftType(),
                        'shiftTime' => $shiftTime,
                        'schoolName' => $smsRequest->getBooking()->getSchool()->getName(),
                        'schoolContactNumber' => $smsRequest->getBooking()->getSchool()->getPhone(),
                        'bookingRef' => $smsRequest->getBooking()->getId()
                    ]),
                    'text/html'
                );

            try {
                $this->container->get('mailer')->send($schoolEmailMessage);
                $this->container->get('mailer')->send($casualMailMessage);
            } catch(\Exception $e) {
                $this->logger->error($e->getMessage());
            }

        } elseif ($decision == self::SMS_DECISION_BOOKING_REQUEST_DECLINE) {

            if ($smsRequest->getParsed() != -3) {
                $this->setTeacherExcluded(
                    $smsRequest->getBooking(),
                    $smsRequest->getTeacher(),
                    $smsRequest->getSchool()
                );
            }

            $smsRequest->setResponse('declined');
            $smsRequest->setResponseTime(new \DateTime());

            $this->entityManager->persist($smsRequest);
            $this->entityManager->flush();

            $processed = true;

        }
        
        if ($processed) {

            $this->checkAndModifyBookingState($smsRequest, $decision);

            return true;
        }

        return false;
    }

    /**
     * @param SmsRequestList $smsRequest
     * @param $decision
     */
    protected function checkAndModifyBookingState(SmsRequestList $smsRequest, $decision)
    {
        // Clear offers
        $this->clearTeacherOfferingCells(
            $smsRequest->getBooking(),
            $smsRequest->getTeacher(),
            $smsRequest->getSchool()
        );

        /** @var SmsRequestListRepository $smsRequestsRepo */
        $smsRequestsRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:SmsRequestList');

        // If all requested teachers were booked
        if ($smsRequest->getBooking()->getTeachersBooked() == $smsRequest->getBooking()->getTeachersRequested()) {

            $noResponseList = $smsRequestsRepo->getOpenNoResponseRequests($smsRequest->getBooking());

            if (count($noResponseList)>0) {
                /** @var SmsRequestList $item */
                foreach ($noResponseList as $item) {

                    $this->clearTeacherOfferingCells($item->getBooking(), $item->getTeacher(), $item->getSchool());

                    if ($item->getSmsStatus() == 'sent') {
                        $item->setResponse('unanswered');
                        $this->setTeacherExcluded($item->getBooking(), $item->getTeacher(), $item->getSchool());
                    } else {
                        $item->setSmsStatus('cancelled');
                        $item->setParsed(2);
                    }

                    $this->entityManager->flush($item);
                }
            }

            $smsRequest->getBooking()->setBookingStatus(Booking::BOOKING_STATUS_COMPLETED);
            $this->entityManager->flush($smsRequest->getBooking());
        } else {
            $yesResponseCount = $smsRequestsRepo->getAcceptedResponsesCount($smsRequest->getBooking());

            if ($decision == self::SMS_DECISION_BOOKING_REQUEST_DECLINE && $yesResponseCount < $smsRequest->getBooking()->getTeachersRequested()) {
                $this->forceRequestNextOne($smsRequest);
            }
        }

        $sms = false;
        $schoolAbbrName = $smsRequest->getBooking()->getSchool()->getShortName();
        $bookingDateStart = $smsRequest->getBooking()->getDateTimeStart();
        $bookingDateEnd = $smsRequest->getBooking()->getDateTimeEnd();

        if ($decision == self::SMS_DECISION_BOOKING_REQUEST_ACCEPT) {
             if (in_array($smsRequest->getBooking()->getBookingStatus(), Booking::$allowedSmsConfirmationStatuses)) {

                $smsFormat = $this->container->getParameter('booking_booked_response_sms_format');
                if ($bookingDateStart->format("Y-m-d") == $bookingDateEnd->format("Y-m-d") ) {
                    $periodText = "on ". $bookingDateStart->format("j/n");
                } else {
                    $periodText = "from ". $bookingDateStart->format("j/n"). ' to '. $bookingDateEnd->format("j/n");
                }
                $periodText.= " " . $bookingDateStart->format("H:i") . '-' . $bookingDateEnd->format("H:i");

                $sms = sprintf($smsFormat, $schoolAbbrName, $periodText);
            }
        } else if ($decision == self::SMS_DECISION_BOOKING_REQUEST_DECLINE) {
            $smsFormat = $this->container->getParameter('booking_declined_response_sms_format');
            $sms = sprintf($smsFormat, $schoolAbbrName);
        }

        if ($sms !== false) {
           $this->sendMessage($smsRequest->getToNumber(), $sms);
        }
    }

    /**
     * @param SmsRequestList $currentSmsRequest
     * @return bool
     */
    public function forceRequestNextOne(SmsRequestList $currentSmsRequest)
    {
        /** @var SmsRequestListRepository $smsRequestsRepo */
        $smsRequestsRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:SmsRequestList');

        /** @var SmsRequestList $nextScheduled */
        $nextScheduled = $smsRequestsRepo->getNextSmsRequest($currentSmsRequest);

        if ($nextScheduled != false) {

            $sendTime = new \DateTime();
            $sendTime->setTime($sendTime->format("H"), $sendTime->format('i'), 0);

            $nextScheduled->setSendTime($sendTime);
            $nextScheduled->setDelayMinutes(0);
            $parsed = $this->processQueuedSmsRequest($nextScheduled);

            if ($parsed === -2) {
                return false;
            }

            while ($parsed == false) {
                if($this->forceRequestNextOne($nextScheduled)) {
                    $parsed = true;
                }
                return false;
            }

            $forward = $smsRequestsRepo->getNextSmsRequests($nextScheduled);

            if (count($forward)) {
                /** @var SmsRequestList $fwd */
                foreach ($forward as $fwd) {
                    $sendTime->modify("+" . $fwd->getDelayMinutes() . " minutes");
                    $fwd->setSendTime($sendTime);
                    $this->entityManager->persist($fwd);
                    $this->entityManager->flush();
                }

            }
            return true;
        } else {
            /** @var KernelInterface $kernel */
            $kernel = $this->container->get('kernel');
            $application = new Application($kernel);
            $application->setAutoExit(false);

            $input = new ArrayInput(array(
                'command' => 'ez:check',
                '--booking' => $currentSmsRequest->getBooking()->getId(),
            ));
            $output = new NullOutput();
            $application->run($input, $output);
        }

        return false;
    }

    /**
     * @param Booking $booking
     * @param Teacher $teacher
     * @param School $school
     * @return mixed
     */
    public function clearTeacherOfferingCells(Booking $booking, Teacher $teacher, School $school)
    {
        /** @var TeacherShiftOfferRepository $teacherShiftOfferRepo */
        $teacherShiftOfferRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherShiftOffer');
        return $teacherShiftOfferRepo->deleteTeacherOfferingCells($booking, $teacher, $school);
    }

    /**
     * @param Booking $booking
     * @param Teacher $teacher
     * @param School $school
     * @return mixed
     */
    public function clearTeacherBookingCells(Booking $booking, Teacher $teacher, School $school)
    {
        /** @var TeacherBookingRepository $teacherBookingRepository */
        $teacherBookingRepository = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherBooking');
        return $teacherBookingRepository->deleteTeacherBookingCells($booking, $teacher, $school);
    }

    /**
     * @param Booking $booking
     * @param Teacher $teacher
     * @param School $school
     * @param null $integrity
     * @return bool
     */
    public function setTeacherBooked(Booking $booking, Teacher $teacher, School $school, $integrity = null)
    {
        $periods = $this->getTeacherStatusPeriods(
            $booking->getDateTimeStart(),
            $booking->getDateTimeEnd(),
            self::TEACHER_STATUS_DEFAULT_ADJACENT_HOURS
        );

        if (empty($integrity)) {
            $integrityId = microtime();
        } else {
            $integrityId = $integrity;
        }
        
        foreach ($periods as $period) {
            /** @var TeacherBooking $bookingCell */
            $bookingCell = new TeacherBooking();
            $bookingCell->setDate($period['dateTime']);
            $bookingCell->setTime($period['dateTime']);
            $bookingCell->setSchool($school);
            $bookingCell->setTeacher($teacher);
            $bookingCell->setIntegrityId($integrityId);
            $bookingCell->setAdjacentInterval($period['adjacent']);
            $bookingCell->setBooking($booking);
            $this->entityManager->persist($bookingCell);
            $this->entityManager->flush();
        }

        $startTimestamp = $booking->getDateTimeStart()->getTimestamp();
        $endTimestamp = $booking->getDateTimeEnd()->getTimestamp();
        $duration = ($endTimestamp-$startTimestamp)/3600;

        /** @var TeacherOvertimeRepository $overtimeRepo */
        $overtimeRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:TeacherOvertime');
        $overtimeRepo->addOverTime($teacher, $duration);

        return true;
    }

    /**
     * @param Booking $booking
     * @param Teacher $teacher
     * @param School $school
     * @return bool
     */
    public function setTeacherExcluded(Booking $booking, Teacher $teacher, School $school)
    {
        $periods = $this->getTeacherStatusPeriods(
            $booking->getDateTimeStart(),
            $booking->getDateTimeEnd(),
            self::TEACHER_STATUS_DEFAULT_ADJACENT_HOURS
        );

        $integrityId = microtime();

        foreach ($periods as $period) {

            /** @var TeacherExclusion $bookingCell */
            $exclusionCell = new TeacherExclusion();
            $exclusionCell->setDate($period['dateTime']);
            $exclusionCell->setTime($period['dateTime']);
            $exclusionCell->setSchool($school);
            $exclusionCell->setTeacher($teacher);
            $exclusionCell->setIntegrityId($integrityId);
            $exclusionCell->setAdjacentInterval($period['adjacent']);
            $exclusionCell->setBooking($booking);
            $this->entityManager->persist($exclusionCell);
        }

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Booking $booking
     * @param Teacher $teacher
     * @param School $school
     * @return bool
     */
    public function setTeacherOffered(Booking $booking, Teacher $teacher, School $school)
    {
        $periods = $this->getTeacherStatusPeriods(
            $booking->getDateTimeStart(),
            $booking->getDateTimeEnd(),
            self::TEACHER_STATUS_DEFAULT_ADJACENT_HOURS
        );

        $integrityId = microtime();

        foreach ($periods as $period) {

            /** @var TeacherShiftOffer $bookingCell */
            $offerCell = new TeacherShiftOffer();
            $offerCell->setDate($period['dateTime']);
            $offerCell->setTime($period['dateTime']);
            $offerCell->setSchool($school);
            $offerCell->setTeacher($teacher);
            $offerCell->setIntegrityId($integrityId);
            $offerCell->setAdjacentInterval($period['adjacent']);
            $offerCell->setBooking($booking);
            $this->entityManager->persist($offerCell);
        }

        if ($booking->getBookingStatus() == Booking::BOOKING_STATUS_NEW) {
            $booking->setBookingStatus(Booking::BOOKING_STATUS_PENDING);
            $this->entityManager->persist($booking);
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param Teacher $teacher
     * @param School|null $school
     * @param \DateTime $from
     * @param \DateTime $to
     * @return bool
     */
    public function setTeacherUnavailable(Teacher $teacher, School $school = null, \DateTime $from, \DateTime $to)
    {
        $periods = $this->getTeacherStatusPeriods(
            $from,
            $to,
            self::TEACHER_STATUS_DEFAULT_NO_ADJACENT_HOURS
        );

        $integrityId = microtime() . $teacher->getId() . $school->getId() . $from->format("ymdhis");

        foreach ($periods as $period) {

            /** @var TeacherUnavailability $bookingCell */
            $uaCell = new TeacherUnavailability();
            $uaCell->setDate($period['dateTime']);
            $uaCell->setTime($period['dateTime']);

            if ($school instanceof School) {
                $uaCell->setSchool($school);
            }

            $uaCell->setTeacher($teacher);
            $uaCell->setIntegrityId($integrityId);
            $uaCell->setAdjacentInterval($period['adjacent']);

            $this->entityManager->persist($uaCell);
        }

        $this->entityManager->flush();

        return $integrityId;
    }

    /**
     * @param \DateTime $sendTime
     * @return array
     */
    public function getQueuedSmsRequests(\DateTime $sendTime)
    {
        /** @var SmsRequestListRepository $smsRequests */
        $smsRequests = $this->entityManager->getRepository('ClassCoverBookingBundle:SmsRequestList');
        /** @var array $requests */
        $requests = $smsRequests->findNewRequests($sendTime);

        return $requests;
    }

    /**
     * @param \DateTime|null $time
     */
    public function processSendList(\DateTime $time = null)
    {
        $refTime = ($time == null) ? new \DateTime() : $time;
        $refTime->setTime($refTime->format('H'), $refTime->format('i'), 0);

        $openBookings = $this->entityManager
            ->getRepository('ClassCoverBookingBundle:Booking')
            ->findBy(['bookingStatus' => Booking::$allowedBookingTeacherStatuses]);

        /** @var SmsRequestListRepository $smsRequestsRepo */
        $smsRequestsRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:SmsRequestList');

        /** @var Booking $booking */
        foreach ($openBookings as $booking) {
            $requests = $smsRequestsRepo->findBy(['booking' => $booking]);
            /** @var SmsRequestList $smsRequest */
            foreach ($requests as $smsRequest) {

                if ($smsRequest->getSkipped() > 0 || $smsRequest->getParsed() > 0) {
                    continue;
                }

                if ($refTime->format("Y-m-d H:i:00") == $smsRequest->getSendTime()->format("Y-m-d H:i:00")) {
                    $processResult = $this->processQueuedRecord($smsRequest);
                    $this->logger->info("Booking id : " . $booking->getId());
                    $this->logger->info("\t Sms req id : " . $smsRequest->getId() . " Result : " . $processResult);

                } else {
                    continue;
                }
            }
        }
    }

    /**
     * @param \DateTime|null $time
     * @return ArrayCollection
     */
    public function getSendListSpecificTime(\DateTime $time = null)
    {
        $refTime = ($time == null) ? new \DateTime() : $time;
        $refTime->setTime($refTime->format('H'), $refTime->format('i'), 0);

        $smsRequests = $this->getQueuedSmsRequests($refTime);

        return $smsRequests;
    }

    public function processQueuedRecord(SmsRequestList $sr)
    {
        // 100 - Processed ok.
        // -1 Unabled to be processed, skipped or booking not new or progress.
        // 200 - Processed but postponed because casual it's on shift and the system has to be paused.

        if (!in_array($sr->getBooking()->getBookingStatus(), Booking::$allowedBookingTeacherStatuses)) {
            return -1;
        }

        /** @var SmsRequestListRepository $smsRequestsRepo */
        $smsRequestsRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:SmsRequestList');

        /** @var array $uaMessages */
        $uaMessages = $this->checkTeacherAvailabilityForSmsBooking(
            $sr->getTeacher(),
            $sr->getBooking()->getDateTimeStart(),
            $sr->getBooking()->getDateTimeEnd(),
            $sr->getParsed()
        );

        if (count($uaMessages)>0) {

            $sr->setObservations(json_encode($uaMessages));
            $sr->setSkipped(1);
            $sr->setParsed(1);
            $this->entityManager->flush($sr);

            /** @var SmsRequestList $nextRequest */
            $nextRequest = $smsRequestsRepo->getNextSmsRequest($sr);

            if ($nextRequest !== false) {

                $overrideTime = new \DateTime();
                $overrideTime->setTime($overrideTime->format("H"), $overrideTime->format("i"), 0);

                $nextRequest->setSendTime($overrideTime);
                $this->processQueuedRecord($nextRequest);
            }

            return -1;
        }

        $shiftTime = $this->checkOnShiftStatus($sr->getTeacher());

        if ($shiftTime !== false) {
            // pause & reschedule list
            $restartTime = clone $shiftTime;
            $restartTime->modify("+5 minutes");

            $messages[] = ['label' => 'On-Shift', 'text' => sprintf("Request paused until %s", $restartTime->format("H:i"))];

            $sr->setSendTime($restartTime);
            $sr->setPostponed(1);
            $sr->setObservations(json_encode($messages));

            $this->entityManager->flush($sr);

            $forward = $smsRequestsRepo->getNextSmsRequests($sr);

            if (count($forward)) {
                /** @var SmsRequestList $fwd */
                foreach ($forward as $fwd) {
                    $restartTime->modify("+" . $fwd->getDelayMinutes() . " minutes");
                    $fwd->setSendTime($restartTime);
                    $this->entityManager->persist($fwd);
                    $this->entityManager->flush();
                }
            }

            return 200;
        }

        $smsMessageId = $this->sendMessage($sr->getToNumber(), $sr->getSmsText());

        // send sms request
        $sentAt = new \DateTime();
        $sr->setSmsStatus('sent');
        $sr->setSentAt($sentAt);
        $sr->setNetworkSmsId($smsMessageId);
        $sr->setParsed(1);

        $this->entityManager->flush($sr);

        $this->setTeacherOffered($sr->getBooking(), $sr->getTeacher(), $sr->getSchool());

        return 100;
    }

    /**
     * @param SmsRequestList $sr
     * @return bool|SmsRequestList
     */
    public function processQueuedSmsRequest(SmsRequestList $sr)
    {
        if (!in_array($sr->getBooking()->getBookingStatus(), Booking::$allowedBookingTeacherStatuses)) {
            return false;
        }

        /** @var SmsRequestListRepository $smsRequestsRepo */
        $smsRequestsRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:SmsRequestList');

        /** @var array $uaMessages */
        $uaMessages = $this->checkTeacherAvailabilityForSmsBooking(
            $sr->getTeacher(),
            $sr->getBooking()->getDateTimeStart(),
            $sr->getBooking()->getDateTimeEnd(),
            $sr->getParsed()
        );

        $parseCode = 1;

        if (count($uaMessages)>0) {
            if ($sr->getParsed() == self::SMS_EXCLUDED_PARSED_CLOSED) {
                $parseCode = self::SMS_EXCLUDED_PARSED_CLOSED;
            }

            $sr->setObservations(json_encode($uaMessages));
            $sr->setSkipped(1);
            $sr->setParsed($parseCode);
            $this->entityManager->flush($sr);

            return false;
        }

        /** @var bool|\DateTime $shift */
        $shiftTime = $this->checkOnShiftStatus($sr->getTeacher());

        if ($shiftTime !== false) {
            // pause & reschedule list
            $restartTime = clone $shiftTime;
            $restartTime->modify("+5 minutes");

            $messages[] = ['label' => 'On-Shift', 'text' => sprintf("Request paused until %s", $restartTime->format("H:i"))];

            if ($sr->getParsed() == -2) {
                $parseCode = -3;
            }

            $sr->setSendTime($restartTime);
            $sr->setPostponed($parseCode);
            $sr->setObservations(json_encode($messages));

            $this->entityManager->flush($sr);

            $forward = $smsRequestsRepo->getNextSmsRequests($sr);

            if (count($forward)) {
                /** @var SmsRequestList $fwd */
                foreach ($forward as $fwd) {
                    $restartTime->modify("+" . $fwd->getDelayMinutes() . " minutes");
                    $fwd->setSendTime($restartTime);
                    $this->entityManager->persist($fwd);
                    $this->entityManager->flush();
                }
            }

            return -2;
        }

        $this->sendMessage($sr->getToNumber(), $sr->getSmsText());

        // send sms request
        $sentAt = new \DateTime();

        if ($sr->getParsed() == -2) {
            $parseCode = -3;
        }

        $sr->setSmsStatus('sent');
        $sr->setSentAt($sentAt);
        $sr->setNetworkSmsId('X1'.time());
        $sr->setParsed($parseCode);

        $this->entityManager->flush($sr);

        $noResponseCount = $smsRequestsRepo->getNoResponsesCount($sr->getBooking());
        $declinedResponseCount = $smsRequestsRepo->getDeclinedResponsesCount($sr->getBooking());
        
        $this->setTeacherOffered($sr->getBooking(), $sr->getTeacher(), $sr->getSchool());

        return $sr;
    }

    /**
     * @param $to
     * @param $text
     * @return mixed
     */
    public function sendMessage($to, $text)
    {
        $env = $this->container->get('kernel')->getEnvironment();

        $messageId = uniqid(time());

        if (!in_array($env, ['dev', 'test', 'stage'])) {

            $twilio = $this->container->get('twilio.api');

            $smsMessage = $twilio->account->messages->sendMessage(
                $this->container->getParameter('twilio_number_from'),
                $to,
                $text
            );

            $messageId = $smsMessage->sid;
        }

        $smsHistory = new SmsHistory();
        $smsHistory->setSmsId($messageId);
        $smsHistory->setSmsText($text);
        $smsHistory->setSmsTo($to);
        $smsHistory->setAdditionalInfo(json_encode(['network' => 'twilio']));

        $this->entityManager->persist($smsHistory);
        $this->entityManager->flush();

        return $messageId;
    }

    /**
     * @param Teacher $teacher
     * @return bool|\DateTime
     */
    protected function checkOnShiftStatus(Teacher $teacher)
    {
        return (new IsOnShift($this->container, $teacher))->evaluate();
    }

    /**
     * @param Teacher $teacher
     * @param \DateTime $from
     * @param \DateTime $to
     * @param bool|false $ignore
     * @return array
     */
    protected function checkTeacherAvailabilityForSmsBooking(Teacher $teacher, \DateTime $from, \DateTime $to, $ignore = false)
    {
        $uaMessages = [];

        $isUnavailable  = (new IsUnavailable($this->container, $teacher))->check($from, $to);
        $isBooked       = (new IsBooked($this->container, $teacher))->check($from, $to);
        $isOffered      = (new IsOffered($this->container, $teacher))->check($from, $to);
        //$isExcluded     = (new IsExcluded($this->container, $teacher))->check($from, $to);

        /*
        if ($ignore == -2) {
            $isExcluded = false;
        }*/
        $isExcluded = false;


        if ($isUnavailable) {
            $uaMessages[] = ['label' => 'UA', 'text' => '-'];
        }

        if ($isBooked) {
            $uaMessages[] = ['label' => 'Ineligible', 'text' => 'Booked within 8 hours'];
        }

        if ($isOffered) {
            $uaMessages[] = ['label' => 'Ineligible', 'text' => 'Existing request within 8 hours'];
        }

        if ($isExcluded) {
            $uaMessages[] = ['label' => 'Excluded', 'text' => '-'];
        }

        return $uaMessages;
    }

    /**
     * @param $keywordNumber
     * @return bool
     */
    protected function getBookingEntityByKeyword($keywordNumber)
    {

        /** @var EntityRepository $bookingRepository */
        $bookingRepository = $this->entityManager->getRepository('ClassCoverBookingBundle:Booking');

        /** @var EntityRepository $keywordCounterRepo */
        $keywordCounterRepo = $this->entityManager->getRepository('ClassCoverBookingBundle:KeywordCounter');
        /** @var KeywordCounter $keyword */
        $keyword = $keywordCounterRepo->findOneByKeywordPostfixNumber($keywordNumber);
        
        if ($keyword == null) {
            return false;
        }

        /** @var Booking $booking */
        $booking = $bookingRepository->findOneByKeyword($keyword);

        if ($booking == null) {
            return false;
        }

        /*if (!in_array($booking->getBookingStatus(), Booking::$allowedBookingTeacherStatuses)) {
            return false;
        }*/

        return $booking;
    }

    /**
     * @param $phoneNumber
     * @return bool|Teacher
     */
    protected function getTeacherEntityByPhoneNumber($phoneNumber)
    {
        /** @var TeacherRepository $teacherRepository */
        $teacherRepository = $this->entityManager->getRepository('ClassCoverCyoBundle:Teacher');
        /** @var Teacher $teacher */
        $teacher = $teacherRepository->findOneByPhone($phoneNumber);

        if ($teacher == null) {
            return false;
        }

        return $teacher;
    }

    /**
     * @return array
     */
    protected function getProviderResponseFields()
    {
        return [
          'smsContent'      => 'Body',
          'smsFromNumber'   => 'From'
        ];
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @param int $adjacentHours
     * @return array
     */
    protected function getTeacherStatusPeriods(\DateTime $start, \DateTime $end, $adjacentHours = self::TEACHER_STATUS_DEFAULT_NO_ADJACENT_HOURS) {

        $modifierValue = self::TEACHER_STATUS_INTERVAL;

        $before = clone $start;
        $after = clone $end;

        $before->modify("-{$adjacentHours} hours");
        $after->modify("+{$adjacentHours} hours");

        $periods = [];

        while($before<$after) {

            $i = ['dateTime' => clone $before, 'adjacent' => 0];

            if ($before->getTimestamp() < $start->getTimestamp() || $before->getTimestamp() >= $end->getTimestamp()){
                $i['adjacent'] = 1;
            }
            $periods[] = $i;

            $before->modify("+" . $modifierValue);
        }

        return $periods;
    }

    /**
     * @param array $messages
     * @param array $errors
     * @param null|Booking $entity
     * @return \stdClass
     */
    protected function getProcessingStatus(array $messages = array(), array $errors = array(), $entity = null) {
        $obj = new \stdClass();
        $obj->status = count($errors) ? false : true;
        $obj->errors = $errors;
        $obj->messages = $messages;
        $obj->entity = $entity;
        return $obj;
    }

}