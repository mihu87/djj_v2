<?php
namespace ClassCover\BookingBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

use ClassCover\BookingBundle\Entity\RequestLog;
use Doctrine\ORM\EntityManager;

/**
 * Class InterceptorService
 * @package ClassCover\BookingBundle\Services
 */
class InterceptorService
{
    const TYPE_POST = 'POST';
    const TYPE_GET = 'GET';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $post
     * @param array $get
     * @param array $attributes
     * @param string $network
     * @return bool
     */
    public function recordIncomingRequest(array $post = [], array $get = [], array $attributes = [], $network = RequestLog::GATEWAY_UNKNOWN)
    {
        $doctrine = $this->container->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        $requestLog = new RequestLog();
        $requestLog->setNetwork($network);
        $requestLog->setRequest($post);
        $requestLog->setQuery($get);
        $requestLog->setAttributes($attributes);
        $requestLog->setWay(RequestLog::WAY_INCOMING);

        $em->persist($requestLog);
        $em->flush();

        return true;
    }

    /**
     * @param array $params
     * @param string $type
     * @return bool
     */
    public function recordOutgoingRequest(array $params = [], $type = self::TYPE_GET)
    {
        $doctrine = $this->container->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        $requestLog = new RequestLog();
        $requestLog->setNetwork(RequestLog::GATEWAY_UNKNOWN);

        switch ($type) {
            case self::TYPE_GET :
                    $requestLog->setQuery($params);
                break;
            case self::TYPE_POST :
                    $requestLog->setRequest($params);
                break;
            default :
                    $requestLog->setAttributes($params);
                break;
        }

        $requestLog->setWay(RequestLog::WAY_OUTGOING);

        $em->persist($requestLog);
        $em->flush();

        return true;
    }

}
