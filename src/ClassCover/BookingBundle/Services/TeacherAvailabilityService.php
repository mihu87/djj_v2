<?php

namespace ClassCover\BookingBundle\Services;

use ClassCover\SchoolBundle\Entity\School;
use ClassCover\SchoolBundle\Entity\RegisteredTeachers;
use ClassCover\CyoBundle\Entity\Teacher;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ClassCover\BookingBundle\Entity\TeacherUnavailabilityRepository;
use ClassCover\BookingBundle\Entity\TeacherBookingRepository;
use ClassCover\BookingBundle\Entity\TeacherExclusionRepository;
use ClassCover\BookingBundle\Entity\TeacherShiftOfferRepository;

use ClassCover\BookingBundle\Mappers\TeacherAvailabilityRepresentation;
use FOS\UserBundle\Model\UserManager;

class TeacherAvailabilityService  {

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getSchoolTeachersListWithAvailabilityByDate($schoolId, $date = null)
    {
        if (is_null($date)) {
            $date = new \DateTime();
        } else {
            $date = new \DateTime($date);
        }

        $date->setTime(0,0,0);

        $doctrine = $this->container->get('doctrine');

        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find($schoolId);

        $teachers = [];

        if ($school->getTeachers()->count()>0) {
            /** @var RegisteredTeachers $regTeacherData */

            foreach ($school->getTeachers() as $k => $regTeacherData) {

                if ($regTeacherData->getRemoved()>0) {
                    continue;
                }

                $teacher = $regTeacherData->getTeacher();
                $teacher->meritOrder = $regTeacherData->getDisplayOrder();
                $teacher->availabilityData = $this->getTeacherAvailabilityDataOnDate($date, $teacher);

                $teachers[] = $teacher;
            }
        }

        return $teachers;
    }

    public function getTeacherAvailabilityDataOnDate(\DateTime $date, Teacher $teacher)
    {
        $doctrine = $this->container->get('doctrine');

        /** @var TeacherUnavailabilityRepository $uaRepository */
        $uaRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherUnavailability');
        $uaData = $uaRepository->getUnavailableDataOnDate($date, $teacher);

        /** @var TeacherBookingRepository $bookingsRepository */
        $bookingsRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherBooking');
        $bookingsData = $bookingsRepository->getBookingsDataOnDate($date, $teacher);

        /** @var TeacherExclusionRepository $exclusionsRepository */
        $exclusionsRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherExclusion');
        $exclusionsData = $exclusionsRepository->getExclusionsDataOnDate($date, $teacher);

        /** @var TeacherShiftOfferRepository $offerRepository */
        $offerRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherShiftOffer');
        $offerData = $offerRepository->getOffersDataOnDate($date, $teacher);

        $availabilityData = array_merge($uaData, $bookingsData, $exclusionsData, $offerData);

        $representation = new TeacherAvailabilityRepresentation($availabilityData);

        return $representation->getRepresentation();

    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @param Teacher $teacher
     * @return array
     */
    public function getRepresentedTeacherUaDataByInterval(\DateTime $start, \DateTime $end, Teacher $teacher)
    {
        $doctrine = $this->container->get('doctrine');

        /** @var TeacherUnavailabilityRepository $uaRepository */
        $uaRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherUnavailability');
        $data = $uaRepository->getStringUaDataOnInterval($start, $end, $teacher);

        return $data;
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @param Teacher $teacher
     * @return mixed
     */
    public function getRepresentedTeacherBookingDataByInterval(\DateTime $start, \DateTime $end, Teacher $teacher)
    {
        $doctrine = $this->container->get('doctrine');

        /** @var TeacherBookingRepository $bookingRepository */
        $bookingRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherBooking');
        $data = $bookingRepository->getStringBookingDataOnInterval($start, $end, $teacher);

        return $data;
    }


    /**
     * @return mixed
     */
    protected function getCurrentLoggedUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @param $date
     * @param $mask
     * @param Teacher $teacher
     * @return bool
     */
    public function editRepresentedAvailability($date, $mask, Teacher $teacher)
    {
        $doctrine = $this->container->get('doctrine');

        /** @var TeacherUnavailabilityRepository $uaRepository */
        $uaRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherUnavailability');
        $data = $uaRepository->editAvailabilityByString($date, $mask, $teacher);

        return $data;
    }
}