<?php

namespace ClassCover\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SmsHistory
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\BookingBundle\Entity\SmsHistoryRepository")
 */
class SmsHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $smsId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $smsTo;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $smsText;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $additionalInfo;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set smsId
     *
     * @param string $smsId
     *
     * @return SmsHistory
     */
    public function setSmsId($smsId)
    {
        $this->smsId = $smsId;

        return $this;
    }

    /**
     * Get smsId
     *
     * @return string
     */
    public function getSmsId()
    {
        return $this->smsId;
    }

    /**
     * Set smsTo
     *
     * @param string $smsTo
     *
     * @return SmsHistory
     */
    public function setSmsTo($smsTo)
    {
        $this->smsTo = $smsTo;

        return $this;
    }

    /**
     * Get smsTo
     *
     * @return string
     */
    public function getSmsTo()
    {
        return $this->smsTo;
    }

    /**
     * Set smsText
     *
     * @param string $smsText
     *
     * @return SmsHistory
     */
    public function setSmsText($smsText)
    {
        $this->smsText = $smsText;

        return $this;
    }

    /**
     * Get smsText
     *
     * @return string
     */
    public function getSmsText()
    {
        return $this->smsText;
    }

    /**
     * Set additionalInfo
     *
     * @param string $additionalInfo
     *
     * @return SmsHistory
     */
    public function setAdditionalInfo($additionalInfo)
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    /**
     * Get additionalInfo
     *
     * @return string
     */
    public function getAdditionalInfo()
    {
        return $this->additionalInfo;
    }
}
