<?php

namespace ClassCover\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SmsRequestList
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\BookingBundle\Entity\SmsRequestListRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SmsRequestList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\SchoolBundle\Entity\School", inversedBy="school")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
     */
    protected $school;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\CyoBundle\Entity\Teacher", inversedBy="teacher")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id", nullable=false)
     */
    protected $teacher;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\BookingBundle\Entity\Booking", inversedBy="booking")
     * @ORM\JoinColumn(name="booking_id", referencedColumnName="id", nullable=false)
     */
    protected $booking;

    /**
     * @ORM\Column(type="integer", nullable=false, options={ "default"="0" })
     */
    protected $delayMinutes;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $response;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $smsStatus;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $acceptKeyword;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $declineKeyword;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $sendTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $responseTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $sentAt;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $skipped;

    /**
     * @ORM\Column(type="integer", nullable=false, options={ "default"="0" })
     */
    protected $postponed;

    /**
     * @ORM\Column(type="integer", nullable=false, options={ "default"="0" })
     */
    protected $parsed;

    /**
     * @ORM\Column(type="integer", nullable=false, options={ "default"="0" })
     */
    protected $excluded;

    /**
     * @ORM\Column(type="integer", nullable=false, options={ "default"="0" })
     */
    protected $attempts;


    /**
     * @ORM\Column(type="string",length=1000, nullable=true)
     */
    protected $observations;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $fromNumber;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $toNumber;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $smsText;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $network;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $networkSmsId;


    /**
     * Set delayMinutes
     *
     * @param integer $delayMinutes
     *
     * @return SmsRequestList
     */
    public function setDelayMinutes($delayMinutes)
    {
        $this->delayMinutes = $delayMinutes;

        return $this;
    }

    /**
     * Get delayMinutes
     *
     * @return integer
     */
    public function getDelayMinutes()
    {
        return $this->delayMinutes;
    }

    /**
     * Set response
     *
     * @param string $response
     *
     * @return SmsRequestList
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set smsStatus
     *
     * @param string $smsStatus
     *
     * @return SmsRequestList
     */
    public function setSmsStatus($smsStatus)
    {
        $this->smsStatus = $smsStatus;

        return $this;
    }

    /**
     * Get smsStatus
     *
     * @return string
     */
    public function getSmsStatus()
    {
        return $this->smsStatus;
    }

    /**
     * Set acceptKeyword
     *
     * @param string $acceptKeyword
     *
     * @return SmsRequestList
     */
    public function setAcceptKeyword($acceptKeyword)
    {
        $this->acceptKeyword = $acceptKeyword;

        return $this;
    }

    /**
     * Get acceptKeyword
     *
     * @return string
     */
    public function getAcceptKeyword()
    {
        return $this->acceptKeyword;
    }

    /**
     * Set declineKeyword
     *
     * @param string $declineKeyword
     *
     * @return SmsRequestList
     */
    public function setDeclineKeyword($declineKeyword)
    {
        $this->declineKeyword = $declineKeyword;

        return $this;
    }

    /**
     * Get declineKeyword
     *
     * @return string
     */
    public function getDeclineKeyword()
    {
        return $this->declineKeyword;
    }

    /**
     * Set sendTime
     *
     * @param \DateTime $sendTime
     *
     * @return SmsRequestList
     */
    public function setSendTime($sendTime)
    {
        $this->sendTime = $sendTime;

        return $this;
    }

    /**
     * Get sendTime
     *
     * @return \DateTime
     */
    public function getSendTime()
    {
        return $this->sendTime;
    }

    /**
     * Set responseTime
     *
     * @param \DateTime $responseTime
     *
     * @return SmsRequestList
     */
    public function setResponseTime($responseTime)
    {
        $this->responseTime = $responseTime;

        return $this;
    }

    /**
     * Get responseTime
     *
     * @return \DateTime
     */
    public function getResponseTime()
    {
        return $this->responseTime;
    }

    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     *
     * @return SmsRequestList
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Set skipped
     *
     * @param integer $skipped
     *
     * @return SmsRequestList
     */
    public function setSkipped($skipped)
    {
        $this->skipped = $skipped;

        return $this;
    }

    /**
     * Get skipped
     *
     * @return integer
     */
    public function getSkipped()
    {
        return $this->skipped;
    }

    /**
     * Set postponed
     *
     * @param integer $postponed
     *
     * @return SmsRequestList
     */
    public function setPostponed($postponed)
    {
        $this->postponed = $postponed;

        return $this;
    }

    /**
     * Get postponed
     *
     * @return integer
     */
    public function getPostponed()
    {
        return $this->postponed;
    }

    /**
     * Set parsed
     *
     * @param integer $parsed
     *
     * @return SmsRequestList
     */
    public function setParsed($parsed)
    {
        $this->parsed = $parsed;

        return $this;
    }

    /**
     * Get parsed
     *
     * @return integer
     */
    public function getParsed()
    {
        return $this->parsed;
    }

    /**
     * Set excluded
     *
     * @param integer $excluded
     *
     * @return SmsRequestList
     */
    public function setExcluded($excluded)
    {
        $this->excluded = $excluded;

        return $this;
    }

    /**
     * Get excluded
     *
     * @return integer
     */
    public function getExcluded()
    {
        return $this->excluded;
    }

    /**
     * Set attempts
     *
     * @param integer $attempts
     *
     * @return SmsRequestList
     */
    public function setAttempts($attempts)
    {
        $this->attempts = $attempts;

        return $this;
    }

    /**
     * Get attempts
     *
     * @return integer
     */
    public function getAttempts()
    {
        return $this->attempts;
    }

    /**
     * Set observations
     *
     * @param string $observations
     *
     * @return SmsRequestList
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * Get observations
     *
     * @return string
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Set fromNumber
     *
     * @param string $fromNumber
     *
     * @return SmsRequestList
     */
    public function setFromNumber($fromNumber)
    {
        $this->fromNumber = $fromNumber;

        return $this;
    }

    /**
     * Get fromNumber
     *
     * @return string
     */
    public function getFromNumber()
    {
        return $this->fromNumber;
    }

    /**
     * Set toNumber
     *
     * @param string $toNumber
     *
     * @return SmsRequestList
     */
    public function setToNumber($toNumber)
    {
        $this->toNumber = $toNumber;

        return $this;
    }

    /**
     * Get toNumber
     *
     * @return string
     */
    public function getToNumber()
    {
        return $this->toNumber;
    }

    /**
     * Set smsText
     *
     * @param string $smsText
     *
     * @return SmsRequestList
     */
    public function setSmsText($smsText)
    {
        $this->smsText = $smsText;

        return $this;
    }

    /**
     * Get smsText
     *
     * @return string
     */
    public function getSmsText()
    {
        return $this->smsText;
    }

    /**
     * Set network
     *
     * @param string $network
     *
     * @return SmsRequestList
     */
    public function setNetwork($network)
    {
        $this->network = $network;

        return $this;
    }

    /**
     * Get network
     *
     * @return string
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * Set networkSmsId
     *
     * @param string $networkSmsId
     *
     * @return SmsRequestList
     */
    public function setNetworkSmsId($networkSmsId)
    {
        $this->networkSmsId = $networkSmsId;

        return $this;
    }

    /**
     * Get networkSmsId
     *
     * @return string
     */
    public function getNetworkSmsId()
    {
        return $this->networkSmsId;
    }

    /**
     * Set school
     *
     * @param \ClassCover\SchoolBundle\Entity\School $school
     *
     * @return SmsRequestList
     */
    public function setSchool(\ClassCover\SchoolBundle\Entity\School $school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \ClassCover\SchoolBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set teacher
     *
     * @param \ClassCover\CyoBundle\Entity\Teacher $teacher
     *
     * @return SmsRequestList
     */
    public function setTeacher(\ClassCover\CyoBundle\Entity\Teacher $teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \ClassCover\CyoBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set booking
     *
     * @param \ClassCover\BookingBundle\Entity\Booking $booking
     *
     * @return SmsRequestList
     */
    public function setBooking(\ClassCover\BookingBundle\Entity\Booking $booking)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \ClassCover\BookingBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * @ORM\PrePersist
     */
    public function setPostponedValueIfNone()
    {
        if (empty($this->postponed)) {
            $this->postponed = 0;
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setParsedValueIfNone()
    {
        if (empty($this->parsed)) {
            $this->parsed = 0;
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setAttemptsValueIfNone()
    {
        if (empty($this->attempts)) {
            $this->attempts = 0;
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setSmsStatusIfNone()
    {
        if (empty($this->smsStatus)) {
            $this->smsStatus = 'pending';
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setResponseIfNone()
    {
        if (empty($this->response)) {
            $this->response = 'none';
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setExcludedValueIfNone()
    {
        if (empty($this->excluded)) {
            $this->excluded = 0;
        }
    }
}
