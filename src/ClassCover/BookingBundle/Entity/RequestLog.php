<?php

namespace ClassCover\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExternalCallbackInterceptor
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\BookingBundle\Entity\RequestLogRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RequestLog
{

    const WAY_INCOMING = 'incoming';
    const WAY_OUTGOING = 'incoming';
    const GATEWAY_UNKNOWN = 'unknown';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $network;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $request;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $query;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $attributes;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('incoming', 'outgoing', '') NULL default ''")
     */
    protected $way;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $receivedOn;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set network
     *
     * @param string $network
     *
     * @return RequestLog
     */
    public function setNetwork($network)
    {
        $this->network = $network;

        return $this;
    }

    /**
     * Get network
     *
     * @return string
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * Set request
     *
     * @param string $request
     *
     * @return RequestLog
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return string
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set query
     *
     * @param string $query
     *
     * @return RequestLog
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get query
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set attributes
     *
     * @param string $attributes
     *
     * @return RequestLog
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Get attributes
     *
     * @return string
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set way
     *
     * @param string $way
     *
     * @return RequestLog
     */
    public function setWay($way)
    {
        $this->way = $way;

        return $this;
    }

    /**
     * Get way
     *
     * @return string
     */
    public function getWay()
    {
        return $this->way;
    }

    /**
     * Set receivedOn
     *
     * @param \DateTime $receivedOn
     *
     * @return RequestLog
     */
    public function setReceivedOn($receivedOn)
    {
        $this->receivedOn = $receivedOn;

        return $this;
    }

    /**
     * Get receivedOn
     *
     * @return \DateTime
     */
    public function getReceivedOn()
    {
        return $this->receivedOn;
    }

    /**
     * @ORM\PrePersist
     */
    public function serializeRequestValue()
    {
        $this->request = json_encode($this->request);
    }

    /**
     * @ORM\PrePersist
     */
    public function serializeQueryValue()
    {
        $this->query = json_encode($this->query);
    }

    /**
     * @ORM\PrePersist
     */
    public function serializeAttributesValue()
    {
        $this->attributes = json_encode($this->attributes);
    }

    /**
     * @ORM\PrePersist
     */
    public function setReceivedOnDateTime()
    {
        $this->receivedOn = new \DateTime();
    }
}
