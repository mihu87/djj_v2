<?php

namespace ClassCover\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TeacherAvailability
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\BookingBundle\Entity\TeacherUnavailabilityRepository")
 */
class TeacherUnavailability
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\CyoBundle\Entity\Teacher", inversedBy="teacher")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id")
     **/
    protected $teacher;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\SchoolBundle\Entity\School", inversedBy="school")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     **/
    protected $school;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\BookingBundle\Entity\Booking")
     * @ORM\JoinColumn(name="booking_id", referencedColumnName="id")
     **/
    protected $booking;

    /**
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    protected $date;

    /**
     * @ORM\Column(name="time", type="time", nullable=false)
     */
    protected $time;
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $integrityId;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $adjacentInterval;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return TeacherUnavailability
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return TeacherUnavailability
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set integrityId
     *
     * @param string $integrityId
     *
     * @return TeacherUnavailability
     */
    public function setIntegrityId($integrityId)
    {
        $this->integrityId = $integrityId;

        return $this;
    }

    /**
     * Get integrityId
     *
     * @return string
     */
    public function getIntegrityId()
    {
        return $this->integrityId;
    }

    /**
     * Set adjacentInterval
     *
     * @param integer $adjacentInterval
     *
     * @return TeacherUnavailability
     */
    public function setAdjacentInterval($adjacentInterval)
    {
        $this->adjacentInterval = $adjacentInterval;

        return $this;
    }

    /**
     * Get adjacentInterval
     *
     * @return integer
     */
    public function getAdjacentInterval()
    {
        return $this->adjacentInterval;
    }

    /**
     * Set teacher
     *
     * @param \ClassCover\CyoBundle\Entity\Teacher $teacher
     *
     * @return TeacherUnavailability
     */
    public function setTeacher(\ClassCover\CyoBundle\Entity\Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \ClassCover\CyoBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set school
     *
     * @param \ClassCover\SchoolBundle\Entity\School $school
     *
     * @return TeacherUnavailability
     */
    public function setSchool(\ClassCover\SchoolBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \ClassCover\SchoolBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set booking
     *
     * @param \ClassCover\BookingBundle\Entity\Booking $booking
     *
     * @return TeacherUnavailability
     */
    public function setBooking(\ClassCover\BookingBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \ClassCover\BookingBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }
}
