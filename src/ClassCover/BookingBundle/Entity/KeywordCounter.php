<?php

namespace ClassCover\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * KeywordCounter
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\BookingBundle\Entity\KeywordCounterRepository")
 */
class KeywordCounter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="ClassCover\BookingBundle\Entity\Booking")
     * @ORM\JoinColumn(name="booking_id", referencedColumnName="id")
     **/
    protected $booking;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $keywordPostfixNumber;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $keywordPostfixString;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keywordPostfixNumber
     *
     * @param integer $keywordPostfixNumber
     *
     * @return KeywordCounter
     */
    public function setKeywordPostfixNumber($keywordPostfixNumber)
    {
        $this->keywordPostfixNumber = $keywordPostfixNumber;

        return $this;
    }

    /**
     * Get keywordPostfixNumber
     *
     * @return integer
     */
    public function getKeywordPostfixNumber()
    {
        return $this->keywordPostfixNumber;
    }

    /**
     * Set keywordPostfixString
     *
     * @param string $keywordPostfixString
     *
     * @return KeywordCounter
     */
    public function setKeywordPostfixString($keywordPostfixString)
    {
        $this->keywordPostfixString = $keywordPostfixString;

        return $this;
    }

    /**
     * Get keywordPostfixString
     *
     * @return string
     */
    public function getKeywordPostfixString()
    {
        return $this->keywordPostfixString;
    }

    /**
     * Set booking
     *
     * @param \ClassCover\BookingBundle\Entity\Booking $booking
     *
     * @return KeywordCounter
     */
    public function setBooking(\ClassCover\BookingBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \ClassCover\BookingBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }
}
