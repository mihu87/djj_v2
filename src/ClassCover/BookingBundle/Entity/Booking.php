<?php

namespace ClassCover\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Booking
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\BookingBundle\Entity\BookingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Booking
{
    /**
     * Booking Status
     */
    const BOOKING_STATUS_NEW = 'new';
    const BOOKING_STATUS_PENDING = 'pending';
    const BOOKING_STATUS_COMPLETED = 'completed';
    const BOOKING_STATUS_UNFILLED = 'unfilled';
    const BOOKING_STATUS_DELETED = 'deleted';
    const BOOKING_STATUS_CANCELLED = 'cancelled';

    /**
     * Shift Type
     */
    const SHIFT_TYPE_MORNING = 'morning';
    const SHIFT_TYPE_EVENING = 'evening';
    const SHIFT_TYPE_SPECIAL = 'special';
    const SHIFT_TYPE_NIGHT   = 'night';

    /**
     * Booking Type
     */
    const BOOKING_TYPE_SMS = 'sms';
    const BOOKING_TYPE_MANUAL = 'manual';

    /**
     * @var array
     */
    static $allowedBookingStatuses = [
        self::BOOKING_STATUS_NEW,
        self::BOOKING_STATUS_PENDING,
        self::BOOKING_STATUS_COMPLETED,
        self::BOOKING_STATUS_UNFILLED,
        self::BOOKING_STATUS_DELETED
    ];

    /**
     * @var array
     */
    static $allowedBookingTeacherStatuses = [
        self::BOOKING_STATUS_NEW,
        self::BOOKING_STATUS_PENDING
    ];

    static $allowedSmsConfirmationStatuses = [
        self::BOOKING_STATUS_NEW,
        self::BOOKING_STATUS_PENDING,
        self::BOOKING_STATUS_COMPLETED
    ];

    /**
     * @var array
     */
    static $allowedShiftTypes = [
        self::SHIFT_TYPE_SPECIAL,
        self::SHIFT_TYPE_EVENING,
        self::SHIFT_TYPE_MORNING,
        self::SHIFT_TYPE_NIGHT
    ];

    static $allowedBookingTypes = [
        self::BOOKING_TYPE_SMS,
        self::BOOKING_TYPE_MANUAL
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\SchoolBundle\Entity\School", inversedBy="school")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
     */
    protected $school;

    /**
     * @ORM\Column(name="date_time_start", type="datetime", nullable=false)
     */
    protected $dateTimeStart;

    /**
     * @ORM\Column(name="date_time_end", type="datetime", nullable=false)
     */
    protected $dateTimeEnd;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('manual', 'sms') NOT NULL default 'sms'")
     */
    protected $bookingType;

    /**
     * @ORM\Column(name="booking_status", type="string", columnDefinition="ENUM('new', 'pending', 'completed', 'unfilled', 'deleted', 'cancelled') NOT NULL default 'new'")
     */
    protected $bookingStatus;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('morning', 'evening', 'special', 'night') NOT NULL default 'special'")
     */
    protected $shiftType;

    /**
     * @ORM\Column(type="integer", nullable=false, options={ "default"="0" })
     */
    protected $teachersRequested;

    /**
     * @ORM\Column(type="integer", nullable=false, options={ "default"="0" })
     */
    protected $teachersBooked;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\TeacherBooking", mappedBy="booking", cascade={"persist"})
     **/
    protected $intervals;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $smsText;

    /**
     * @ORM\OneToOne(targetEntity="ClassCover\BookingBundle\Entity\KeywordCounter", cascade={"persist"})
     * @ORM\JoinColumn(name="keyword_id", referencedColumnName="id")
     **/
    protected $keyword;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdOn;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\SmsRequestList", mappedBy="booking", cascade={"persist"})
     **/
    protected $requests;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     **/
    protected $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->intervals = new \Doctrine\Common\Collections\ArrayCollection();
        $this->requests = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateTimeStart
     *
     * @param \DateTime $dateTimeStart
     *
     * @return Booking
     */
    public function setDateTimeStart($dateTimeStart)
    {
        $this->dateTimeStart = $dateTimeStart;

        return $this;
    }

    /**
     * Get dateTimeStart
     *
     * @return \DateTime
     */
    public function getDateTimeStart()
    {
        return $this->dateTimeStart;
    }

    /**
     * Set dateTimeEnd
     *
     * @param \DateTime $dateTimeEnd
     *
     * @return Booking
     */
    public function setDateTimeEnd($dateTimeEnd)
    {
        $this->dateTimeEnd = $dateTimeEnd;

        return $this;
    }

    /**
     * Get dateTimeEnd
     *
     * @return \DateTime
     */
    public function getDateTimeEnd()
    {
        return $this->dateTimeEnd;
    }

    /**
     * Set bookingType
     *
     * @param string $bookingType
     *
     * @return Booking
     */
    public function setBookingType($bookingType)
    {
        $this->bookingType = $bookingType;

        return $this;
    }

    /**
     * Get bookingType
     *
     * @return string
     */
    public function getBookingType()
    {
        return $this->bookingType;
    }

    /**
     * Set bookingStatus
     *
     * @param string $bookingStatus
     *
     * @return Booking
     */
    public function setBookingStatus($bookingStatus)
    {
        $this->bookingStatus = $bookingStatus;

        return $this;
    }

    /**
     * Get bookingStatus
     *
     * @return string
     */
    public function getBookingStatus()
    {
        return $this->bookingStatus;
    }

    /**
     * Set shiftType
     *
     * @param string $shiftType
     *
     * @return Booking
     */
    public function setShiftType($shiftType)
    {
        $this->shiftType = $shiftType;

        return $this;
    }

    /**
     * Get shiftType
     *
     * @return string
     */
    public function getShiftType()
    {
        return $this->shiftType;
    }

    /**
     * Set teachersRequested
     *
     * @param integer $teachersRequested
     *
     * @return Booking
     */
    public function setTeachersRequested($teachersRequested)
    {
        $this->teachersRequested = $teachersRequested;

        return $this;
    }

    /**
     * Get teachersRequested
     *
     * @return integer
     */
    public function getTeachersRequested()
    {
        return $this->teachersRequested;
    }

    /**
     * Set teachersBooked
     *
     * @param integer $teachersBooked
     *
     * @return Booking
     */
    public function setTeachersBooked($teachersBooked)
    {
        $this->teachersBooked = $teachersBooked;

        return $this;
    }

    /**
     * Get teachersBooked
     *
     * @return integer
     */
    public function getTeachersBooked()
    {
        return $this->teachersBooked;
    }

    /**
     * Set smsText
     *
     * @param string $smsText
     *
     * @return Booking
     */
    public function setSmsText($smsText)
    {
        $this->smsText = $smsText;

        return $this;
    }

    /**
     * Get smsText
     *
     * @return string
     */
    public function getSmsText()
    {
        return $this->smsText;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     *
     * @return Booking
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set school
     *
     * @param \ClassCover\SchoolBundle\Entity\School $school
     *
     * @return Booking
     */
    public function setSchool(\ClassCover\SchoolBundle\Entity\School $school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \ClassCover\SchoolBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Add interval
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherBooking $interval
     *
     * @return Booking
     */
    public function addInterval(\ClassCover\BookingBundle\Entity\TeacherBooking $interval)
    {
        $this->intervals[] = $interval;

        return $this;
    }

    /**
     * Remove interval
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherBooking $interval
     */
    public function removeInterval(\ClassCover\BookingBundle\Entity\TeacherBooking $interval)
    {
        $this->intervals->removeElement($interval);
    }

    /**
     * Get intervals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIntervals()
    {
        return $this->intervals;
    }

    /**
     * Set keyword
     *
     * @param \ClassCover\BookingBundle\Entity\KeywordCounter $keyword
     *
     * @return Booking
     */
    public function setKeyword(\ClassCover\BookingBundle\Entity\KeywordCounter $keyword = null)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return \ClassCover\BookingBundle\Entity\KeywordCounter
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Add request
     *
     * @param \ClassCover\BookingBundle\Entity\SmsRequestList $request
     *
     * @return Booking
     */
    public function addRequest(\ClassCover\BookingBundle\Entity\SmsRequestList $request)
    {
        $this->requests[] = $request;

        return $this;
    }

    /**
     * Remove request
     *
     * @param \ClassCover\BookingBundle\Entity\SmsRequestList $request
     */
    public function removeRequest(\ClassCover\BookingBundle\Entity\SmsRequestList $request)
    {
        $this->requests->removeElement($request);
    }

    /**
     * Get requests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRequests()
    {
        return $this->requests;
    }

    /**
     * @return $this
     */
    public function incrementBookedTeacers()
    {
        ++$this->teachersBooked;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedOnValue()
    {
        $this->createdOn = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function setShiftTypeValueIfUnknown()
    {
        if (!in_array($this->shiftType, self::$allowedShiftTypes)) {
            $this->shiftType = 'special';
        }
    }

    /**
     * Set user
     *
     * @param \ClassCover\AppBundle\Entity\User $user
     *
     * @return Booking
     */
    public function setUser(\ClassCover\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ClassCover\AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
