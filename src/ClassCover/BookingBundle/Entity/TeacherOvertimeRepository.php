<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 01.11.2015
 * Time: 16:57
 */

namespace ClassCover\BookingBundle\Entity;


use ClassCover\CyoBundle\Entity\Teacher;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class TeacherOvertimeRepository extends EntityRepository
{
    /**
     * @param Teacher $teacher
     * @param $hours
     * @return bool
     */
    public function addOverTime(Teacher $teacher, $hours)
    {
        $em = $this->getEntityManager();

        $teacherOvertime = $this->findOneBy(['teacher' => $teacher]);

        if ($teacherOvertime == null) {
            $entity = new TeacherOvertime();
            $entity->setTeacher($teacher);
            $entity->setHours($hours);

            $em->persist($entity);
            $em->flush();
        } else {

            $currentHours = $teacherOvertime->getHours();
            $newHours = $currentHours+$hours;
            
            /** @var TeacherOvertime $entity */
            $entity = $teacherOvertime;
            $entity->setHours($newHours);

            $em->persist($entity);
            $em->flush();
        }

        return true;
    }

    /**
     * @param Teacher $teacher
     * @param $hours
     * @return bool
     */
    public function substractOverTime(Teacher $teacher, $hours)
    {

        $em = $this->getEntityManager();

        /** @var TeacherOvertime $teacherOvertime */
        $teacherOvertime = $this->findOneBy(['teacher' => $teacher]);

        if ($teacherOvertime !== null) {
            $currentHours = $teacherOvertime->getHours();

            $newHours = $currentHours-$hours;
            if ($newHours<0 || $newHours == 0) {
                $em->remove($teacherOvertime);
                $em->flush();
            } else {
                $teacherOvertime->setHours($newHours);
                $em->persist($teacherOvertime);
                $em->flush();
            }
        }

        return true;
    }

    /**
     * @param Teacher $teacher
     *
     * @return int
     */
    public function getOvertimeHours(Teacher $teacher)
    {
        $result = 0;

        /** @var Query $query */
        $query = $this->getEntityManager()
            ->createQuery('SELECT o FROM ClassCover\BookingBundle\Entity\TeacherOvertime o WHERE o.teacher = :teacher')
            ->setParameter('teacher', $teacher);
        $result_query = $query->getResult();

        if(count($result_query) > 0) {
            /** @var TeacherOvertime $entity */
            $entity = $result_query[0];
            $result = $entity->getHours();
        }

        return $result;
    }

    public function resetOvertimeAll()
    {
        /** @var Query $query */
        $query = $this->getEntityManager()
            ->createQuery('SELECT o FROM ClassCover\BookingBundle\Entity\TeacherOvertime o');
        $result = $query->getResult();

        /** @var TeacherOvertime $item */
        foreach ($result as $item) {
            $item->setHours(0);
        }
    }

}