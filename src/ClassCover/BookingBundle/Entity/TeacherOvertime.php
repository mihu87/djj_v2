<?php

namespace ClassCover\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TeacherShiftOffer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\BookingBundle\Entity\TeacherOvertimeRepository")
 */
class TeacherOvertime
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\CyoBundle\Entity\Teacher", inversedBy="teacher")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id")
     **/
    protected $teacher;

    /**
     * @ORM\Column(type="float", nullable=false, options={ "default"="0" })
     */
    protected $hours;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teacher
     *
     * @param \ClassCover\CyoBundle\Entity\Teacher $teacher
     *
     * @return TeacherOverTime
     */
    public function setTeacher(\ClassCover\CyoBundle\Entity\Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \ClassCover\CyoBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set teacher
     *
     * @param integer
     *
     * @return TeacherOverTime
     */
    public function setHours($value)
    {
        $this->hours = $value;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return integer
     */
    public function getHours()
    {
        return $this->hours;
    }
}
