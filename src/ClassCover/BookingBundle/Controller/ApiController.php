<?php

namespace ClassCover\BookingBundle\Controller;

use ClassCover\BookingBundle\Entity\Booking;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Count;

use ClassCover\SchoolBundle\Entity\School;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use FOS\UserBundle\Doctrine\UserManager;
use ClassCover\BookingBundle\Entity\TeacherBooking;
use ClassCover\BookingBundle\Entity\TeacherOvertimeRepository;
use ClassCover\BookingBundle\Entity\TeacherOvertime;
use ClassCover\BookingBundle\Services\SmsBookingService;

class ApiController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        return JsonResponse::create([]);
    }

}