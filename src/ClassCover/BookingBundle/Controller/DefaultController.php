<?php

namespace ClassCover\BookingBundle\Controller;

use ClassCover\BookingBundle\Entity\Booking;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Count;

use ClassCover\SchoolBundle\Entity\School;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use FOS\UserBundle\Doctrine\UserManager;
use ClassCover\BookingBundle\Entity\TeacherBooking;
use ClassCover\BookingBundle\Entity\TeacherOvertimeRepository;
use ClassCover\BookingBundle\Entity\TeacherOvertime;
use ClassCover\BookingBundle\Services\SmsBookingService;

class DefaultController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction()
    {
            return $this->render('ClassCoverBookingBundle:Default:index.html.twig');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getTeachersAvailableForSmsBookingAjaxAction(Request $request)
    {

        $response = [
            'status' => 'success',
            'errors' => [],
            'list'   => []
        ];

        $constraints = new Collection([
            'schoolId' => [
                new NotBlank(),
                new GreaterThan(0)
            ],
            'isMultiple' => [
                new NotBlank()
            ],
            'shiftTypeName' => [
                new NotBlank()
            ],
            'shiftTypeId' => [
                new NotBlank(),
            ],
            'teachersToBook' => [
                new NotBlank(),
                new GreaterThan(['value' => 0])
            ],
            'timeFrom' => [
                new NotBlank()
            ],
            'timeTo' => [
                new NotBlank()
            ],
            'dateFrom' => [
                new NotBlank()
            ],
            'dateTo' => [
                new NotBlank()
            ],
            'format' => [
                new NotBlank()
            ]

        ]);

        $data = $request->request->all();

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
            $response['success'] = 'error';

            return new JsonResponse($response);
        }

        if (!isset($data['format'])) {
            $data['format'] = 'api/json';
        }

        /** @var SmsBookingService $smsBookingService */
        $smsBookingService = $this->get('sms_booking_service');
        $teachers = $smsBookingService->getTeachersAvailabilityForSmsBooking($data['schoolId'], $data['dateFrom'], $data['dateTo'], $data['timeFrom'], $data['timeTo'], $data['teachersToBook']);

        if ($data['format'] == 'display/html') {
            $response['html'] = $this->renderView('ClassCoverBookingBundle:Ajax:sms-availability.html.twig', array('teachers' => $teachers));
        }

        $response['list'] = $teachers;

        return new JsonResponse($response);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getTeacherAvailabilityForManualBookingAjaxAction(Request $request)
    {

        $response = [
            'status' => 'success',
            'errors' => []
        ];

        $constraints = new Collection([
            'schoolId' => [
                new NotBlank(),
                new GreaterThan(0)
            ],
            'teacherId' => [
                new NotBlank(),
                new GreaterThan(0)
            ],
            'shiftTypeName' => [
                new NotBlank()
            ],
            'shiftTypeId' => [
                new NotBlank(),
            ],
            'timeFrom' => [
                new NotBlank()
            ],
            'timeTo' => [
                new NotBlank()
            ],
            'dateFrom' => [
                new NotBlank()
            ],
            'dateTo' => [
                new NotBlank()
            ]
        ]);

        $data = $request->request->all();

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
            $response['success'] = 'error';

            return new JsonResponse($response, 200, ['Content-type', 'application/json']);
        }

        if (!isset($data['format'])) {
            $data['format'] = 'api/json';
        }

        /** @var \ClassCover\BookingBundle\Services\SmsBookingService $smsBookingService */
        $smsBookingService = $this->get('sms_booking_service');
        $result = $smsBookingService->getTeacherAvailabilityForManualBooking(
            $data['teacherId'],
            $data['dateFrom'],
            $data['dateTo'],
            $data['timeFrom'],
            $data['timeTo']
        );

        /** @var School $school */
        $school = $this->getDoctrine()->getRepository('ClassCoverSchoolBundle:School')->find($data['schoolId']);
        /** @var Teacher $teacher */
        $teacher = $this->getDoctrine()->getRepository('ClassCoverCyoBundle:Teacher')->find($data['teacherId']);
        /** @var ArrayCollection $teachers */
        $teachers = new ArrayCollection();
        $teachers->add($teacher);

        if (is_int($result) && $result == -1) {
            $response['errors'][] = 'Request processing error.';
            $response['status'] = 'error';

            return new JsonResponse($response);
        }

        if ($result == false) {
            $teacherFullName = $teacher->getFirstName() . ' ' . $teacher->getSurname();
            $response['errors'][] = 'This shift cannot be booked as ' . $teacherFullName . ' is already booked for another shift within 8 hours. Please review and adjust times to continue.';
            $response['status'] = 'error';

            return new JsonResponse($response);
        }

        $timeFrom   = explode(":", $data['timeFrom']);
        $timeTo     = explode(":", $data['timeTo']);

        $dateTimeFrom = (new \DateTime($data['dateFrom']))->setTime($timeFrom[0], $timeFrom[1], 0);
        $dateTimeTo = (new \DateTime($data['dateTo']))->setTime($timeTo[0], $timeTo[1], 0);

        $createResult = $smsBookingService->createBookingRequest(
            $school,
            $teachers,
            $dateTimeFrom,
            $dateTimeTo,
            1,
            [],
            strtolower($data['shiftTypeName']),
            Booking::BOOKING_TYPE_MANUAL,
            Booking::BOOKING_STATUS_COMPLETED
        );

        if (count($createResult->errors)) {
            $response['errors'][] = $createResult->errors;
            $response['status'] = 'error';
            return new JsonResponse($response);
        }

        $response['booking_type'] = Booking::BOOKING_TYPE_MANUAL;

        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createBookingAjaxAction(Request $request)
    {
        $doctrine = $this->container->get('doctrine');

        /** @var SmsBookingService $smsBookingService */
        $smsBookingService = $this->get('sms_booking_service');

        $response = [
            'status' => 'success',
            'errors' => []
        ];

        $constraints = new Collection([
            'schoolId' => [
                new NotBlank(),
                new GreaterThan(0)
            ],
            'isMultiple' => [
                new NotBlank()
            ],
            'shiftTypeName' => [
                new NotBlank()
            ],
            'teachersToBook' => [
                new NotBlank(),
                new GreaterThan(['value' => 0])
            ],
            'timeFrom' => [
                new NotBlank()
            ],
            'timeTo' => [
                new NotBlank()
            ],
            'dateFrom' => [
                new NotBlank()
            ],
            'dateTo' => [
                new NotBlank()
            ],
            'teacherList' => [
                new Count(['min' => 1])
            ]
        ]);

        $data = $request->request->all();

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
        }

        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find($data['schoolId']);

        /** @var TeacherRepository $teacherRepository */
        $teacherRepository = $doctrine->getRepository('ClassCoverCyoBundle:Teacher');

        if (empty($school)) {
            $response['errors'][] = 'Cannot find school with id ' . $data['userId'];
        }

        if (count($data['teacherList']) == 0) {
            $response['errors'][] = 'There are no teachers present in your list';
        }

        $teachersCollection = new ArrayCollection();

        $lastDelay = 0;
        $exclusionPosition = 0;

        foreach ($data['teacherList'] as $teacher) {
            $teacher = $teacherRepository->find((int) $teacher['teacherId']);

            if (empty($teacher)) {
                $response['errors'][] = 'Cannot find entity for teacher with id ' . $teacher['teacherId'];
                continue;
            }
            $teachersCollection->add($teacher);
        }

        if (count($response['errors'])) {
            $response['status'] = 'error';
            return new JsonResponse($response);
        }

        $timeFrom   = explode(":", $data['timeFrom']);
        $timeTo     = explode(":", $data['timeTo']);

        $dateTimeFrom = (new \DateTime($data['dateFrom']))->setTime($timeFrom[0], $timeFrom[1], 0);
        $dateTimeTo = (new \DateTime($data['dateTo']))->setTime($timeTo[0], $timeTo[1], 0);


        $numTeachers = (int) $data['teachersToBook'];
        $shiftType = strtolower($data['shiftTypeName']);
        
        $createResult = $smsBookingService->createBookingRequest($school, $teachersCollection, $dateTimeFrom, $dateTimeTo, $numTeachers, $data, $shiftType);

        $response['result'] = $createResult;

        return new JsonResponse($response);
    }

    public function updateBookingAjaxAction(Request $request)
    {
        $doctrine = $this->container->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        /** @var \ClassCover\BookingBundle\Services\SmsBookingService $smsBookingService */
        $smsBookingService = $this->get('sms_booking_service');

        $response = [
            'status' => 'success',
            'errors' => []
        ];

        $constraints = new Collection([
            'schoolId' => [
                new NotBlank(),
                new GreaterThan(0)
            ],
            'teacherId' => [
                new NotBlank(),
                new GreaterThan(0)
            ],
            'bookingId' => [
                new NotBlank(),
                new GreaterThan(0)
            ],
            'shiftTypeName' => [
                new NotBlank()
            ],
            'timeFrom' => [
                new NotBlank()
            ],
            'timeTo' => [
                new NotBlank()
            ],
            'dateFrom' => [
                new NotBlank()
            ],
            'dateTo' => [
                new NotBlank()
            ]
        ]);

        $data = $request->request->all();

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
        }

        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find($data['schoolId']);
        /** @var Teacher $teacher */
        $teacher = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->find($data['teacherId']);
        /** @var Booking $booking */
        $booking = $doctrine->getRepository('ClassCoverBookingBundle:Booking')->find($data['bookingId']);

        if ($school == null) {
            $response['errors'][] = 'Cannot find school entity with id ' . $data['schoolId'];
        }
        if ($teacher == null) {
            $response['errors'][] = 'Cannot find teacher entity with id ' . $data['teacherId'];
        }
        if ($booking == null) {
            $response['errors'][] = 'Cannot find booking entity with id ' . $data['bookingId'];
        }

        if (count($response['errors'])) {
            $response['status'] = 'error';
            return JsonResponse::create($response);
        }

        $smsBookingService->updateTeacherBooking($booking, $school, $teacher, $data);

        return JsonResponse::create($response);
    }
    /**
     * @param $network
     * @param Request $request
     * @return Response
     */
    public function teacherBookingResponseCallbackAction($network, Request $request)
    {
        /** @var \ClassCover\BookingBundle\Services\InterceptorService $interceptorService */
        $interceptorService = $this->get('requests_interceptor_service');
        $interceptorService->recordIncomingRequest($request->request->all(), $request->query->all(), $request->attributes->all(), $network);

        /** @var array $data */
        $data = $request->query->all();

        /** @var \ClassCover\BookingBundle\Services\SmsBookingService $smsBookingService */
        $smsBookingService = $this->get('sms_booking_service');

        if (strpos($_SERVER['SERVER_NAME'], 'medley') !== false) {
            // Send fallback notification
            if (isset($data['Body']) && isset($data['From'])) {
                $smsContent = "{$network} callback routed through fallback.[{$data['Body']}/{$data['From']}]";
                $smsBookingService->sendMessage('+40748638647', $smsContent);
                $smsBookingService->sendMessage('+61410484446', $smsContent);
            }
        }

        if (empty($data)) {
            return new Response('NO DATA SENT', 200, ['Content-type', 'text/plain']);
        }

        $data['From'] = strtoupper(trim(str_replace("+", "", $data['From'])));

        $smsBookingService->handleSmsBookingResponseRequest($data);
        
        return new Response('OK', 200, ['Content-type', 'text/plain']);
    }

}
