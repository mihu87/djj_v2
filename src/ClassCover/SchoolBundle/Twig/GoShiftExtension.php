<?php

namespace ClassCover\SchoolBundle\Twig;

use ClassCover\BookingBundle\Mappers\TeacherAvailabilityRepresentation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;
/**
 * Class GoShiftExtension
 * @package ClassCover\SchoolBundle\Twig
 */
class GoShiftExtension extends \Twig_Extension
{

    /** @var ContainerInterface  */
    protected $container;

    /**
     * GoShiftExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @var array
     */
    protected $stringRepresentationMap = [
        TeacherAvailabilityRepresentation::STATUS_AVAILABLE     => 'A',
        TeacherAvailabilityRepresentation::STATUS_UNAVAILABLE   => 'U',
        TeacherAvailabilityRepresentation::STATUS_BOOKED        => 'B',
        TeacherAvailabilityRepresentation::STATUS_EXCLUDED      => 'X',
        TeacherAvailabilityRepresentation::STATUS_OFFERED       => 'P',

    ];

    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('availabilityFrontValue', array($this, 'availabilityValueFrontGenerator'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('observationContent', array($this, 'observationContent'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('observationTitle', array($this, 'observationTitle'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('toNational', array($this, 'toNationalFormat')),
            new \Twig_SimpleFilter('toInternational', array($this, 'toInternationalFormat')),
            new \Twig_SimpleFilter('displayFormat', array($this, 'toInternationalPrettyFormat')),
            new \Twig_SimpleFilter('phoneNumberToNational', array($this, 'phoneNumberToNational')),
        );
    }

    /**
     * @param array $statuses
     * @return array
     */
    public function availabilityValueFrontGenerator(array $statuses)
    {
        $statusOutput = $this->getAvailabilityStatusStringRepresentation($statuses[0]);
        $statusCount  = count($statuses);

        if ($statusCount > 1) {
            $statusOutput .= '<sub class="second-status">';
            for($i=1; $i<$statusCount; $i++) {
                $statusOutput.=  $this->getAvailabilityStatusStringRepresentation($statuses[$i]) . ' ';
            }
            $statusOutput .= '</sub>';
        }

        return $statusOutput;
    }

    /**
     * @param $status
     * @return mixed
     */
    protected function getAvailabilityStatusStringRepresentation($status) {

        if (array_key_exists($status, $this->stringRepresentationMap)) {
            return $this->stringRepresentationMap[$status];
        }

        return $status;
    }

    /**
    * @param $phoneNumber
    * @return mixed
    */
    public function toNationalFormat($phoneNumber)
    {

        $national = "";

        if (substr($phoneNumber, 0, 2) == "00")
        {
            $national =  "0".substr($phoneNumber, 4);

        } elseif (substr($phoneNumber, 0, 1) == "+") {

            $national =  "0".substr($phoneNumber, 3);

        } elseif (strlen($phoneNumber) == "11" || strlen($phoneNumber) == "10") {

            $national =  "0".substr($phoneNumber, 2);

        }

        return $national;
    }

    /**
     * @param $phoneNumber
     * @return mixed|string
     */
    public function toInternationalPrettyFormat($phoneNumber)
    {
        $num = $this->toNationalFormat($phoneNumber);
        $formatted = '';

        $prefix = substr($num,0,2);
        if($prefix == '04') {
            $formatted = $prefix . " ".substr($num,2,4)." ".substr($num,-6);
        } else {
            $formatted = substr($num,0,4)." ".substr($num,2,3)." ".substr($num,-3);
        }

        //temp
        return str_replace("+", '', $phoneNumber);
    }

    /**
     * @param $phoneNumber
     * @return mixed
     */
    public function toInternationalFormat($phoneNumber)
    {
        return $phoneNumber;
    }

    /**
     * @param $reason
     * @return string
     */
    public function observationContent($reason)
    {
        $str = '';
        $reason = (array) json_decode($reason, true);
        $oReason = end($reason);
        
        if (isset($oReason['text'])) {
            $str = $oReason['text'];
        }

        return $str;
    }

    /**
     * @param $reason
     * @return string
     */
    public function observationTitle($reason)
    {
        $str = '';
        $reason = (array) json_decode($reason, true);
        $oReason = end($reason);

        if (isset($oReason['label'])) {
            $str = $oReason['label'];
        }

        return $str;
    }

    public function phoneNumberToNational($string)
    {

        $phoneUtil = $this->container->get('libphonenumber.phone_number_util');

        try {
            $pN = $phoneUtil->parse($string, "AU");
        } catch (\libphonenumber\NumberParseException $e) {
            var_dump($e);
        }

        return $phoneUtil->format($pN, \libphonenumber\PhoneNumberFormat::NATIONAL);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'goshift_extension';
    }
}