<?php
namespace ClassCover\SchoolBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\ORM\EntityManager;
use ClassCover\SchoolBundle\Entity\School;
use FOS\UserBundle\Doctrine\UserManager;
use ClassCover\AppBundle\Entity\User;

/**
 * Class SchoolService
 * @package ClassCover\ScoolBundle\Services
 */
class SchoolService
{

    const DEFAULT_PASSWORD = '123456';
    const DEFAULT_ROLE_SCHOOL = 'ROLE_SCHOOL';
    const ROLE_SCHOOL_ADMIN = 'ROLE_SCHOOL_ADMIN';
    const ROLE_ADDITIONAL_USER = 'ROLE_SCHOOL_ADDITIONAL_USER';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $name
     * @param $shortName
     * @param $email
     * @param $mobile
     * @return bool
     */
    public function createSchool($name, $shortName, $email, $mobile)
    {
        $doctrine = $this->container->get('doctrine');

        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->createUser();

        $user->setUsername(strtolower($shortName));
        $user->setEmail($email);
        $user->setPlainPassword(self::DEFAULT_PASSWORD);
        $user->addRole(self::DEFAULT_ROLE_SCHOOL);
        $user->setEnabled(true);

        $userManager->updateUser($user);

        $school = new School();
        $school->setName($name);
        $school->setShortName($shortName);
        $school->setEmail($email);
        $school->setPhone($mobile);
        $school->setUser($user);

        $em->persist($school);
        $em->flush();

        return true;
    }
}
