ClassCover.module('SchoolTeachers', (function($){

    var CONFIG = {

        TEACHER_SET_PROPERTY_REQUEST : {
            success     : teacherPropertySetSuccessHandler,
            dataType    : 'JSON',
            type        : 'POST'
        },
        TEACHER_SET_ORDER_REQUEST : {
            success     : teacherOrderSetSuccessHandler,
            dataType    : 'JSON',
            type        : 'POST'
        },
        PROPERTY_BUTTON_TEXT_MAPPINGS : {
          suspended : { OFF : 'Suspend' , ON : 'Reactivate' },
          removed : { OFF : 'Remove' , ON : 'Reinstate' }
        },
        PROPERTY_BUTTON_CLASS : {
            suspended : { ON : 'warning', disable : false },
            removed : { ON : 'danger', disable : true }
        }
    };

    function initPropertyHandlersButtons() {
        $('.property-handler').on('click', propertyClickHandler);
    }

    function propertyClickHandler() {

        var params = {};

        params.property = $(this).attr('property');
        params.value = $(this).attr('data-property-value');
        params.teacherId = $(this).attr('data-teacher-id');
        params.schoolId = window['schoolId'];

        CONFIG.TEACHER_SET_PROPERTY_REQUEST.url = Routing.generate('class_cover_school_teacher_set_property');
        CONFIG.TEACHER_SET_PROPERTY_REQUEST.data = params;

        $.ajax(CONFIG.TEACHER_SET_PROPERTY_REQUEST);
    }

    function teacherPropertySetSuccessHandler(response) {

        if ('success' == response.status) {
            var propertyName = response.data.property.name;
            var $btnElement = $('.property-handler[data-teacher-id="' + response.data.teacherId + '"][property="' + propertyName + '"]');
                $btnElement.attr('property', response.data.property.name);

            if (parseInt(response.data.property.value) == 1) {
                $('tr[data-teacher-id="' + response.data.teacherId + '"]').addClass(CONFIG.PROPERTY_BUTTON_CLASS[propertyName].ON);
                $btnElement.attr('data-property-value', 0).text(CONFIG.PROPERTY_BUTTON_TEXT_MAPPINGS[propertyName].ON);
                $btnElement.removeClass('btn-danger');

                if (CONFIG.PROPERTY_BUTTON_CLASS[propertyName].disable == true) {
                    $('.property-handler[data-teacher-id="' + response.data.teacherId + '"][property!="' + propertyName + '"]')
                        .attr('disabled', 'disabled');
                    $('input[property="displayOrder"][data-teacher-id="' + response.data.teacherId + '"]')
                        .attr('disabled', 'disabled');
                }
            } else {
                $('tr[data-teacher-id="' + response.data.teacherId + '"]').removeClass('danger').removeClass('warning');
                $btnElement.attr('data-property-value', 1).text(CONFIG.PROPERTY_BUTTON_TEXT_MAPPINGS[propertyName].OFF);
                $btnElement.addClass('btn-danger');

                $('.property-handler[data-teacher-id="' + response.data.teacherId + '"][property!="' + propertyName + '"]')
                    .removeAttr('disabled', 'disabled');
                $('input[property="displayOrder"][data-teacher-id="' + response.data.teacherId + '"]')
                    .removeAttr('disabled', 'disabled');
            }
        }
    }

    function teacherOrderSetSuccessHandler(response) {

        console.log(response);
    }

    function initOrderingFunction() {

        $('.registered-teachers-table').sortable({
            placeholder: "ui-state-highlight",
            items: "tr",
            handle: '.ordering-arrow',
            start: function (event, ui) {
                window.casualOrderCache = {};
                $.each($(this).find('input'), function(i,v) {
                    window.casualOrderCache[i] = $(v).val();
                });

            },
            stop: function(event, ui) {

                $.each($(this).find('input'), function(i,v) {
                    $(v).val(window.casualOrderCache[i]);
                });

                delete window.casualOrderCache;

                var pairs = [];

                $.each($(this).find('input'), function(i,v) {
                    pairs.push({
                        teacherId : $(v).attr('data-teacher-id'),
                        order : $(v).val()
                    })
                });

                CONFIG.TEACHER_SET_ORDER_REQUEST.url = Routing.generate('class_cover_school_teachers_set_ordering');
                CONFIG.TEACHER_SET_ORDER_REQUEST.data = { pairs : pairs, schoolId : window['schoolId'] };
                $.ajax(CONFIG.TEACHER_SET_ORDER_REQUEST);
            }
        });
        
    }

    /**
     * GoShift App School SMS Book Module
     * @returns {boolean}
     */
    function initModule() {

        initPropertyHandlersButtons();
        initOrderingFunction();

        return true;
    }

    /**
     * return facade
     */
    return {
        init : initModule
    }

}(jQuery)));