ClassCover.module('SchoolSmsBook', (function($){

    var CONFIG = {
        searchDateFieldConfig : {
            dateFormat: "dd M, yy",
            defaultDate : 1,
            onSelect : formDateTimeSelectorsHandler
        },
        shiftTypeMap : {
            '1' : 'Morning',
            '2' : 'Evening',
            '3' : 'Night',
            '0' : 'Special'
        },
        defaultShiftType : {
            interval : ['06:00', '14:00']
        },
        TEACHER_AVAILABILITY_REQUEST_CONFIG : {
            success : searchSmsAvailableTeachersSuccessHandler,
            beforeSend : searchSmsAvailableTeachersBeforeHandler,
            error : searchSmsAvailableTeachersErrorHandler,
            complete : searchSmsAvailableTeachersCompleteHandler,
            dataType : 'JSON',
            type : 'POST'
        },
        CREATE_BOOKING_REQUEST_CONFIG : {
            success     : createBookingSuccessHandler,
            beforeSend  : createBookingBeforeHandler,
            error       : createBookingErrorHandler,
            complete    : createBookingCompleteHandler,
            dataType    : 'JSON',
            type        : 'POST'
        },
        SECTION_TITLE : 'Create SMS Booking',
        DEFAULT_DATE : new Date($('#default-date-for-search').val())
    };

    var initialTeacherOrder = [];

    function initBookingPageDatePickers() {

        var $dateFrom = $(".book-via-sms-shift-list-pickers.datepicker.date-from");
        var $dateTo = $(".book-via-sms-shift-list-pickers.datepicker.date-to");

        $dateFrom.datepicker(CONFIG.searchDateFieldConfig)
            .on('keyUp', datePickerKeyHandler)
            .on('keyPress', datePickerKeyHandler)
            .datepicker('setDate', CONFIG.DEFAULT_DATE);

        $dateTo.datepicker(CONFIG.searchDateFieldConfig)
            .on('keyUp', datePickerKeyHandler)
            .on('keyPress', datePickerKeyHandler)
            .datepicker('setDate', CONFIG.DEFAULT_DATE);

    }

    function formDateTimeSelectorsHandler(e)
    {
        if ($(this).hasClass('date-from')) {
            var fromPickerSelectedDate = $(this).datepicker('getDate');
            $(".book-via-sms-shift-list-pickers.datepicker.date-to").datepicker('setDate', fromPickerSelectedDate);
        }
    }

    /**
     * Prevent user key input
     * @param event
     * @returns {boolean}
     */
    function datePickerKeyHandler(event)
    {
        event.preventDefault();
        return false;
    }

    function initSearchButton()
    {
        $('.book-via-sms.search').on('click', searchButtonHandler);
    }

    function searchButtonHandler()
    {
        searchSmsAvailableTeachers();
    }

    function hideBat()
    {
        $('.sub-page-ta-main').hide();
    }

    function showBat()
    {
        $('.sub-page-ta-main').show();
    }

    function initShiftTypeChoiceInput() {
        $('.book-via-sms.shift-type-choice > li > a').off().on('click', shiftTypeChoiceHandler);
    }

    function initTeachersToBookSelect() {
        var $teachersToBook = $('.book-via-sms.teachers-to-book');
            $teachersToBook.on('change', teachersToBookChangeHandler);
    }

    function initManualOverrideActions() {
        $('.book-via-sms.manual-override').on('change', manualOverrideClickHandler);
        $('.book-via-sms.override-confirm').on('click', overrideConfirmClickHandler);
        $('.book-via-sms.override-abort').on('click', overrideAbortClickHandler);
    }

    function teachersToBookChangeHandler() {

    }

    function initTimeIntervalChoiceInputs() {

        var $timeFrom = $('.book-via-sms-shift-list-pickers.timepicker.time-from');
        var $timeTo = $('.book-via-sms-shift-list-pickers.timepicker.time-to');

        setTimeChoiceIntervals(CONFIG.defaultShiftType.interval, false);

        $timeFrom.on('change', timeChoiceChangeHandler);
        $timeTo.on('change', timeChoiceChangeHandler);
    }

    function initBackToSchoolPageButton() {
        $('.book-via-sms.back-to-book-page').on('click', backToSchoolPageClickHandler);
    }

    function initBackToHistoryPageButton() {
        $('.book-via-sms.go-to-sms-history-page').on('click', backToHistoryPageClickHandler);
    }


    function initResetButton() {
        $('.book-via-sms.reset').on('click', resetClickHandler);
    }

    function timeChoiceChangeHandler() {

    }

    function initCreateSmsBookingRequestButton() {
        $('.book-via-sms.submit').on('click', createSmsBookingRequestClickHandler);
    }

    function shiftTypeChoiceHandler() {

        var shiftTypeId = parseInt($(this).attr('id'));
        var typeName = CONFIG.shiftTypeMap[shiftTypeId];
        setChosenShiftValue(shiftTypeId, typeName);

        if (0 != shiftTypeId) {
            var interval = getIntervalByShiftTypeId(shiftTypeId);
            if (false != interval) {
                if (shiftTypeId == 3) {
                    setTimeChoiceIntervals(interval, true);
                } else {
                    setTimeChoiceIntervals(interval, false);
                }

            }
        }
    }

    function getShiftTypeIdByInterval(from, to) {

        if (from == '06:00' && to == '14:00') {
            return 1;
        } else if (from == '14:00' && to == '22:00') {
            return 2;
        } else if (from == '22:00' && to == '06:00') {
            return 3;
        } else {
            return 0;
        }
    }

    function getIntervalByShiftTypeId(shiftTypeId) {

        if (shiftTypeId == 1) {
            return ['06:00', '14:00'];
        } else if (shiftTypeId == 2) {
            return ['14:00', '22:00'];
        } else if (shiftTypeId == 3) {
            return ['22:00', '06:00'];
        }

        return false;
    }

    function setChosenShiftValue(id, name) {
        var $shiftTypeDisplay = $('#book-via-sms-shift-list-value');
        $shiftTypeDisplay.attr('data-selected-shift-type-id', id);
        $shiftTypeDisplay.val(name);
    }

    function setTimeChoiceIntervals(interval, overNight) {

        $('.book-via-sms-shift-list-pickers.timepicker.time-from').val(interval[0]);
        $('.book-via-sms-shift-list-pickers.timepicker.time-to').val(interval[1]);

        if (overNight == true) {
            var $el = $('.book-via-sms.datepicker.date-to');
            var dateNext = $el.datepicker('getDate');

            dateNext.setDate(dateNext.getDate()+1);
            $el.datepicker('setDate', dateNext);
        } else {
            var oPDate = $('.book-via-sms-shift-list-pickers.datepicker.date-from').datepicker('getDate');
            $('.book-via-sms-shift-list-pickers.datepicker.date-to').datepicker('setDate', oPDate);
        }
    }

    function initDynamicSearch() {

        /*
        $('#book-via-sms-shift-list > li').on('click', dynamicSearchHandler);
        $('.book-via-sms-shift-list-pickers.timepicker.time-from').on('change', dynamicSearchHandler);
        $('.book-via-sms-shift-list-pickers.timepicker.time-to').on('change', dynamicSearchHandler);
        $('.book-via-sms.teachers-to-book').on('change', dynamicSearchHandler);
        */
    }

    function dynamicSearchHandler() {

        var dateFrom = $('.book-via-sms-shift-list-pickers.datepicker.date-from').datepicker('getDate');
        var dateTo = $('.book-via-sms-shift-list-pickers.datepicker.date-to').datepicker('getDate');

        var timeFrom =  $('.book-via-sms-shift-list-pickers.timepicker.time-from').val().split(":");
        var timeTo =  $('.book-via-sms-shift-list-pickers.timepicker.time-to').val().split(":");


        dateFrom.setHours(parseInt(timeFrom[0]))
        dateFrom.setMinutes(parseInt(timeFrom[1]));

        dateTo.setHours(parseInt(timeTo[0]))
        dateTo.setMinutes(parseInt(timeTo[1]));


        var tDiff = dateTo.getTime()-dateFrom.getTime();

        if (tDiff < 3600000 || tDiff > 43200000) {
            return 'MIN_MAX_INTERVAL_ERROR';
        }
    }

    function searchSmsAvailableTeachers() {

        var actValidator = dynamicSearchHandler();

        if (actValidator == 'MIN_MAX_INTERVAL_ERROR') {

            $('#confirm-large-booking-modal').modal({
                backdrop : 'static',
                keyboard : false,
                show : true
            });

            $('#confirm-large-booking').off().on('click', function() {
               $('#confirm-large-booking-modal').modal('hide');
               executeSearchAvailableTeachers();
            });

            return false;
        }

        executeSearchAvailableTeachers();
    }

    function executeSearchAvailableTeachers()
    {
        disableOverrideElements();
        var shiftType = $('.book-via-sms.shift-type-value').attr('data-selected-shift-type-id');
        var shiftTypeName = CONFIG.shiftTypeMap[shiftType];

        var DateFrom = moment(
            new Date($('.book-via-sms-shift-list-pickers.datepicker.date-from').val())
        ).format();

        var DateTo = moment(
            new Date($('.book-via-sms-shift-list-pickers.datepicker.date-to').val())
        ).format();

        var params = {
            dateFrom        : DateFrom,
            dateTo          : DateTo,
            timeFrom        : $('.book-via-sms-shift-list-pickers.timepicker.time-from').val(),
            timeTo          : $('.book-via-sms-shift-list-pickers.timepicker.time-to').val(),
            teachersToBook  : $('.book-via-sms.teachers-to-book').val(),
            shiftTypeId     : shiftType,
            shiftTypeName   : shiftTypeName,
            isMultiple      : 0,
            schoolId        : window['schoolId'],
            format          : 'display/html'
        };

        if (params.dateTo > params.dateFrom) {
            params.isMultiple = 1;
        }

        CONFIG.TEACHER_AVAILABILITY_REQUEST_CONFIG.url = Routing.generate('class_cover_booking_search_sms_available_teachers_ajax');
        CONFIG.TEACHER_AVAILABILITY_REQUEST_CONFIG.data = params;

        $.ajax(CONFIG.TEACHER_AVAILABILITY_REQUEST_CONFIG);
    }

    function searchSmsAvailableTeachersBeforeHandler(xhr) {
        $('.book-via-sms.submit').hide();
        $('#manual-override-function').hide();
        hideBat();
    }

    function searchSmsAvailableTeachersErrorHandler(xhr, status, error) {

    }

    function searchSmsAvailableTeachersSuccessHandler(result, status, xhr) {

        if (result.status && 'success' == result.status) {

            $('#sms-available-teachers-container').html(result.html);

            $.each($('.sortable.listing'), function(i,v){
                initialTeacherOrder.push({
                    priority : $(v).attr('data-priority'),
                    teacher : $(v).attr('data-teacher-id'),
                    delay : $(v).attr('data-delay')
                });
            });
            $('.book-via-sms.submit').show();
            $('#manual-override-function').show();
        }
    }

    function searchSmsAvailableTeachersCompleteHandler(jqXHR, textStatus) {

    }

    function backToSchoolPageClickHandler() {
        $('.sub-page-ta-main').show();
        $('.sub-page-book-via-sms').hide();
    }

    function backToHistoryPageClickHandler() {

        $('.sub-page-book-via-sms').hide();
        $('.sub-page-ta-main').hide();
        $('.sub-page-school-booking-history').show();

        ClassCover.modules()['SchoolBook'].fireSchoolBookingHistoryRequest();
    }

    function resetClickHandler() {
        $('.book-via-sms-shift-list-pickers.datepicker.date-from').datepicker('setDate', CONFIG.DEFAULT_DATE);
        $('.book-via-sms-shift-list-pickers.datepicker.date-to').datepicker('setDate', CONFIG.DEFAULT_DATE);
        setTimeChoiceIntervals(CONFIG.defaultShiftType.interval, false);
        setChosenShiftValue(1, CONFIG.shiftTypeMap[1]);
        $('.book-via-sms.teachers-to-book').val(1);

        $('#sms-available-teachers-container').html("");
        $('.book-via-sms.submit').hide();
        showBat();
    }

    function manualOverrideClickHandler() {
        if ($(this).is(':checked')) {
            showOverridePrompt();
        } else {
            disableOverrideMode();
        }
    }

    function showOverridePrompt() {
        $('#confirm-override').modal({
            backdrop : 'static',
            keyboard : false,
            show : true
        });
    }

    function overrideConfirmClickHandler() {
        if($('#manual-override-by').val() == '' || $('#manual-override-reason').val() == '') {
            alert('Please enter a Reason and who this was Authorised by!');
            return false;
        }
        enableOverrideMode();
    }

    function overrideAbortClickHandler() {
        disableOverrideMode();
    }

    function enableOverrideMode() {

        $('#confirm-override').modal('hide');
        $('.sorting-arrow').show();
        $('.remove-sl-item').on('click', function() {
            var tid = $(this).attr('data-teacher-id');

            $(this).parents().find('tr[data-teacher-id="' + tid + '"]').remove();

        }).show();
        $('.request-delay').removeAttr('readonly');
        $('.global-delay').show();
        $('#default-global-delay').on('keyup', function() {
            if ('' != $(this).val()) {
                $('.request-delay').val($(this).val());
            }

        });

        $('.sms-availability-table tbody').sortable({
            placeholder: "ui-state-highlight",
            items: "tr.sortable",
            handle: '.sorting-arrow',
            start: function (event, ui) {
                window.delayOrderCache = {};
                window.contactOrderCache = {};

                $.each($(this).find('input'), function(i,v) {
                    window.delayOrderCache[i] = $(v).val();
                });

                $.each($(this).find('span.contact-order'), function(i,v) {
                    window.contactOrderCache[i] = parseInt($(v).html());
                });
            },
            update: function (event, ui) {

                $.each($(this).find('input'), function(i,v) {
                    $(v).val(window.delayOrderCache[i]);
                });

                $.each($(this).find('span.contact-order'), function(i,v) {
                    $(v).html(window.contactOrderCache[i]);
                });

                delete window.delayOrderCache;
                delete window.contactOrderCache;
            }

        });

        $('.add-sl-item').on('click', function() {
            var tid = $(this).attr('data-teacher-id');
            var $row = $(this).parent().parent();
            $(".sortable.listing.available-teachers-to-book:last").after($row);
            $(".hide-in-sl", $row).hide();
            $(".unhide-in-sl", $row).show();
            $row.removeClass("overtime-teachers").addClass("available-teachers-to-book");
        }).show();
    }

    function disableOverrideElements()
    {
        $('.book-via-sms.manual-override').removeAttr('checked');
        $('.sorting-arrow').hide();
        $('.remove-sl-item').hide().off('click');
        $('.global-delay').hide();
        $('#default-global-delay').off('keyup');
        $('.request-delay').attr('readonly', 'readonly');
        $('.add-sl-item').hide().off('click');
    }

    function disableOverrideMode() {
        disableOverrideElements();
        executeSearchAvailableTeachers();
    }

    function incrementToDate() {
        var pickerShiftedDate = new Date($('.book-via-sms.datepicker.date-from').val());
            pickerShiftedDate.setDate(pickerShiftedDate.getDate() + 1);
        $('.book-via-sms.datepicker.date-to').datepicker('setDate', pickerShiftedDate);
    }

    function createAlert(caption, text, type) {

        var alertHtml = '<div class="alert alert-dismissible ' + type + ' book-via-sms form-alert" role="alert">';
                alertHtml+= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                alertHtml+= '<span class="alert-text"><strong>'+caption+'</strong> ' + text + '</span>';
            alertHtml+= '</div>';
        $('#sms-alert-container').html(alertHtml);

    }

    function createSmsBookingRequestClickHandler() {

        var DateFrom = moment(
            new Date(
                $('.book-via-sms-shift-list-pickers.datepicker.date-from').val()
            )
        ).format("dddd, Do MMMM YYYY");

        var DateTo = moment(
            new Date(
                $('.book-via-sms-shift-list-pickers.datepicker.date-to').val()
            )
        ).format("dddd, Do MMMM YYYY");


        if (DateFrom == DateTo) {
            $('.shift-date-prompt').html(DateFrom);
        } else {
            $('.shift-date-prompt').html(DateFrom + ' to ' + DateTo);
        }

        $('.shift-from-prompt').html(
            $('.book-via-sms-shift-list-pickers.timepicker.time-from').val()
        );

        $('.shift-to-prompt').html(
            $('.book-via-sms-shift-list-pickers.timepicker.time-to').val()
        );

        $('.shift-casuals-prompt').html(
            $('.book-via-sms.teachers-to-book').val()
        );

        $('.shift-type-prompt').html(
            $('.book-via-sms.shift-type-value').val()
        );

        $('#book-request-confirmation').modal({
            backdrop : 'static',
            keyboard : false,
            show : true
        });

        $('.book-request.confirm').off().on('click', function() {
            $('#book-request-confirmation').modal('hide');
            createSmsBookingRequest();
        });

    }

    function createSmsBookingRequest() {

        var $availableTeachersElements = $('.available-teachers-to-book');
        var $excludedTeachersElements = $('.excluded-teachers-to-book');
        var teacherList = [];

        $.each($availableTeachersElements, function(i,v) {
            teacherList.push({
                'delay' : $(v).children().find('.request-delay').val(),
                'teacherId' : $(v).attr('data-teacher-id'),
                'priority' : $(v).attr('data-priority')
            });
        });

        if ($excludedTeachersElements.length>0) {
            $.each($excludedTeachersElements, function(i,v) {
                teacherList.push({
                    'delay' : 0,
                    'teacherId' : $(v).attr('data-teacher-id'),
                    'priority' : -1,
                    'exclusion' : 1
                });
            });
        }

        var shiftType = $('.book-via-sms.shift-type-value').attr('data-selected-shift-type-id');
        var shiftTypeName = CONFIG.shiftTypeMap[shiftType];

        var DateFrom = moment(
            new Date($('.book-via-sms-shift-list-pickers.datepicker.date-from').val())
        ).format();

        var DateTo = moment(
            new Date($('.book-via-sms-shift-list-pickers.datepicker.date-to').val())
        ).format();

        var params = {
            dateFrom        : DateFrom,
            dateTo          : DateTo,
            timeFrom        : $('.book-via-sms-shift-list-pickers.timepicker.time-from').val(),
            timeTo          : $('.book-via-sms-shift-list-pickers.timepicker.time-to').val(),
            teachersToBook  : $('.book-via-sms.teachers-to-book').val(),
            shiftTypeName   : shiftTypeName,
            isMultiple      : 0,
            schoolId        : window['schoolId'],
            teacherList     : teacherList
        };

        if (params.dateTo > params.dateFrom) {
            params.isMultiple = 1;
        }

        CONFIG.CREATE_BOOKING_REQUEST_CONFIG.url = 'booking/create';
        CONFIG.CREATE_BOOKING_REQUEST_CONFIG.data = params;

        $.ajax(CONFIG.CREATE_BOOKING_REQUEST_CONFIG);
    }

    /**
     * Success
     * @param result
     */
    function createBookingSuccessHandler(result) {

        if (result.status && result.status == "success") {

            $('#sms-available-teachers-container').html("");
            $('#sms-available-teachers-container').show();

            $('.book-via-sms.booking-ok').show('slow');
            $('.book-via-sms.submit').hide();

            $('#sms-booking-request-loading').hide();
            $('#manual-override-function').hide();

            showBat();

            setTimeout(function() {
                $('.book-via-sms.booking-ok').hide();
            }, 7000);
        }
    }

    /**
     * Before
     * @param xhr
     * @param status
     * @param error
     */
    function createBookingBeforeHandler(xhr, status, error) {
        $('#sms-available-teachers-container').hide();
        $('#sms-booking-request-loading').show();
    }

    /**
     * Error
     * @param result
     * @param status
     * @param xhr
     */
    function createBookingErrorHandler(result, status, xhr) {
    }

    /**
     * Complete
     * @param jqXHR
     * @param textStatus
     */
    function createBookingCompleteHandler(jqXHR, textStatus) {

    }


    /**
     * GoShift App School SMS Book Module
     * @returns {boolean}
     */
    function initModule() {

        initBookingPageDatePickers();
        initSearchButton();
        initShiftTypeChoiceInput();
        initTimeIntervalChoiceInputs();
        initTeachersToBookSelect();
        initBackToSchoolPageButton();
        initResetButton();
        initManualOverrideActions();
        initCreateSmsBookingRequestButton();
        initBackToHistoryPageButton();
        initDynamicSearch();

        return true;
    }

    /**
     * return facade
     */
    return {
        init : initModule
    }

}(jQuery)));