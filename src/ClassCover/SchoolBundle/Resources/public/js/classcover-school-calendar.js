ClassCover.module('SchoolCalendar', (function($){

    var CONFIG = {
        SCHOOL_TIMES : {
            "morning_start":"06:00",
            "morning_end":"14:00",
            "evening_start":"14:00",
            "evening_end":"22:00",
            "night_start":"22:00",
            "night_end":"06:00",
            "break_times":[]
        },
        BOOKINGS_JSON_REQUEST_CONFIG : {
            success  : bookingCallbackResponseHandler,
            url      : Routing.generate('class_cover_school_calendar_booking'),
            dataType : 'JSON',
            type     : 'POST'
        },
        shiftTypeMap : {
            '1' : 'Morning',
            '2' : 'Evening',
            '3' : 'Night',
            '0' : 'Special'
        },
        defaultShiftType : {
            interval : ['06:00', '14:00']
        },
        searchDateFieldConfig : {
            dateFormat: "dd M, yy",
            defaultDate : 1,
            DEFAULT_DATE : new Date($('#default-date-for-search').val())
        }
    };

    function initCalendar() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            height: 605,
            defaultView: 'agendaWeek',
            firstDay: 1,
            editable: false,
            selectable: false,
            selectHelper: false,
            slotDuration: '01:00:00',
            defaultDate: $('#default-date-for-search'),
            allDaySlot: false,
            slotEventOverlap: true,
            timeFormat: 'H:mm',
            events: function(start, end, timezone, callback) {
                jQuery.ajax({
                    url: Routing.generate('class_cover_school_calendar_booking'),
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        start: start.format(),
                        end: end.format(),
                        schoolId : window['schoolId']
                    },
                    success: function(events) {
                        callback(events);
                    }
                });
            },
            eventColor: '#553F00',
            eventClick: function(calEvent, jsEvent, view) {
                ClassCover.modules()['SchoolManualBook'].calendarBookingClickHandler(calEvent);

            },
            columnFormat: {
                month: 'ddd',
                week: 'ddd D/M',
                day: 'dddd D/M'
            }
        });
    }

    function bookingCallbackResponseHandler(data) {

    }

    function initDatePickers() {

        var dpConfig = {
            dateFormat: "dd M, yy",
            defaultDate : 1
        };

        $('.calendar-report-pickers.date-from')
            .datepicker(dpConfig)
            .datepicker('setDate', new Date());

        $('.calendar-report-pickers.date-to')
            .datepicker(dpConfig)
            .datepicker('setDate', new Date());

        $('.teacher-shift-report').on('click', function() {

            $('#download-casual-shift-report').modal({
                backdrop : 'static',
                keyboard : false,
                show : true
            });

        });
    }

    function initAdditionalButtons() {
        $('.download-report-confirm').on('click', function(){
            var dateFrom = moment($('.calendar-report-pickers.date-from').datepicker('getDate')).format();
            var dateTo   = moment($('.calendar-report-pickers.date-to').datepicker('getDate')).format();
            var casual   = $('.report-casuals-selection').val();
            var requestParams = {
                dateFrom : dateFrom,
                dateTo : dateTo
            };
            if (casual > 0) {
                requestParams.casualId = casual;
            }
            $('#download-casual-shift-report').modal('hide');
            var downloadUri = Routing.generate('class_cover_download_casuals_report', requestParams);
            window.location = downloadUri;
        });
    }

    function initShiftChoice() {
        $('.shift-type-choice > li > a').off().on('click', shiftTypeChoiceHandler);
    }

    function shiftTypeChoiceHandler(e) {

        var destId = $(this).parent().parent().attr('id');

        var shiftTypeId = parseInt($(this).attr('id'));
        var typeName = CONFIG.shiftTypeMap[shiftTypeId];
        setChosenShiftValue(shiftTypeId, typeName, destId);

        if (0 != shiftTypeId) {
            var interval = getIntervalByShiftTypeId(shiftTypeId);
            if (false != interval) {
                if (shiftTypeId == 3) {
                    setTimeChoiceIntervals(interval, true, destId);
                } else {
                    setTimeChoiceIntervals(interval, false, destId);
                }
            }
        }
    }

    function getIntervalByShiftTypeId(shiftTypeId) {

        if (shiftTypeId == 1) {
            return ['06:00', '14:00'];
        } else if (shiftTypeId == 2) {
            return ['14:00', '22:00'];
        } else if (shiftTypeId == 3) {
            return ['22:00', '06:00'];
        }

        return false;
    }

    function setChosenShiftValue(id, name, displayContainer) {

        var destContainerId = '#' + displayContainer +"-value";

        var $shiftTypeDisplay = $(destContainerId);
        $shiftTypeDisplay.attr('data-selected-shift-type-id', id);
        $shiftTypeDisplay.val(name);
    }

    function setTimeChoiceIntervals(interval, overNight, displayContainer) {

        var destContainerId = '.'+ displayContainer + '-pickers';

        $(destContainerId + '.timepicker.time-from').val(interval[0]);
        $(destContainerId + '.timepicker.time-to').val(interval[1]);

        if (overNight == true) {
            var $el = $(destContainerId + '.datepicker.date-to');
            var dateNext = $el.datepicker('getDate');

            dateNext.setDate(dateNext.getDate()+1);
            $el.datepicker('setDate', dateNext);
        } else {
            var oPDate = $(destContainerId + '.datepicker.date-from').datepicker('getDate');
            $(destContainerId + 'datepicker.date-to').datepicker('setDate', oPDate);
        }
    }

    function initModule() {
        initCalendar();
        initDatePickers();
        initShiftChoice();
        initAdditionalButtons();
        return true;
    }

    /**
     * return facade
     */
    return {
        init : initModule
    }

}(jQuery)));
