ClassCover.module('SchoolBook', (function($){

    /**
     * Config parameters
     * @type {{SEARCH_DATE_FIELD_CONFIG: {dateFormat: string, defaultDate: number, onSelect: datePickerOnSelectHandler}, TEACHER_AVAILABILITY_REQUEST_CONFIG: {success: searchTeachersRequestSuccessHandler, beforeSend: searchTeachersRequestBeforeSendHandler, error: searchTeachersRequestErrorHandler, complete: searchTeachersRequestCompleteHandler, dataType: string}}}
     */
    var CONFIG = {
        SEARCH_DATE_FIELD_CONFIG : {
            dateFormat: "dd M, yy",
            defaultDate : 1,
            onSelect : datePickerOnSelectHandler
        },
        HISTORY_SEARCH_DATE_FIELD_CONFIG : {
            dateFormat: "dd M, yy",
            defaultDate : 1
        },
        TEACHER_AVAILABILITY_REQUEST_CONFIG : {
            success : searchTeachersRequestSuccessHandler,
            beforeSend : searchTeachersRequestBeforeSendHandler,
            error : searchTeachersRequestErrorHandler,
            complete : searchTeachersRequestCompleteHandler,
            dataType : 'html'
        },
        SCHOOL_BOOKING_HISTORY_REQUEST_CONFIG : {
            success : schoolBookingHistorySuccessHandler,
            beforeSend : schoolBookingHistoryBeforeHandler,
            error : schoolBookingHistoryErrorHandler,
            complete : schoolBookingHistoryCompleteHandler,
            dataType : 'html',
            type: 'POST'
        },
        CANCEL_BOOKING_REQUEST_CONFIG : {
            success : cancelBookingSuccessHandler,
            dataType : 'json',
            type: 'POST'
        },
        SECTION_TITLE : 'Book A CYO'
    };

    var TEMP = {};

    function datePickerOnSelectHandler() {

        var dateObject = $(this).datepicker('getDate');

        if (null == dateObject) {
            $('.search-variable-date').attr('disabled', 'disabled');
        } else {
            $('.search-variable-date').removeAttr('disabled');
        }
    }

    function datePickerKeyUpHandler() {
        var dateObject = $(this).datepicker('getDate');

        if (null == dateObject) {
            $('.search-variable-date').attr('disabled', 'disabled');
        } else {
            $('.search-variable-date').removeAttr('disabled');
        }
    }

    function historyDatePickerOnSelectHandler() {
        //fireSchoolBookingHistoryRequest();
    }

    function historyShiftTypeChoiceHandler() {
        //fireSchoolBookingHistoryRequest();
    }


    /**
     *
     * @param date
     * @param schoolId
     */
    function fireSearchTeachersRequest(date, schoolId) {

        date = date || $('#default_search_date').val();
        schoolId = window['schoolId'];

        $("#datetime").datepicker('setDate', new Date(date));
        $('#current_search_date').val(date);

        var $dateShowElement = $('.current-date-display');
        $dateShowElement.html(moment(
            new Date(date)
        ).format('dddd, MMMM Do, YYYY'));

        CONFIG.TEACHER_AVAILABILITY_REQUEST_CONFIG.url = Routing.generate('class_cover_centre_search_booking_teachers_ajax', { date : date, schoolId : schoolId });
        $.ajax(CONFIG.TEACHER_AVAILABILITY_REQUEST_CONFIG);
    }

    /**
     *
     * @param type
     */
    function fireSchoolBookingHistoryRequest(type) {

        var params = {
            schoolId  : window['schoolId'],
            type      : type || null
        };

        if ($('.sms-history.datepicker-history.date-from').val() != "" && $('.sms-history.datepicker-history.date-to').val() != "") {
            params.dateFrom = $('.sms-history.datepicker-history.date-from').val();
            params.dateTo = $('.sms-history.datepicker-history.date-to').val();
        }

        CONFIG.SCHOOL_BOOKING_HISTORY_REQUEST_CONFIG.url = Routing.generate('class_cover_school_sms_booking_history_ajax');
        CONFIG.SCHOOL_BOOKING_HISTORY_REQUEST_CONFIG.data = params;

        $.ajax(CONFIG.SCHOOL_BOOKING_HISTORY_REQUEST_CONFIG);
    }

    /**
     * Search Request Before - Perform something before search
     * @param xhr
     */
    function searchTeachersRequestBeforeSendHandler(xhr) {
        $('.operational-buttons').attr('disabled', 'disabled');
    }

    /**
     * Search Success Handler - Populates the Availability table with results
     * @param result
     * @param status
     * @param xhr
     */
    function searchTeachersRequestSuccessHandler(result, status, xhr) {
        if (200 == xhr.status && '' != result) {
            $('table#availability-table > tbody').html(result);
        }
    }

    /**
     * Removes disable state of search buttons when request is complete
     * @param jqXHR
     * @param textStatus
     */
    function searchTeachersRequestCompleteHandler(jqXHR, textStatus) {
        $('.operational-buttons').removeAttr('disabled');
    }

    /**
     * Search Request Error Handler - Basically we load a simple notification with details
     * @param xhr
     * @param status
     * @param error
     */
    function searchTeachersRequestErrorHandler(xhr, status, error) {

    }

    /**
     *
     * @param result
     * @param status
     * @param xhr
     */
    function schoolBookingHistorySuccessHandler(result, status, xhr) {
        if (200 == xhr.status && '' != result) {
            $('#school-sms-history-result-container').html(result);
            $('.expand-booking').on('click', expandButtonClickHandler);
            $('.cancel-booking').on('click', cancelBookingButtonClickHandler);
        }
    }

    /**
     *
     * @param xhr
     */
    function schoolBookingHistoryBeforeHandler(xhr) {

    }

    /**
     *
     * @param xhr
     * @param status
     * @param error
     */
    function schoolBookingHistoryErrorHandler(xhr, status, error) {

    }

    /**
     *
     * @param jqXHR
     * @param textStatus
     */
    function schoolBookingHistoryCompleteHandler(jqXHR, textStatus) {

    }

    function expandButtonClickHandler() {
        $('.row-booking-sms-list').hide();
        var id = $(this).attr('data-booking-id');
        var $el = $('tr#bs_' + id);
        if (!$el.hasClass('expanded')) {
            $el.show('slow');
            $el.addClass('expanded');
            $('.expand-booking').html('View');
            $(this).html('Close');
        } else {
            $el.hide('slow');
            $el.removeClass('expanded');
            $(this).html('View');
        }
    }


    function cancelBookingButtonClickHandler() {
        TEMP.bookingIdToCancel = parseInt($(this).attr('data-booking-id'));

        $('#confirm-sms-booking-cancel').modal({
            backdrop : 'static',
            keyboard : false,
            show : true
        });

    }

    function bookingCancelConfirmClickHandler() {

        if (TEMP.bookingIdToCancel) {
            $('#confirm-sms-booking-cancel').modal('hide');

            CONFIG.CANCEL_BOOKING_REQUEST_CONFIG.url = Routing.generate('class_cover_centre_cancel_booking');
            CONFIG.CANCEL_BOOKING_REQUEST_CONFIG.data = { bookingId : TEMP.bookingIdToCancel };

            $.ajax(CONFIG.CANCEL_BOOKING_REQUEST_CONFIG);
        }

    }


    function cancelBookingSuccessHandler(response) {
        if (response.status == "success") {
            fireSchoolBookingHistoryRequest();
            delete TEMP.bookingIdToCancel;
        }
    }

    /** END SEARCH AJAX HANDLERS **/

    // *************************************************************** //

    /** BEGIN FORM SEARCH HANDLERS **/

    /**
     * Search days with buttons handler
     * @param e
     */
    function searchFixedDatesClickHandler(e) {
        var searchDate      = $(this).attr('data-search-date');
        fireSearchTeachersRequest(searchDate);
    }

    /**
     * Search Dates with date picker handler
     * @param e
     */
    function searchVariableDatesClickHandler(e) {
        var m = moment($('#datetime').datepicker('getDate'));
        if (m.isValid()) {
            fireSearchTeachersRequest(m.format('YYYY-MM-DD'));
        }
    }

    /**
     * Init Date picker with config
     */
    function initDatePicker() {
        var $dateTime = $("#datetime");
        $dateTime.datepicker(CONFIG.SEARCH_DATE_FIELD_CONFIG);
        $dateTime.on('keyup', datePickerKeyUpHandler);
    }

    function initHistoryDatePicker() {

        var $historyDateFrom = $(".sms-history.datepicker-history.date-from");
        var $historyDateTo = $(".sms-history.datepicker-history.date-to");

        $historyDateFrom.datepicker(CONFIG.HISTORY_SEARCH_DATE_FIELD_CONFIG);
            //.datepicker('setDate', new Date());

        $historyDateTo.datepicker(CONFIG.HISTORY_SEARCH_DATE_FIELD_CONFIG);
            //.datepicker('setDate', new Date());

        $('.search-history-fire').on('click', function() {
            var histType = $(this).attr('hist-type');
            fireSchoolBookingHistoryRequest(histType);
        });

    }

    /**
     * Init Date Prev/Next Seekers
     */
    function initDateSeekers() {

        var $dateShowElement = $('.current-date-display');
        $dateShowElement.html(moment().format('dddd, MMMM Do, YYYY'));

        var $prevSeekSelector = $('.prev-day');
        var $nextSeekSelector = $('.next-day');

        $prevSeekSelector.on('click', prevDaySeekClickHandler);
        $nextSeekSelector.on('click', nextDaySeekClickHandler);
    }

    function nextDaySeekClickHandler(e) {
        var currentDate = new Date($('#current_search_date').val());
        fireSearchTeachersRequest(moment(currentDate).add(1, 'day').format('YYYY-MM-DD'));
    }

    function prevDaySeekClickHandler(e) {
        var currentDate = new Date($('#current_search_date').val());
        fireSearchTeachersRequest(moment(currentDate).subtract(1, 'day').format('YYYY-MM-DD'));
    }

    /**
     * Init search handlers
     */
    function initSearchHandlers() {
        $('.search-fixed-date').on('click', searchFixedDatesClickHandler);
        $('.search-variable-date').on('click', searchVariableDatesClickHandler);
    }

    function initBookCyoHandlers() {
        var $bookCyoButton = $('.book-cyo');
        $bookCyoButton.on('click', bookCyoButtonClickHandler);
    }

    function initCancelBookingClickHandler() {
        $('.book-via-sms.booking-cancel-confirm').on('click', bookingCancelConfirmClickHandler);
    }

    function initHistoryButtonClickHandler() {
        var $showHistoryButton = $('.sms-history');
        $showHistoryButton.on('click', showHistoryButtonHandler)
    }

    function initHistoryShiftTypeChoiceInput() {
        $('.sms-history.shift-type-choice > li > a').on('click', historyShiftTypeChoiceHandler);
    }

    function initHistoryBookingSearch() {
        $('.search-history-fire').on('click', function() {
            fireSchoolBookingHistoryRequest();
        });
    }

    function bookCyoButtonClickHandler() {
        $('.sub-page-ta-main').hide();
        $('.sub-page-book-via-sms').show();
    }

    function showHistoryButtonHandler() {
        $('.sub-page-ta-main').hide();
        $('.sub-page-school-booking-history').show();
        fireSchoolBookingHistoryRequest();
    }


    function initModule() {

        initDatePicker();
        initHistoryDatePicker();
        initDateSeekers();
        initSearchHandlers();
        initBookCyoHandlers();
        initHistoryButtonClickHandler();
        initHistoryShiftTypeChoiceInput();
        fireSearchTeachersRequest();
        initCancelBookingClickHandler();
    }

    /**
     * return facade
     */
    return {
        init : initModule,
        fireSearchTeachersRequest : fireSearchTeachersRequest,
        fireSchoolBookingHistoryRequest : fireSchoolBookingHistoryRequest
    }

}(jQuery)));