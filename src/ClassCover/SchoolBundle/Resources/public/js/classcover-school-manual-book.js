ClassCover.module('SchoolManualBook', (function($){

    var CONFIG = {
        searchDateFieldConfig : {
            dateFormat: "dd M, yy",
            defaultDate : 1,
            onSelect : formDateTimeSelectorsHandler
        },
        shiftTypeMap : {
            '1' : 'Morning',
            '2' : 'Evening',
            '3' : 'Night',
            '0' : 'Special'
        },
        defaultShiftType : {
            interval : ['06:00', '14:00']
        },
        SECTION_TITLE : 'Create Manual Booking',
        DEFAULT_DATE : new Date($('#default-date-for-search').val()),
        TEACHER_AVAILABILITY_REQUEST_CONFIG : {
            success : function(data) {
                if ("success" == data.status) {
                    $('.book-manually.booking-ok').show();
                    setTimeout(function() {
                        $('#confirm-booking-override-manual').modal('hide');
                        $('.book-manually.booking-ok').hide();
                    }, 1500);
                    ClassCover.modules()['SchoolBook'].fireSearchTeachersRequest()
                } else {
                    if (data.errors.length>0) {
                        $('.book-manually.booking-error').html(data.errors[0]).show();

                        //todo : decide when to remove this if not needed on a longer term
                        /*setTimeout(function() {
                            $('.book-manually.booking-error').hide('slow');
                        }, 3000);*/
                    }
                }
            },
            dataType : 'JSON',
            type : 'POST'
        },
        BOOKING_INFO_REQUEST_CONFIG : {
            success : bookingInfoSuccessHandler,
            dataType : 'JSON',
            type : 'GET'
        },
        BOOKING_UPDATE_REQUEST_CONFIG : {
            success : bookingUpdateSuccessHandler,
            dataType: 'JSON',
            type : 'POST'
        },
        CANCEL_BOOKING_REQUEST_CONFIG : {
            success : cancelBookingSuccessHandler,
            dataType : 'json',
            type: 'POST'
        }
    };

    var TEMP = {};

    function initBookingPageDatePickers() {

        var $dateFrom = $(".book-manually-shift-list-pickers.datepicker.date-from");
        var $dateTo = $(".book-manually-shift-list-pickers.datepicker.date-to");

        $dateFrom.datepicker(CONFIG.searchDateFieldConfig)
            .on('keyUp', datePickerKeyHandler)
            .on('keyPress', datePickerKeyHandler)
            .datepicker('setDate', CONFIG.DEFAULT_DATE);

        $dateTo.datepicker(CONFIG.searchDateFieldConfig)
            .on('keyUp', datePickerKeyHandler)
            .on('keyPress', datePickerKeyHandler)
            .datepicker('setDate', CONFIG.DEFAULT_DATE);

        $(".book-manually-update-shift-list-pickers.datepicker.date-from").datepicker(CONFIG.searchDateFieldConfig);
        $(".book-manually-update-shift-list-pickers.datepicker.date-to").datepicker(CONFIG.searchDateFieldConfig);

    }


    /**
     * Prevent user key input
     * @param event
     * @returns {boolean}
     */
    function datePickerKeyHandler(event)
    {
        event.preventDefault();
        return false;
    }

    var tmp = {};

    function initCellClick() {
        $('#availability-table').delegate('.av-cell:not(.non-vacant)', 'click', nonVacantCellClickHandler);
        $('#availability-table').delegate('.av-cell.non-vacant', 'click', vacantClickHandler);
    }

    function initManualOverrideActions() {
        $('.book-manually.override-confirm').on('click', overrideConfirmClickHandler);
        $('.book-manually.override-abort').on('click', overrideAbortClickHandler);
    }

    /**
     * Attempt to book manually
     * @param e
     */
    function nonVacantCellClickHandler(e) {

        var startHour = parseInt($(this).attr('data-start-hour'));

        var startDate = moment(new Date($('#current_search_date').val()))
            .hour(startHour)
            .minute(0)
            .seconds(0);

        var endDate = moment(startDate)
            .add(8, 'hours');

        tmp.startHour = startDate.format("HH:mm");
        tmp.endHour = endDate.format("HH:mm");
        tmp.teacherId = $(this).parent().attr('data-teacher-id');
        tmp.teacherName = $(this).parent().attr('data-teacher-name');
        tmp.shiftTypeId = getShiftTypeIdByInterval(tmp.startHour, tmp.endHour);
        tmp.shiftTypeName = CONFIG.shiftTypeMap[tmp.shiftTypeId];

        tmp.startDay = startDate;
        tmp.endDay = endDate;

        showOverridePrompt();
    }

    /**
     * Attempt to manually modify an existing booking
     * @param e
     * @returns {boolean}
     */
    function vacantClickHandler(e) {

        var bookingId = parseInt($(this).attr('data-entity-id'));
        var teacherId = parseInt($(this).parent().attr('data-teacher-id'));

        if (bookingId>0) {

            $('#manual-override-by').val('');
            $('#manual-override-reason').val('');
            $('#confirm-override').modal({
                backdrop : 'static',
                keyboard : false,
                show : true
            });

            $('.book-via-sms.override-confirm').off().on('click', function() {

                if($('#manual-override-by').val() == '' || $('#manual-override-reason').val() == '') {
                    alert('Please enter a Reason and who this was Authorised by!');
                    return false;
                }

                $('#confirm-override').modal('hide');
                CONFIG.BOOKING_INFO_REQUEST_CONFIG.url = Routing.generate('class_cover_centre_get_booking_info', { bookingId : bookingId, teacherId : teacherId });
                $.ajax(CONFIG.BOOKING_INFO_REQUEST_CONFIG);
            });

        } else {
            e.preventDefault();
            return false;
        }
    }

    /**
     * 
     * @param calendarEvent
     * @param requirePrompt
     * @returns {boolean}
     */
    function calendarBookingClickHandler(calendarEvent, requirePrompt) {

        var bookingId = parseInt(calendarEvent.bookingId);
        var teacherId = parseInt(calendarEvent.teacherId);
        var prompt = requirePrompt|| false;

        if (bookingId>0) {

            if (prompt) {
                $('#manual-override-by').val('');
                $('#manual-override-reason').val('');
                $('#confirm-override').modal({
                    backdrop : 'static',
                    keyboard : false,
                    show : true
                });

                $('.book-via-sms.override-confirm').off().on('click', function() {
                    $('#confirm-override').modal('hide');

                });
            } else {
                CONFIG.BOOKING_INFO_REQUEST_CONFIG.url = Routing.generate('class_cover_centre_get_booking_info', { bookingId : bookingId, teacherId : teacherId });
                $.ajax(CONFIG.BOOKING_INFO_REQUEST_CONFIG);
            }

        } else {
            e.preventDefault();
            return false;
        }
    }


    function bookingInfoSuccessHandler(response) {

        var shiftTypeId = getShiftTypeIdByInterval(response.data.timeStart, response.data.timeEnd);
        var shiftTypeName = response.data.shiftType.charAt(0).toUpperCase() + response.data.shiftType.slice(1);

        $('.book-manually-update.casual-name').html(response.data.teacherName);
        $('.book-manually-update-shift-list-pickers.datepicker.date-from').datepicker('setDate', new Date(response.data.dateStart));
        $('.book-manually-update-shift-list-pickers.datepicker.date-to').datepicker('setDate', new Date(response.data.dateEnd));

        $('.book-manually-update-shift-list-pickers.timepicker.time-from').val(response.data.timeStart);
        $('.book-manually-update-shift-list-pickers.timepicker.time-to').val(response.data.timeEnd);
        $('#book-manually-update-shift-list-value').attr('data-selected-shift-type-id', shiftTypeId);
        $('#book-manually-update-shift-list-value').val(shiftTypeName);

        $('.booking-update-cancel-confirm').off().on('click', function() {

            TEMP.bookingIdToCancel = parseInt(response.data.bookingId);
            $('#confirm-change-booking-parameters').modal('hide');
            $('#confirm-sms-booking-cancel').modal({
                backdrop : 'static',
                keyboard : false,
                show : true
            });

            $('.book-via-sms.booking-cancel-confirm').off().on('click', function() {

                if (TEMP.bookingIdToCancel) {
                    $('#confirm-sms-booking-cancel').modal('hide');

                    CONFIG.CANCEL_BOOKING_REQUEST_CONFIG.url = Routing.generate('class_cover_centre_cancel_booking');
                    CONFIG.CANCEL_BOOKING_REQUEST_CONFIG.data = { bookingId : TEMP.bookingIdToCancel };

                    $.ajax(CONFIG.CANCEL_BOOKING_REQUEST_CONFIG);
                }
            });

        });

        $('.booking-update-confirm').off().on('click', function() {

            TEMP.bookingIdToUpdate = parseInt(response.data.bookingId);
            TEMP.teacherIdToUpdateBooking = parseInt(response.data.teacherId);

            var shiftType = $('.book-manually-update.shift-type-value').attr('data-selected-shift-type-id');
            var shiftTypeName = CONFIG.shiftTypeMap[shiftType];

            var DateFrom = moment(
                new Date(
                    $('.book-manually-update-shift-list-pickers.datepicker.date-from').val()
                )
            ).format();

            var DateTo = moment(
                new Date(
                    $('.book-manually-update-shift-list-pickers.datepicker.date-to').val()
                )
            ).format();

            var params = {
                dateFrom        : DateFrom,
                dateTo          : DateTo,
                timeFrom        : $('.book-manually-update-shift-list-pickers.timepicker.time-from').val(),
                timeTo          : $('.book-manually-update-shift-list-pickers.timepicker.time-to').val(),
                schoolId        : window['schoolId'],
                teacherId       : TEMP.teacherIdToUpdateBooking,
                bookingId       : TEMP.bookingIdToUpdate,
                shiftTypeName   : shiftTypeName
            };

            CONFIG.BOOKING_UPDATE_REQUEST_CONFIG.url = Routing.generate('class_cover_booking_update_booking_ajax');
            CONFIG.BOOKING_UPDATE_REQUEST_CONFIG.data = params;
            $.ajax(CONFIG.BOOKING_UPDATE_REQUEST_CONFIG);

        });

        var shiftTypeId_ = getShiftTypeIdByInterval(response.data.timeStart, response.data.timeEnd);
        var typeName = CONFIG.shiftTypeMap[shiftTypeId_];

        $('#bmbc-shift-type-value').attr('data-selected-shift-type-id', shiftTypeId_);
        $('#bmbc-shift-type-value').val(typeName);

        showBookingUpdatePrompt();
    }

    function cancelBookingSuccessHandler(response) {
        if (response.status == "success") {
            ClassCover.modules()['SchoolBook'].fireSearchTeachersRequest();
            delete TEMP.bookingIdToCancel;

            if (window.location.pathname.indexOf('calendar') != -1) {
                location.reload();
            }
        }
    }

    function bookingUpdateSuccessHandler(response)
    {
        if (response.status == 'success') {
            $('#confirm-change-booking-parameters').modal('hide');
            ClassCover.modules()['SchoolBook'].fireSearchTeachersRequest();
            delete TEMP.bookingIdToUpdate;
            delete TEMP.teacherIdToUpdateBooking;

            if (window.location.pathname.indexOf('calendar') != -1) {
                location.reload();
            }
        }
    }


    function showBookingUpdatePrompt()
    {
        $('#confirm-change-booking-parameters').modal({
            backdrop : 'static',
            keyboard : false,
            show : true
        })
    }

    function showOverridePrompt() {

        $('#manual-booking-override-reason').val('');
        $('#manual-booking-override-by').val('');

        $('#confirm-override-manual').modal({
            backdrop : 'static',
            keyboard : false,
            show : true
        }).on('hidden.bs.modal', function () {
            tmp = {};
        });
    }

    function showBookingConfirmPrompt() {

        $('.book-manually.casual-name').html(tmp.teacherName).attr('data-teacher-id', tmp.teacherId);

        $(".book-manually-shift-list-pickers.datepicker.date-from").datepicker('setDate',
            new Date(tmp.startDay)
        );
        $(".book-manually-shift-list-pickers.datepicker.date-to").datepicker('setDate',
            new Date(tmp.endDay)
        );

        $(".book-manually-shift-list-pickers.timepicker.time-from").val(tmp.startHour);
        $(".book-manually-shift-list-pickers.timepicker.time-to").val(tmp.endHour);

        $("#book-manually-shift-list-value").val(tmp.shiftTypeName);
        $("#book-manually-shift-list-value").attr('data-selected-shift-type-id', tmp.shiftTypeId);
        
        $('#confirm-booking-override-manual').modal({
            backdrop : 'static',
            keyboard : false,
            show : true
        }).on('hidden.bs.modal', function() {
            tmp = {};
        });

    }

    function overrideConfirmClickHandler() {

        if($('#manual-booking-override-by').val() == '' || $('#manual-booking-override-reason').val() == '') {
            alert('Please enter a Reason and who this was Authorised by!');
            return false;
        }

        $('#confirm-override-manual').modal('hide');
        showBookingConfirmPrompt();
    }

    function overrideAbortClickHandler() {

    }

    function formDateTimeSelectorsHandler() {
        if ($(this).hasClass('date-from')) {
            var fromPickerSelectedDate = $(this).datepicker('getDate');
            $(".book-manually-update-shift-list-pickers.datepicker.date-to").datepicker('setDate', fromPickerSelectedDate);
            $(".book-manually-update-shift-list-pickers.datepicker.date-to").datepicker('setDate', fromPickerSelectedDate);
        }
    }


    /** TIME PICKERS **/
    function initTimeIntervalChoiceInputs() {

        var $timeFrom = $('.book-manually-shift-list-pickers.timepicker.time-from');
        var $timeTo = $('.book-manually-shift-list-pickers.timepicker.time-to');

        setTimeChoiceIntervals(CONFIG.defaultShiftType.interval, false, 'book-manually-shift-list');

        $timeFrom.on('change', timeChoiceChangeHandler);
        $timeTo.on('change', timeChoiceChangeHandler);
    }

    function timeChoiceChangeHandler() {

    }

    function shiftTypeChoiceHandler() {

        var destId = $(this).parent().parent().attr('id');

        var shiftTypeId = parseInt($(this).attr('id'));
        var typeName = CONFIG.shiftTypeMap[shiftTypeId];
        setChosenShiftValue(shiftTypeId, typeName, destId);

        if (0 != shiftTypeId) {
            var interval = getIntervalByShiftTypeId(shiftTypeId);
            if (false != interval) {
                if (shiftTypeId == 3) {
                    setTimeChoiceIntervals(interval, true, destId);
                } else {
                    setTimeChoiceIntervals(interval, false, destId);
                }

            }
        }
    }

    function getShiftTypeIdByInterval(from, to) {

        if (from == '06:00' && to == '14:00') {
            return 1;
        } else if (from == '14:00' && to == '22:00') {
            return 2;
        } else if (from == '22:00' && to == '06:00') {
            return 3;
        } else {
            return 0;
        }
    }

    function getShiftTypeByHours(from, to) {

        if (from == 6 && to == 14) {
            return 1;
        } else if (from == 14 && to == 22) {
            return 2;
        } else if (from == 22 && to == 6) {
            return 3;
        } else {
            return 0;
        }
    }

    function getIntervalByShiftTypeId(shiftTypeId) {

        if (shiftTypeId == 1) {
            return ['06:00', '14:00'];
        } else if (shiftTypeId == 2) {
            return ['14:00', '22:00'];
        } else if (shiftTypeId == 3) {
            return ['22:00', '06:00'];
        }

        return false;
    }

    function setChosenShiftValue(id, name, displayContainer) {

        var destContainerId = '#' +  displayContainer +"-value";

        var $shiftTypeDisplay = $(destContainerId);
        $shiftTypeDisplay.attr('data-selected-shift-type-id', id);
        $shiftTypeDisplay.val(name);
    }

    function setTimeChoiceIntervals(interval, overNight, displayContainer) {

        var destContainerId = '.'+ displayContainer + '-pickers';

        $(destContainerId + '.timepicker.time-from').val(interval[0]);
        $(destContainerId + '.timepicker.time-to').val(interval[1]);

        if (overNight == true) {
            var $el = $(destContainerId + '.datepicker.date-to');
            var dateNext = $el.datepicker('getDate');

            dateNext.setDate(dateNext.getDate()+1);
            $el.datepicker('setDate', dateNext);
        } else {
            var oPDate = $(destContainerId + '.datepicker.date-from').datepicker('getDate');
            $(destContainerId + '.datepicker.date-to').datepicker('setDate', oPDate);
        }
    }

    function initShiftTypeChoiceInput() {
        //$('.book-manually-update.shift-type-choice > li > a').on('click', shiftTypeChoiceHandler);
        $('.shift-type-choice > li > a').on('click', shiftTypeChoiceHandler);
    }
    /** TIME PICKERS END **/

    function searchSmsAvailableTeachers() {

        var shiftType = $('.book-manually.shift-type-value').attr('data-selected-shift-type-id');
        var shiftTypeName = CONFIG.shiftTypeMap[shiftType];

        var DateFrom = moment(
            new Date($('.book-manually-shift-list-pickers.datepicker.date-from').val())
        ).format();

        var DateTo = moment(
            new Date($('.book-manually-shift-list-pickers.datepicker.date-to').val())
        ).format();

        var params = {
            dateFrom        : DateFrom,
            dateTo          : DateTo,
            timeFrom        : $('.book-manually-shift-list-pickers.timepicker.time-from').val(),
            timeTo          : $('.book-manually-shift-list-pickers.timepicker.time-to').val(),
            shiftTypeId     : shiftType,
            shiftTypeName   : shiftTypeName,
            schoolId          : window['schoolId'],
            teacherId       : $('.book-manually.casual-name').attr('data-teacher-id')
        };


        CONFIG.TEACHER_AVAILABILITY_REQUEST_CONFIG.url = Routing.generate('class_cover_booking_search_manual_available_teachers_ajax');
        CONFIG.TEACHER_AVAILABILITY_REQUEST_CONFIG.data = params;
        $.ajax(CONFIG.TEACHER_AVAILABILITY_REQUEST_CONFIG);
    }

    function initConfirmBookingButtonHandler() {
        $('.book-manually.booking-override-confirm').on('click', function() {
           searchSmsAvailableTeachers();
        });
    }

    /**
     * GoShift App School SMS Book Module
     * @returns {boolean}
     */
    function initModule() {

        initCellClick();
        initManualOverrideActions();
        initBookingPageDatePickers();
        initTimeIntervalChoiceInputs();
        initShiftTypeChoiceInput();
        initConfirmBookingButtonHandler();

        return true;
    }

    /**
     * return facade
     */
    return {
        init : initModule,
        calendarBookingClickHandler : calendarBookingClickHandler
    }

}(jQuery)));