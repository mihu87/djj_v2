ClassCover.module('SchoolAdditionalUsers', (function($){

    var CONFIG = {
        ADDITIONAL_LOGIN_ADMIN_REQUEST_CONFIG : {
            success : additionalLoginAdminSetupSuccessHandler,
            dataType : 'json'
        }
    };

    function initAdminButtons() {
        $(".admin-handler").bootstrapSwitch({
            size : 'mini',
            onColor : 'success',
            offColor : 'danger',
            onSwitchChange : adminHandlerButtonClickHandler
        });
    }

    function adminHandlerButtonClickHandler() {

        var flag = $(this).is(':checked') == true ? 1 : 0;
        var entity = parseInt($(this).attr('data-id'));

        CONFIG.ADDITIONAL_LOGIN_ADMIN_REQUEST_CONFIG.url = Routing.generate('school_config_additional-users_admin', { entityId : entity , flag : flag });
        $.ajax(CONFIG.ADDITIONAL_LOGIN_ADMIN_REQUEST_CONFIG);
    }

    function additionalLoginAdminSetupSuccessHandler(response) {
        if ("success" == response.status && response.code) {
            if (response.code == 0) {
                $('.admin-handler[data-id="' + response.entity_id + '"').bootstrapSwitch({state:true});
            } else {
                $('.admin-handler[data-id="' + response.entity_id + '"').bootstrapSwitch({state:false});
            }
        }
    }

    function initModule() {
        initAdminButtons();
    }
    /**
     * return facade
     */
    return {
        init : initModule
    }

}(jQuery)));