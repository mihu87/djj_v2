<?php

namespace ClassCover\SchoolBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TeacherType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('surname')
            ->add('email')
           /* ->add('additionalEmail')
            ->add('photoFile')
            ->add('addressStreet')
            ->add('addressCity')
            ->add('addressState')
            ->add('addressCountry')
            ->add('addressPostCode')*/
            ->add('phone')
           // ->add('alternativePhone')
           // ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ClassCover\CyoBundle\Entity\Teacher'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'classcover_cyobundle_teacher';
    }
}
