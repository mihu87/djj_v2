<?php

namespace ClassCover\SchoolBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdditionalUsersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('surname')
            ->add('phone')
            ->add('user', 'email', [
                'label' => 'Email Address'
            ])
            ->add('school', 'entity', [
                'class' => 'ClassCoverSchoolBundle:School',
                'read_only' => true,
                'mapped' => true
           ]);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ClassCover\SchoolBundle\Entity\AdditionalUsers'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'classcover_schoolbundle_additionalusers';
    }
}
