<?php

namespace ClassCover\SchoolBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SchoolType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('shortName')
            ->add('phone')
            ->add('email')
            ->add('additionalEmail')
            ->add('addressStreet')
            ->add('addressCity')
            ->add('addressState')
            ->add('addressCountry')
            ->add('addressPostCode')
            //->add('latitude')
            //->add('longitude')
            ->add('website')
            //->add('studentNumber')
            ->add('principal')
            ->add('casualTeacherAdmin')
            ->add('casualTeacherAdminPhone')
            ->add('casualTeacherAdminEmail')
            ->add('overtimeThreshold')
            //->add('accessType')
            //->add('howDidYouHearText')
            //->add('timezoneId')
            //->add('createdOn')
            //->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ClassCover\SchoolBundle\Entity\School'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'classcover_schoolbundle_school';
    }
}
