<?php

namespace ClassCover\SchoolBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ClassCover\SchoolBundle\Entity\School;
use ClassCover\SchoolBundle\Form\SchoolType;
use FOS\UserBundle\Doctrine\UserManager;
use ClassCover\AppBundle\Entity\User;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
/**
 * School controller.
 *
 */
class SchoolController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ClassCoverSchoolBundle:School')->findAll();

        return $this->render('ClassCoverSchoolBundle:School:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function createAction(Request $request)
    {
        $entity = new School();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('school_config_show', array('id' => $entity->getId())));
        }

        return $this->render('ClassCoverSchoolBundle:School:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a School entity.
     *
     * @param School $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(School $entity)
    {
        $form = $this->createForm(new SchoolType(), $entity, array(
            'action' => $this->generateUrl('school_config_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function newAction()
    {
        $entity = new School();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClassCoverSchoolBundle:School:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClassCoverSchoolBundle:School')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find School entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClassCoverSchoolBundle:School:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $schoolId = $this->get('session')->get('school')->getId();
        /** @var School $entity */
        $entity = $em->getRepository('ClassCoverSchoolBundle:School')->find($schoolId);
        $id = $entity->getId();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find School entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClassCoverSchoolBundle:School:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a School entity.
    *
    * @param School $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(School $entity)
    {
        $form = $this->createForm(new SchoolType(), $entity, array(
            'action' => $this->generateUrl('school_config_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var School $entity */
        $entity = $em->getRepository('ClassCoverSchoolBundle:School')->find($id);

        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');

        $data = $request->request->all();
        $currentUserEmail = $data['classcover_schoolbundle_school']['email'];

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find School entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            /** @var User $user */
            $user = $entity->getUser();
            $user->setEmail($entity->getEmail());

            $userManager->updateUser($user);

            return $this->redirect($this->generateUrl('school_config_edit', array('id' => $id)));
        }

        return $this->render('ClassCoverSchoolBundle:School:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or (hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ClassCoverSchoolBundle:School')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find School entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('school_config'));
    }

    /**
     * Creates a form to delete a School entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('school_config_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
