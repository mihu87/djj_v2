<?php

namespace ClassCover\SchoolBundle\Controller;

use ClassCover\CyoBundle\Services\TeacherService;
use ClassCover\SchoolBundle\Entity\School;
use ClassCover\SchoolBundle\Services\SchoolService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ClassCover\SchoolBundle\Entity\AdditionalUsers;
use ClassCover\SchoolBundle\Form\AdditionalUsersType;
use FOS\UserBundle\Model\UserManager;
use ClassCover\AppBundle\Entity\User;

/**
 * AdditionalUsers controller.
 *
 */
class AdditionalUsersController extends Controller
{

    /**
     * Lists all AdditionalUsers entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $school = $em->getRepository('ClassCoverSchoolBundle:School')->findOneBy(['user' => $user]);

        $entities = $em->getRepository('ClassCoverSchoolBundle:AdditionalUsers')->findBy(['school' => $school]);

        return $this->render('ClassCoverSchoolBundle:AdditionalUsers:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new AdditionalUsers entity.
     *
     */
    public function createAction(Request $request)
    {
        $objects = $request->request->get('classcover_schoolbundle_additionalusers');

        if (!empty($objects['user'])) {

            $email = $objects['user'];
            $firstName = $objects['firstName'];
            $lastName = $objects['surname'];

            /** @var UserManager $userManager */
            $userManager = $this->get('fos_user.user_manager');
            /** @var User $user */
            $user = $userManager->createUser();

            $user->setUsername(strtolower($firstName . '.' . $lastName));
            $user->setEmail($email);
            $user->setPlainPassword(TeacherService::DEFAULT_PASSWORD);
            $user->addRole(SchoolService::ROLE_ADDITIONAL_USER);
            $user->setEnabled(true);

            $userManager->updateUser($user);

            $objects['user'] = $user;
        }

        $request->request->replace(['classcover_schoolbundle_additionalusers' => $objects]);

        $entity = new AdditionalUsers();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('school_config_additional-users', array('id' => $entity->getId())));
        }

        return $this->render('ClassCoverSchoolBundle:AdditionalUsers:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * @param $entityId
     * @param $flag
     * @return JsonResponse
     */
    public function adminAccessHandlerAction($entityId, $flag)
    {
        $response = [
            'status' => 'success',
            'errors' => [],
            'entity_id' => $entityId,
            'code' => $flag
        ];

        $em = $this->getDoctrine()->getManager();
        /** @var UserManager $userManager */
        $userManager = $this->get('fos_user.user_manager');

        /** @var AdditionalUsers $info */
        $info = $em->getRepository('ClassCoverSchoolBundle:AdditionalUsers')->find($entityId);

        if ($info==null) {
            $response['status'] = 'error';
            $response['errors'][] = 'Cannot find entity for id ' . $entityId;

            return new JsonResponse($response);
        }

        $flag = (int) $flag;
        /** @var User $user */
        $user = $info->getUser();

        if ($flag == 1) {
            $user->addRole(SchoolService::ROLE_SCHOOL_ADMIN);
        } else {
            $user->removeRole(SchoolService::ROLE_SCHOOL_ADMIN);
        }

        $userManager->updateUser($user);

        return new JsonResponse($response);
    }

    /**
     * Creates a form to create a AdditionalUsers entity.
     *
     * @param AdditionalUsers $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AdditionalUsers $entity)
    {
        $form = $this->createForm(new AdditionalUsersType(), $entity, array(
            'action' => $this->generateUrl('school_config_additional-users_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new AdditionalUsers entity.
     *
     */
    public function newAction()
    {
        $entity = new AdditionalUsers();
        $form   = $this->createCreateForm($entity);

        return $this->render('ClassCoverSchoolBundle:AdditionalUsers:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a AdditionalUsers entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClassCoverSchoolBundle:AdditionalUsers')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AdditionalUsers entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClassCoverSchoolBundle:AdditionalUsers:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing AdditionalUsers entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClassCoverSchoolBundle:AdditionalUsers')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AdditionalUsers entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ClassCoverSchoolBundle:AdditionalUsers:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a AdditionalUsers entity.
    *
    * @param AdditionalUsers $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(AdditionalUsers $entity)
    {
        $form = $this->createForm(new AdditionalUsersType(), $entity, array(
            'action' => $this->generateUrl('school_config_additional-users_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing AdditionalUsers entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ClassCoverSchoolBundle:AdditionalUsers')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AdditionalUsers entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('school_config_additional-users_edit', array('id' => $id)));
        }

        return $this->render('ClassCoverSchoolBundle:AdditionalUsers:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a AdditionalUsers entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();


        if ($request->attributes->has('id')) {
            $id = $request->attributes->get('id');

            $entity = $em->getRepository('ClassCoverSchoolBundle:AdditionalUsers')->find($id);

            if ($entity !== null) {
                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('school_config_additional-users'));
    }

    /**
     * Creates a form to delete a AdditionalUsers entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('school_config_additional-users_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
