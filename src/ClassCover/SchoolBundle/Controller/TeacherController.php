<?php

namespace ClassCover\SchoolBundle\Controller;

use ClassCover\CyoBundle\Services\TeacherService;
use ClassCover\SchoolBundle\Form\TeacherType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use ClassCover\CyoBundle\Entity\TeacherRepository;
use ClassCover\SchoolBundle\Entity\School;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\SchoolBundle\Entity\SchoolRepository;
use ClassCover\SchoolBundle\Entity\RegisteredTeachersRepository;
use ClassCover\SchoolBundle\Entity\RegisteredTeachers;
use FOS\UserBundle\Doctrine\UserManager;
use ClassCover\AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

class TeacherController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN')")
     */
    public function indexAction()
    {
        $doctrine = $this->get('doctrine');

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $schoolId = $this->get('session')->get('school')->getId();

        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find($schoolId);

        /** @var ArrayCollection $registers */
        $registers = $school->getTeachers();

        return $this->render('ClassCoverSchoolBundle:Teacher:teachers.html.twig', ['registers' => $registers]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $doctrine = $this->get('doctrine');

        /** @var TeacherService $teacherService */
        $teacherService = $this->get('teacher_service');

        $schoolId = $this->get('session')->get('school')->getId();

        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find($schoolId);

        $entity = new Teacher();

        $form = $this->createForm(new TeacherType(), $entity, array(
            'action' => $this->generateUrl('class_cover_school_add_teacher'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        $form->handleRequest($request);

        if ($form->isValid()) {

            $teacher = $teacherService->createTeacher(
                $form->get('firstName')->getData(),
                $form->get('surname')->getData(),
                $form->get('email')->getData(),
                $form->get('phone')->getData()
            );

            $register = $teacherService->registerToSchool($teacher, $school);

            if ($register) {
                return $this->redirect($this->generateUrl('class_cover_school_teachers'));
            }
        }

        return $this->render('ClassCoverSchoolBundle:Teacher:add.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function setSchoolCasualPropertyAjaxAction(Request $request)
    {
        $response = [
          'status' => 'success', 'errors' => [], 'data' => []
        ];

        $constraints = new Collection([
            'property' => [
                new NotBlank(),
            ],
            'teacherId' => [
                new NotBlank(),
                new GreaterThan(0)
            ],
            'schoolId' => [
                new NotBlank(),
                new GreaterThan(0)
            ],
            'value' => [
                new NotBlank()
            ],
        ]);

        $data = $request->request->all();

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
            $response['status'] = 'error';

            return new JsonResponse($response, 200, ['Content-type', 'application/json']);
        }

        $doctrine = $this->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();
        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find($data['schoolId']);
        /** @var Teacher $teacher */
        $teacher = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->find($data['teacherId']);

        if ($teacher == null) {
            $response['errors'][] = 'Unable to find teacher entity for id ' . $data['teacherId'];
        }

        if ($school == null) {
            $response['errors'][] = 'Unable to find school entity for user id ' . $data['userId'];
        }

        if (count($response['errors'])) {
            $response['status'] = 'error';
            return new JsonResponse($response);
        }

        /** @var RegisteredTeachers $registration */
        $registration = $doctrine->getRepository('ClassCoverSchoolBundle:RegisteredTeachers')
            ->findOneBy(['teacher' => $teacher, 'school' => $school]);

        if ($registration == null) {
            $response['status'] = 'error';
            $response['errors'][] = 'Unable to find school registration entity for school and user with ids [' . $school->getId() . ',' . $teacher->getId() . ']';
            return new JsonResponse($response);
        }


        $accessor = PropertyAccess::createPropertyAccessor();

        $property = $data['property'];
        $propertyValue = $data['value'];

        if ($accessor->isWritable($registration, $property)) {
            $accessor->setValue($registration, $property, $propertyValue);
            $em->flush($registration);

            $response['data']['property'] = [
              'name'  => $property,
              'value' => $accessor->getValue($registration, $property)
            ];
            $response['data']['teacherId'] = $teacher->getId();

        } else {
            $response['status'] = 'error';
            $response['errors'][] = 'Unable to find property name ' . $property;
        }

        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function setSchoolCasualsOrderingAjaxAction(Request $request)
    {
        $response = [
            'status' => 'success', 'errors' => []
        ];

        $constraints = new Collection([
            'pairs' => [
                new NotBlank(),
            ],
            'schoolId' => [
                new NotBlank(),
                new GreaterThan(0)
            ]
        ]);

        $data = $request->request->all();

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
            $response['status'] = 'error';

            return new JsonResponse($response);
        }

        $doctrine = $this->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        /** @var TeacherRepository $teacherRepo */
        $teacherRepo = $doctrine->getRepository('ClassCoverCyoBundle:Teacher');

        /** @var RegisteredTeachersRepository $regTeachersRepo */
        $regTeachersRepo = $doctrine->getRepository('ClassCoverSchoolBundle:RegisteredTeachers');

        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find($data['schoolId']);

        foreach ($data['pairs'] as $pair) {

            /** @var Teacher $teacher */
            $teacher = $teacherRepo->find($pair['teacherId']);
            /** @var RegisteredTeachers $regTeacher */
            $regTeacher = $regTeachersRepo->findOneBy(['teacher' => $teacher, 'school' => $school]);

            if ($teacher == null || $regTeacher == null) {
                $response['errors'][] = 'Cannot find one or more entities for teacher id : ' . $pair['teacherId'];
                return new JsonResponse($response);
            }

            $regTeacher->setDisplayOrder($pair['order']);
            $em->persist($regTeacher);
            $em->flush();
        }


        return new JsonResponse($response);
    }

}