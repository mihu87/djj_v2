<?php
namespace ClassCover\SchoolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use FOS\UserBundle\Model\UserManager;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

class SettingsController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function indexAction(Request $request)
    {
        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        $formFactory = $this->get('fos_user.change_password.form.factory');

        /** @var Form $form */
        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $userManager->updateUser($user);
            return $this->redirect($this->generateUrl('class_cover_school_user_settings'));
        }

        return $this->render('ClassCoverSchoolBundle:Settings:password.html.twig', ['form' => $form->createView()]);
    }
}