<?php

namespace ClassCover\SchoolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ClassCover\BookingBundle\Services\TeacherAvailabilityService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ClassCover\BookingBundle\Entity\BookingRepository;
use ClassCover\SchoolBundle\Entity\School;
use ClassCover\BookingBundle\Entity\TeacherOvertimeRepository;
use ClassCover\BookingBundle\Entity\Booking;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThan;
use ClassCover\BookingBundle\Entity\SmsRequestList;
use ClassCover\BookingBundle\Entity\TeacherBooking;
use ClassCover\BookingBundle\Entity\TeacherBookingRepository;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\SchoolBundle\Entity\AdditionalUsersRepository;
use ClassCover\SchoolBundle\Entity\AdditionalUsers;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use ClassCover\BookingBundle\Services\SmsBookingService;
use ClassCover\AdminBundle\Entity\Announcement;

class DefaultController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function indexAction()
    {
        $beginPickerDate = (new \DateTime())->setTime(0,0,0);
        $endPickerDate   = (new \DateTime())->modify("+1 day")->setTime(0,0,0);
        $interval        = new \DateInterval('PT15M');
        $datePickerRange = new \DatePeriod($beginPickerDate, $interval ,$endPickerDate);

        $pickerDates = [];

        /** @var \DateTime $date */
        foreach($datePickerRange as $date){
            $pickerDates[] = $date->format('H:i');
        }

        return $this->render('ClassCoverSchoolBundle:Default:index.html.twig', ['pickerTimes' => $pickerDates]);
    }


    /**
     * @param $date
     * @param $schoolId
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function searchCentreTeachersAjaxAction($date, $schoolId)
    {
        /** @var TeacherAvailabilityService $teacherService */
        $availabilityService = $this->get('teacher_availability_service');
        $teachers = $availabilityService->getSchoolTeachersListWithAvailabilityByDate($schoolId, $date);
        return $this->render('ClassCoverSchoolBundle:Ajax:teachers-table.html.twig', ['teachers' => $teachers]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function smsBookingHistoryAjaxAction(Request $request)
    {
        $data = $request->request->all();

        $doctrine = $this->container->get('doctrine');
        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find($data['schoolId']);

        /** @var BookingRepository $repo */
        $repo = $doctrine->getRepository('ClassCoverBookingBundle:Booking');

        if (isset($data['dateFrom']) && isset($data['dateTo'])) {

            $dateFrom = new \DateTime($data['dateFrom']);
            $dateTo = new \DateTime($data['dateTo']);

            $dateFrom->setTime(0,0,0);
            $dateTo->setTime(59, 59, 59);

            $list = $repo->getSchoolBookingsOnInterval($dateFrom, $dateTo, $school);

        } else {
            $repo = $doctrine->getRepository('ClassCoverBookingBundle:Booking');
            $list = $repo->getAllSchoolBookings($school);
        }

        $bookingsList = [];

        /** @var AdditionalUsersRepository $additionalUsersRepo */
        $additionalUsersRepo = $doctrine->getRepository('ClassCoverSchoolBundle:AdditionalUsers');

        if (count($list)) {

            /** @var Booking $line */
            foreach ($list as $line) {

                /** @var AdditionalUsers $userCreated */
                $userCreated = $additionalUsersRepo->findOneBy([
                    'user' => $line->getUser(),
                    'school' => $school
                ]);

                if ($userCreated == null) {
                    $userCreated = $line->getSchool()->getName();
                } else {
                    $userCreated = $userCreated->getFirstname() . ' ' . $userCreated->getSurname();
                }

                $arrayInfo = [
                    'id' => $line->getId(),
                    'created' => $line->getCreatedOn()->format('d M, Y H:i'),
                    'school_requested' => $line->getSchool()->getName(),
                    'created_by' => $userCreated,
                    'status' => $line->getBookingStatus(),
                    'shift_date_start' => $line->getDateTimeStart()->format('d M, Y'),
                    'shift_date_end' => $line->getDateTimeEnd()->format('d M, Y'),
                    'shift_hour_start' => $line->getDateTimeStart()->format('H:i'),
                    'shift_hour_end' => $line->getDateTimeEnd()->format('H:i'),
                    'shift_type' => ucfirst($line->getShiftType()),
                    'teachers_requested' => $line->getTeachersRequested(),
                    'teachers_booked' => $line->getTeachersBooked(),
                    'sms_text' => $line->getSmsText(),
                    'keyword' => $line->getKeyword()->getKeywordPostfixNumber(),
                    'sms_list' => [],
                    'sms_list_excluded' => [],
                    'excluded_parsed' => 0,
                    'excluded_total' => 0,
                ];

                /** @var SmsRequestList $smsRequest */
                foreach ($line->getRequests() as $smsRequest) {

                    if ($smsRequest->getExcluded() > 0) {
                        $arrayInfo['sms_list_excluded'][] = $smsRequest;
                    } else {
                        $arrayInfo['sms_list'][] = $smsRequest;
                    }

                    if ($smsRequest->getSmsStatus() == "sent" && $smsRequest->getExcluded() > 0) {
                        $arrayInfo['excluded_parsed']++;
                    }

                    if ($smsRequest->getExcluded()>0) {
                        $arrayInfo['excluded_total']++;
                    }
                }

                $bookingsList[] = $arrayInfo;
            }
        }

        return $this->render('ClassCoverSchoolBundle:Ajax:sms-history-table.html.twig', ['bookings' => $bookingsList]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|static
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function cancelBookingAjaxAction(Request $request)
    {
        /** @var SmsBookingService $smsBookingService */
        $smsBookingService = $this->get('sms_booking_service');

        $response = [
            'status' => 'success', 'errors' => [], 'data' => []
        ];

        $constraints = new Collection([
            'bookingId' => [
                new NotBlank(),
                new GreaterThan(0)
            ]
        ]);

        $data = $request->request->all();

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);

        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
            $response['status'] = 'error';

            return JsonResponse::create($response);
        }

        $doctrine = $this->container->get('doctrine');
        /** @var Booking $booking */
        $booking = $doctrine->getRepository('ClassCoverBookingBundle:Booking')->find($data['bookingId']);

        if ($booking == null) {
            $response['status'] = 'error';
            $response['errors'][] = 'Cannot find entity for booking id ' . $data['bookingId'];

            return JsonResponse::create($response);
        }

        $smsBookingService->cancelBooking($booking);

        return JsonResponse::create($response);
    }

    /**
     * @param $bookingId
     * @param $teacherId
     * @return \Symfony\Component\HttpFoundation\Response|static
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function getBookingInfoAjaxAction($bookingId, $teacherId)
    {
        $em = $this->getDoctrine()->getManager();
        $doctrine = $this->container->get('doctrine');

        $response = [
            'status' => 'success', 'errors' => [], 'data' => []
        ];

        /** @var Booking $booking */
        $booking = $doctrine->getRepository('ClassCoverBookingBundle:Booking')->find($bookingId);
        /** @var Teacher $teacher */
        $teacher = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->find($teacherId);

        if ($booking == null) {
            $response['status'] = 'error';
            $response['errors'][] = 'Cannot find booking entity for id ' . $bookingId;
        }

        if ($teacher == null) {
            $response['status'] = 'error';
            $response['errors'][] = 'Cannot find teacher entity for id ' . $teacherId;
        }

        if (count($response['errors'])) {
            return JsonResponse::create($response);
        }

        $response['data'] = [
            'timeStart' => $booking->getDateTimeStart()->format("H:i"),
            'timeEnd' => $booking->getDateTimeEnd()->format("H:i"),
            'dateStart' => $booking->getDateTimeStart()->format("Y-m-d"),
            'dateEnd' => $booking->getDateTimeEnd()->format("Y-m-d"),
            'shiftType' => $booking->getShiftType(),
            'teacherId' => $teacher->getId(),
            'teacherName' => $teacher->getFirstName() . ' ' . $teacher->getSurname(),
            'bookingId' => $booking->getId()
        ];

        return JsonResponse::create($response);
    }


    public function getAnnouncementsAction()
    {
        $doctrine = $this->container->get('doctrine');

        $announcements = $doctrine->getRepository('ClassCoverAdminBundle:Announcement')->findBy(['type' => ['school', 'all']]);

        $response = ['data' => false];
        /** @var Announcement $last */
        $last = false;
        if (count($announcements)) {
            $last = end($announcements);

            $now = new \DateTime();
            $now = $now->getTimestamp();

            if ($now >= $last->getDateTimeStart()->getTimestamp() && $now <= $last->getDateTimeEnd()->getTimestamp()) {
                $response['data']['heading'] = $last->getHeadLine();
                $response['data']['content'] = $last->getSmsText();
            }
        }

        return JsonResponse::create($response);
    }
}
