<?php

namespace ClassCover\SchoolBundle\Controller;

use ClassCover\BookingBundle\Entity\BookingRepository;
use ClassCover\BookingBundle\Entity\SmsRequestList;
use ClassCover\BookingBundle\Entity\SmsRequestListRepository;
use ClassCover\BookingBundle\Entity\TeacherBookingRepository;
use ClassCover\BookingBundle\Services\SmsBookingService;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use ClassCover\SchoolBundle\Entity\School;
use ClassCover\BookingBundle\Entity\TeacherBooking;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use Symfony\Component\HttpFoundation\Response;
use ClassCover\CyoBundle\Entity\Teacher;
use ClassCover\SchoolBundle\Entity\SchoolRepository;
use ClassCover\BookingBundle\Entity\Booking;

class CalendarController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function indexAction()
    {
        $doctrine = $this->container->get('doctrine');

        $beginPickerDate = (new \DateTime())->setTime(0,0,0);
        $endPickerDate   = (new \DateTime())->modify("+1 day")->setTime(0,0,0);
        $interval        = new \DateInterval('PT15M');
        $datePickerRange = new \DatePeriod($beginPickerDate, $interval ,$endPickerDate);

        $pickerDates = [];

        /** @var \DateTime $date */
        foreach($datePickerRange as $date){
            $pickerDates[] = $date->format('H:i');
        }

        $user = $this->getUser();
        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->findOneBy(['user' => $user]);

        $casualsList = [];
        if ($school !== null && count($school->getTeachers()) > 0) {
           $casualsList = $school->getTeachers();
        }

        return $this->render('ClassCoverSchoolBundle:Calendar:calendar.html.twig', ['pickerTimes' => $pickerDates, 'casualList' => $casualsList]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @PreAuthorize("hasRole('ROLE_SCHOOL') or hasRole('ROLE_SCHOOL_ADMIN') or hasRole('ROLE_SCHOOL_ADDITIONAL_USER')")
     */
    public function getEventsAjaxAction(Request $request)
    {
        $doctrine = $this->container->get('doctrine');
        $data = $request->request->all();

        $response = [
            'status' => 'success', 'errors' => [], 'data' => []
        ];

        $constraints = new Collection([
            'start' => [
                new NotBlank()
            ],
            'end' => [
                new NotBlank()
            ],
            'schoolId' => [
                new NotBlank(),
                new GreaterThan(0)
            ]
        ]);

        $data = $request->request->all();

        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
        $violations = $this->get('validator')->validateValue($data, $constraints);
        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                $response['errors'][] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
            }
            $response['status'] = 'error';

            return new JsonResponse($response, 200, ['Content-type', 'application/json']);
        }

        $dateFrom = new \DateTime($data['start']);
        $dateTo = new \DateTime($data['end']);

        /** @var School $school */
        $school = $doctrine->getRepository('ClassCoverSchoolBundle:School')->find($data['schoolId']);

        /** @var ArrayCollection $repo */
        $bookingsList = $doctrine->getRepository('ClassCoverBookingBundle:TeacherBooking')
            ->getBySchoolAndInterval($school, $dateFrom, $dateTo);

        $events = [];

        /** @var TeacherBooking $teacherBooking */
        foreach ($bookingsList as $teacherBooking)
        {
            $bookingText = $teacherBooking->getTeacher()->getFirstName() . ' ' .
                $teacherBooking->getTeacher()->getSurname() . ', ' .
                ucfirst($teacherBooking->getBooking()->getShiftType()) . ' shift ';

            $events[] = [
                'id' => $teacherBooking->getBooking()->getId(),
                'bookingId' => $teacherBooking->getBooking()->getId(),
                'teacherId' => $teacherBooking->getTeacher()->getId(),
                'title' => $bookingText,
                'start' => $teacherBooking->getBooking()->getDateTimeStart()->format(\DateTime::ISO8601),
                'end' => $teacherBooking->getBooking()->getDateTimeEnd()->format(\DateTime::ISO8601),
                'type' => 'booking'
            ];
        }

        return new JsonResponse($events, 200);
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param int $casualId
     * @return Response
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function casualReportAction($dateFrom, $dateTo, $casualId = 0) {

        $doctrine = $this->container->get('doctrine');
        $user = $this->getUser();

        /** @var SchoolRepository $schoolRepo */
        $schoolRepo = $doctrine->getRepository('ClassCoverSchoolBundle:School');
        /** @var SmsRequestListRepository $smsRequestsRepo */
        $smsRequestsRepo = $doctrine->getRepository('ClassCoverBookingBundle:SmsRequestList');
        /** @var BookingRepository $bookingRepository */
        $bookingRepository = $doctrine->getRepository('ClassCoverBookingBundle:Booking');
        /** @var TeacherBookingRepository $bookingRepository */
        $teacherBookingRepository = $doctrine->getRepository('ClassCoverBookingBundle:TeacherBooking');

        if ($this->get('session')->has('school')) {
            /** @var School $school */
            $school = $this->get('session')->get('school');
        } else {
            throw new AccessDeniedException('You should be logged in as school or school user.');
        }

        $dateFrom = new \DateTime($dateFrom);
        $dateTo = new \DateTime($dateTo);

        $casuals = [];

        if ($casualId > 0) {
            /** @var Teacher $casual */
            $casual = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->find($casualId);
            if (!empty($casual)) {
                $casuals[] = $casual;
            }
        } else {
            $casuals = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->findAll();
        }

        $data = [];

        if (count($casuals)) {
            /** @var Teacher $casual */
            foreach ($casuals as $casual) {

                $casualData = [
                    'entries' => [],
                    'totals' => [
                        'offered_special' => 0,
                        'offered_morning' => 0,
                        'offered_evening' => 0,
                        'offered_night'   => 0,
                        'accepted_special'=> 0,
                        'accepted_morning'=> 0,
                        'accepted_evening'=> 0,
                        'accepted_night'  => 0,
                        'declined_special'=> 0,
                        'declined_morning'=> 0,
                        'declined_evening'=> 0,
                        'declined_night'  => 0
                    ]
                ];

               $smsRequests = $smsRequestsRepo->findBy([
                   'teacher'   => $casual,
                   'smsStatus' => "sent",
                   'school'    => $school
               ]);


                $tbs = $teacherBookingRepository->getBookingIdsOnInterval($dateFrom, $dateTo, $casual);

                $manualBookings = [];

                foreach ($tbs as $tb) {
                    /** @var Booking $booking */
                    $booking = $bookingRepository->find($tb['booking_id']);
                    if ($booking->getBookingType() == Booking::BOOKING_TYPE_MANUAL) {
                        $manualBookings[] = $booking;
                    }
                }

                if (count($smsRequests)) {
                    /** @var SmsRequestList $smsRequest */
                    foreach ($smsRequests as $smsRequest) {

                        try {
                            $booking = clone $smsRequest->getBooking();
                        } catch (EntityNotFoundException $ex) {
                            $booking = null;
                        }

                        if (empty($booking)) {
                            continue;
                        }

                        if ($smsRequest->getSmsStatus() != "sent") {
                            continue;
                        }

                        $aDates = [];
                        $dateCursor = clone $dateFrom;
                        while ($dateCursor <= $dateTo) {
                            $aDates[] = $dateCursor->format('Y-m-d');
                            $dateCursor->modify("+1 day");
                        }

                        $bookingDateStartStr = $booking->getDateTimeStart()->format("Y-m-d");

                        if (!in_array($bookingDateStartStr, $aDates)) {
                            continue;
                        }

                        $requestDate = $booking->getCreatedOn()->format('d/m/Y');

                        $shiftDates = '';

                        /** @var \DateTime $bookingTimeStart */
                        $bookingTimeStart = $booking->getDateTimeStart();
                        /** @var \DateTime $bookingTimeEnd */
                        $bookingTimeEnd = $booking->getDateTimeEnd();

                        if ($bookingTimeStart->format("dmY") == $bookingTimeEnd->format("dmY")) {
                            $shiftDates = $bookingTimeStart->format("d/m");
                        } else {
                            $shiftDates = $bookingTimeStart->format("d/m");
                            $shiftDates.= "-" . $bookingTimeEnd->format("d/m");
                        }

                        $bookingTime = $bookingTimeStart->format("H:i").'-'.$bookingTimeEnd->format("H:i");

                        $item = [
                            'date' => $requestDate . ": " . $shiftDates,
                            'name' => $casual->getFirstName() . ' ' . $casual->getSurname(),
                            'list' => 'Merit',
                            'centre' => $school->getName(),
                            'offered_special' => '',
                            'offered_morning' => '',
                            'offered_evening' => '',
                            'offered_night' => '',
                            'offered_total' => '',
                            'accepted_special' => '',
                            'accepted_morning' => '',
                            'accepted_evening' => '',
                            'accepted_night' => '',
                            'accepted_total' => '',
                            'declined_special' => '',
                            'declined_morning' => '',
                            'declined_evening' => '',
                            'declined_night' => '',
                            'declined_total' => '',
                            'booking_type' => $booking->getBookingType()
                        ];

                        $shiftType = $booking->getShiftType();

                        switch ($shiftType) {

                            case "morning":
                                $item['offered_morning'] = $bookingTime;
                                $casualData['totals']['offered_morning']++;
                                break;
                            case "evening":
                                $item['offered_evening'] = $bookingTime;
                                $casualData['totals']['offered_evening']++;
                                break;
                            case "night":
                                $item['offered_night'] = $bookingTime;
                                $casualData['totals']['offered_night']++;
                                break;
                            default:
                                $item['offered_special'] = $bookingTime;
                                $casualData['totals']['offered_special']++;
                                break;
                        }

                        if($smsRequest->getResponse() == "accepted") {

                            $key = 'accepted_' . $shiftType;

                            $item['accepted_' . $shiftType] = $bookingTime;
                            $casualData['totals'][$key]++;

                        } else {

                            $key = 'declined_' . $shiftType;

                            $item['declined_' . $shiftType] = $bookingTime;
                            $casualData['totals'][$key]++;

                        }

                        $casualData['entries'][] = $item;
                    }
                }

                //manual
                if(count($manualBookings)) {
                    /** @var Booking $booking */
                    foreach ($manualBookings as $booking) {
                        
                        $requestDate = $booking->getCreatedOn()->format('d/m/Y');

                        $shiftDates = '';

                        /** @var \DateTime $bookingTimeStart */
                        $bookingTimeStart = $booking->getDateTimeStart();
                        /** @var \DateTime $bookingTimeEnd */
                        $bookingTimeEnd = $booking->getDateTimeEnd();

                        if ($bookingTimeStart->format("dmY") == $bookingTimeEnd->format("dmY")) {
                            $shiftDates = $bookingTimeStart->format("d/m");
                        } else {
                            $shiftDates = $bookingTimeStart->format("d/m");
                            $shiftDates.= "-" . $bookingTimeEnd->format("d/m");
                        }

                        $bookingTime = $bookingTimeStart->format("H:i").'-'.$bookingTimeEnd->format("H:i");

                        $item = [
                            'date' => $requestDate . ": " . $shiftDates,
                            'name' => $casual->getFirstName() . ' ' . $casual->getSurname(),
                            'list' => 'Merit',
                            'centre' => $school->getName(),
                            'offered_special' => '',
                            'offered_morning' => '',
                            'offered_evening' => '',
                            'offered_night' => '',
                            'offered_total' => '',
                            'accepted_special' => '',
                            'accepted_morning' => '',
                            'accepted_evening' => '',
                            'accepted_night' => '',
                            'accepted_total' => '',
                            'declined_special' => '',
                            'declined_morning' => '',
                            'declined_evening' => '',
                            'declined_night' => '',
                            'declined_total' => '',
                            'booking_type' => $booking->getBookingType()
                        ];

                        $shiftType = $booking->getShiftType();

                        switch ($shiftType) {

                            case "morning":
                                $item['offered_morning'] = $bookingTime;
                                $item['accepted_morning'] = $bookingTime;
                                $casualData['totals']['offered_morning']++;
                                $casualData['totals']['accepted_morning']++;
                                break;
                            case "evening":
                                $item['offered_evening'] = $bookingTime;
                                $item['accepted_evening'] = $bookingTime;
                                $casualData['totals']['offered_evening']++;
                                $casualData['totals']['accepted_evening']++;
                                break;
                            case "night":
                                $item['offered_night'] = $bookingTime;
                                $item['accepted_night'] = $bookingTime;
                                $casualData['totals']['offered_night']++;
                                $casualData['totals']['accepted_night']++;
                                break;
                            default:
                                $item['offered_special'] = $bookingTime;
                                $item['accepted_special'] = $bookingTime;
                                $casualData['totals']['offered_special']++;
                                $casualData['totals']['accepted_special']++;
                                break;
                        }

                        $casualData['entries'][] = $item;

                    }
                }

                if (count($casualData['entries']) > 0) {
                   $data[] = $casualData;
                }
            }
        }

        // Build excel using data
        if (count($data) > 0) {

            $objPHPExcel = new \PHPExcel();
            $sheet = $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getDefaultStyle()->getFont()->setSize(11);
            $sheet->setTitle('CYO Report');

            $heading_style = array(
                'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER, 'wrap' => true, 'shrinkToFit' => true)
            );
            $total_style = array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                        'argb' => 'FFA6A6A6'
                    )
                ),
                'font' => array('bold' => true)
            );
            $total_value_style = array(
                'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER, 'wrap' => true, 'shrinkToFit' => true)
            );

            $sheet->getColumnDimension('A')->setAutoSize(true);
            $sheet->getColumnDimension('B')->setAutoSize(true);
            $sheet->getColumnDimension('C')->setAutoSize(true);
            $sheet->getColumnDimension('D')->setAutoSize(true);
            $sheet->getColumnDimension('E')->setAutoSize(true);
            $sheet->getColumnDimension('F')->setAutoSize(true);
            $sheet->getColumnDimension('G')->setAutoSize(true);
            $sheet->getColumnDimension('H')->setAutoSize(true);
            $sheet->getColumnDimension('I')->setAutoSize(true);
            $sheet->getColumnDimension('J')->setAutoSize(true);
            $sheet->getColumnDimension('K')->setAutoSize(true);
            $sheet->getColumnDimension('L')->setAutoSize(true);
            $sheet->getColumnDimension('M')->setAutoSize(true);
            $sheet->getColumnDimension('N')->setAutoSize(true);
            $sheet->getColumnDimension('O')->setAutoSize(true);
            $sheet->getColumnDimension('P')->setAutoSize(true);
            $sheet->getColumnDimension('Q')->setAutoSize(true);
            $sheet->getColumnDimension('R')->setAutoSize(true);
            $sheet->getColumnDimension('S')->setAutoSize(true);
            $sheet->getColumnDimension('T')->setAutoSize(true);

            $sheet->mergeCells("E1:S1");
            $sheet->setCellValue("E1", "Shifts");
            $sheet->mergeCells("E2:I2");
            $sheet->setCellValue("E2", "Offered");
            $sheet->mergeCells("J2:N2");
            $sheet->setCellValue("J2", "Accepted");
            $sheet->mergeCells("O2:S2");
            $sheet->setCellValue("O2", "Declined or No response");
            $sheet->setCellValue("A3", "Created on: Shift Date(s)");
            $sheet->setCellValue("B3", "Youth Officer Name");
            $sheet->setCellValue("C3", "List");
            $sheet->setCellValue("D3", "Centre");
            $sheet->setCellValue("E3", "Special");
            $sheet->setCellValue("F3", "Morning");
            $sheet->setCellValue("G3", "Evening");
            $sheet->setCellValue("H3", "Night");
            $sheet->setCellValue("I3", "Total");
            $sheet->setCellValue("J3", "Special");
            $sheet->setCellValue("K3", "Morning");
            $sheet->setCellValue("L3", "Evening");
            $sheet->setCellValue("M3", "Night");
            $sheet->setCellValue("N3", "Total");
            $sheet->setCellValue("O3", "Special");
            $sheet->setCellValue("P3", "Morning");
            $sheet->setCellValue("Q3", "Evening");
            $sheet->setCellValue("R3", "Night");
            $sheet->setCellValue("S3", "Total");
            $sheet->setCellValue("T3", "Booking Type");

            $sheet->getStyle("E2")->applyFromArray($this->colorCell('ffb84d'));
            $sheet->getStyle("J2")->applyFromArray($this->colorCell('48d148'));
            $sheet->getStyle("O2")->applyFromArray($this->colorCell('ff8080'));

            $sheet->getStyle("A1:T3")->applyFromArray($heading_style);
            $row_index = 4;

            foreach ($data as $cyo) {

                foreach ($cyo['entries'] as $row) {

                    $sheet->setCellValue("A".$row_index, $row['date']);
                    $sheet->setCellValue("B".$row_index, $row['name']);
                    $sheet->setCellValue("C".$row_index, $row['list']);
                    $sheet->setCellValue("D".$row_index, $row['centre']);
                    $sheet->setCellValue("E".$row_index, $row['offered_special']);
                    $sheet->setCellValue("F".$row_index, $row['offered_morning']);
                    $sheet->setCellValue("G".$row_index, $row['offered_evening']);
                    $sheet->setCellValue("H".$row_index, $row['offered_night']);
                    $sheet->setCellValue("I".$row_index, $row['offered_total']);
                    $sheet->setCellValue("J".$row_index, $row['accepted_special']);
                    $sheet->setCellValue("K".$row_index, $row['accepted_morning']);
                    $sheet->setCellValue("L".$row_index, $row['accepted_evening']);
                    $sheet->setCellValue("M".$row_index, $row['accepted_night']);
                    $sheet->setCellValue("N".$row_index, $row['accepted_total']);
                    $sheet->setCellValue("O".$row_index, $row['declined_special']);
                    $sheet->setCellValue("P".$row_index, $row['declined_morning']);
                    $sheet->setCellValue("Q".$row_index, $row['declined_evening']);
                    $sheet->setCellValue("R".$row_index, $row['declined_night']);
                    $sheet->setCellValue("S".$row_index, $row['declined_total']);
                    $sheet->setCellValue("T".$row_index, ucfirst($row['booking_type']));
                    $sheet->getStyle("E".$row_index.":T".$row_index)->applyFromArray($total_value_style);

                    // Offered Line
                    $sheet->getStyle("E".$row_index)->applyFromArray($this->colorCell('fff0e5'));
                    $sheet->getStyle("F".$row_index)->applyFromArray($this->colorCell('ffffe5'));
                    $sheet->getStyle("G".$row_index)->applyFromArray($this->colorCell('ffe5b3'));
                    $sheet->getStyle("H".$row_index)->applyFromArray($this->colorCell('ccddff'));
                    $sheet->getStyle("I".$row_index)->applyFromArray($this->colorCell('f2f2f2'));

                    // Accepted Line
                    $sheet->getStyle("J".$row_index)->applyFromArray($this->colorCell('fff0e5'));
                    $sheet->getStyle("K".$row_index)->applyFromArray($this->colorCell('ffffe5'));
                    $sheet->getStyle("L".$row_index)->applyFromArray($this->colorCell('ffe5b3'));
                    $sheet->getStyle("M".$row_index)->applyFromArray($this->colorCell('ccddff'));
                    $sheet->getStyle("N".$row_index)->applyFromArray($this->colorCell('f2f2f2'));

                    // Declined Line
                    $sheet->getStyle("O".$row_index)->applyFromArray($this->colorCell('fff0e5'));
                    $sheet->getStyle("P".$row_index)->applyFromArray($this->colorCell('ffffe5'));
                    $sheet->getStyle("Q".$row_index)->applyFromArray($this->colorCell('ffe5b3'));
                    $sheet->getStyle("R".$row_index)->applyFromArray($this->colorCell('ccddff'));
                    $sheet->getStyle("S".$row_index)->applyFromArray($this->colorCell('f2f2f2'));

                    $row_index++;
                }

                $sheet->setCellValue("A".$row_index, "Total");

                $offered_total = $cyo['totals']['offered_special'] + $cyo['totals']['offered_morning'] + $cyo['totals']['offered_evening'] + $cyo['totals']['offered_night'];
                //@$percent = number_format((100.0*$cyo['totals']['offered_special'])/$offered_total, 0);
                $total_value = $cyo['totals']['offered_special'];
                //$total_value .= $cyo['totals']['offered_special'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("E".$row_index, $total_value);
                //@$percent = number_format((100.0*$cyo['totals']['offered_morning'])/$offered_total, 0);
                $total_value = $cyo['totals']['offered_morning'];
                //$total_value .= $cyo['totals']['offered_morning'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("F".$row_index, $total_value);
                //@$percent = number_format((100.0*$cyo['totals']['offered_evening'])/$offered_total, 0);
                $total_value = $cyo['totals']['offered_evening'];
                //$total_value .= $cyo['totals']['offered_evening'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("G".$row_index, $total_value);
                //@$percent = number_format((100.0*$cyo['totals']['offered_night'])/$offered_total, 0);
                $total_value = $cyo['totals']['offered_night'];
                //$total_value .= $cyo['totals']['offered_night'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("H".$row_index, $total_value);
                $sheet->setCellValue("I".$row_index, $offered_total);

                $accepted_total = $cyo['totals']['accepted_special'] + $cyo['totals']['accepted_morning'] + $cyo['totals']['accepted_evening'] + $cyo['totals']['accepted_night'];
                //@$percent = number_format((100.0*$cyo['totals']['accepted_special'])/$accepted_total, 0);
                $total_value = $cyo['totals']['accepted_special'];
                //$total_value .= $cyo['totals']['accepted_special'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("J".$row_index, $total_value);
                //@$percent = number_format((100.0*$cyo['totals']['accepted_morning'])/$accepted_total, 0);
                $total_value = $cyo['totals']['accepted_morning'];
                //$total_value .= $cyo['totals']['accepted_morning'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("K".$row_index, $total_value);
                //@$percent = number_format((100.0*$cyo['totals']['accepted_evening'])/$accepted_total, 0);
                $total_value = $cyo['totals']['accepted_evening'];
                //$total_value .= $cyo['totals']['accepted_evening'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("L".$row_index, $total_value);
                //@$percent = number_format((100.0*$cyo['totals']['accepted_night'])/$accepted_total, 0);
                $total_value = $cyo['totals']['accepted_night'];
                //$total_value .= $cyo['totals']['accepted_night'] > 0 ? ' ('.$percent.'%)': '';
                $sheet->setCellValue("M".$row_index, $total_value);
                $sheet->setCellValue("N".$row_index, $accepted_total);

                $declined_total = $cyo['totals']['declined_special'] + $cyo['totals']['declined_morning'] + $cyo['totals']['declined_evening'] + $cyo['totals']['declined_night'];
                //@$percent = number_format((100.0*$cyo['totals']['declined_special'])/$declined_total, 0);
                $total_value = $cyo['totals']['declined_special'];
                //$total_value .= $cyo['totals']['declined_special'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("O".$row_index, $total_value);
                //@$percent = number_format((100.0*$cyo['totals']['declined_morning'])/$declined_total, 0);
                $total_value = $cyo['totals']['declined_morning'];
                //$total_value .= $cyo['totals']['declined_morning'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("P".$row_index, $total_value);
                //@$percent = number_format((100.0*$cyo['totals']['declined_evening'])/$declined_total, 0);
                $total_value = $cyo['totals']['declined_evening'];
                //$total_value .= $cyo['totals']['declined_evening'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("Q".$row_index, $total_value);
                //@$percent = number_format((100.0*$cyo['totals']['declined_night'])/$declined_total, 0);
                $total_value = $cyo['totals']['declined_night'];
                //$total_value .= $cyo['totals']['declined_night'] > 0 ? ' ('.$percent.')': '';
                $sheet->setCellValue("R".$row_index, $total_value);
                $sheet->setCellValue("S".$row_index, $declined_total);

                $sheet->getStyle("A".$row_index.":T".$row_index)->applyFromArray($total_style);
                $sheet->getStyle("B".$row_index.":T".$row_index)->applyFromArray(array_merge($total_style, $total_value_style));

                $row_index++;
            }

            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            $objWriter->save('php://output');
            $excel_stream = ob_get_clean();
            ob_end_clean();

            $response = new Response($excel_stream);

            $response->headers->set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            $response->headers->set('Content-Description', 'File Transfer');
            $response->headers->set('Content-Transfer-Encoding', 'binary');
            $response->headers->set("Expires", "0");
            $response->headers->set('Pragma', 'public');
            $response->headers->set("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            $response->headers->set("Content-Type", "application/force-download");
            $response->headers->set("Content-Type", "application/octet-stream");
            $response->headers->set("Content-Type", "application/download");
            $response->headers->set("Content-Disposition", "attachment;filename=cyo_report.xlsx");

            return $response;
        }

    }

    /**
     * @param $color
     * @return array
     */
    protected function colorCell($color)
    {
        return [
            'fill' => [
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => strtoupper($color)]
            ]
        ];
    }


}