<?php

namespace ClassCover\SchoolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RegisteredTeachers
 *
 * @ORM\Table(name="school_registered_teachers")
 * @ORM\Entity(repositoryClass="ClassCover\SchoolBundle\Entity\RegisteredTeachersRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RegisteredTeachers
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\SchoolBundle\Entity\School", inversedBy="schoolTeachers")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
     */
    protected $school;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\CyoBundle\Entity\Teacher", inversedBy="schoolTeachers")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id", nullable=false)
     */
    protected $teacher;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $rating;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $displayOrder;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $regular;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $favorite;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $suspended;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $removed;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return RegisteredTeachers
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set displayOrder
     *
     * @param integer $displayOrder
     *
     * @return RegisteredTeachers
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;

        return $this;
    }

    /**
     * Get displayOrder
     *
     * @return integer
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * Set regular
     *
     * @param integer $regular
     *
     * @return RegisteredTeachers
     */
    public function setRegular($regular)
    {
        $this->regular = $regular;

        return $this;
    }

    /**
     * Get regular
     *
     * @return integer
     */
    public function getRegular()
    {
        return $this->regular;
    }

    /**
     * Set favorite
     *
     * @param integer $favorite
     *
     * @return RegisteredTeachers
     */
    public function setFavorite($favorite)
    {
        $this->favorite = $favorite;

        return $this;
    }

    /**
     * Get favorite
     *
     * @return integer
     */
    public function getFavorite()
    {
        return $this->favorite;
    }

    /**
     * Set suspended
     *
     * @param integer $suspended
     *
     * @return RegisteredTeachers
     */
    public function setSuspended($suspended)
    {
        $this->suspended = $suspended;

        return $this;
    }

    /**
     * Get suspended
     *
     * @return integer
     */
    public function getSuspended()
    {
        return $this->suspended;
    }

    /**
     * Set removed
     *
     * @param integer $removed
     *
     * @return RegisteredTeachers
     */
    public function setRemoved($removed)
    {
        $this->removed = $removed;

        return $this;
    }

    /**
     * Get removed
     *
     * @return integer
     */
    public function getRemoved()
    {
        return $this->removed;
    }

    /**
     * Set school
     *
     * @param \ClassCover\SchoolBundle\Entity\School $school
     *
     * @return RegisteredTeachers
     */
    public function setSchool(\ClassCover\SchoolBundle\Entity\School $school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \ClassCover\SchoolBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set teacher
     *
     * @param \ClassCover\CyoBundle\Entity\Teacher $teacher
     *
     * @return RegisteredTeachers
     */
    public function setTeacher(\ClassCover\CyoBundle\Entity\Teacher $teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \ClassCover\CyoBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDefaultValueIfNotSpecified()
    {
        if (empty($this->rating)) {
            $this->rating = 0;
        }

        if (empty($this->displayOrder)) {
            $this->displayOrder = 0;
        }

        if (empty($this->regular)) {
            $this->regular = 0;
        }

        if (empty($this->favorite)) {
            $this->favorite = 0;
        }

        if (empty($this->suspended)) {
            $this->suspended = 0;
        }

        if (empty($this->removed)) {
            $this->removed = 0;
        }

        return $this;
    }
}
