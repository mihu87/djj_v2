<?php

namespace ClassCover\SchoolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * School
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\SchoolBundle\Entity\SchoolRepository")
 */
class School
{

    const SCHOOL_ACCESS_TYPE_SINGLE = 0;
    const SCHOOL_ACCESS_TYPE_MULTIPLE = 0;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClassCover\AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    protected $shortName;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $additionalEmail;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressStreet;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressCity;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressState;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressCountry;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $addressPostCode;

    /**
     * @ORM\Column(type="float", nullable=true, options={ "default"=0 })
     */
    protected $latitude;

    /**
     * @ORM\Column(type="float", nullable=true, options={ "default"=0 })
     */
    protected $longitude;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $website;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $studentNumber;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $principal;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $casualTeacherAdmin;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $casualTeacherAdminPhone;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $casualTeacherAdminEmail;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={ "default"=0 })
     */
    protected $accessType;

    /**
     * @ORM\Column(type="string", nullable=true, options={ "default"=NULL })
     */
    protected $howDidYouHearText;

    /**
     * @ORM\Column(type="integer", nullable=true, options={ "default"="0" })
     */
    protected $timezoneId;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={ "default"=NULL })
     */
    protected $createdOn;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\SchoolBundle\Entity\RegisteredTeachers", mappedBy="school")
     * @ORM\OrderBy({"displayOrder" = "ASC"})
     **/
    protected $teachers;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\TeacherBooking", mappedBy="school")
     */
    protected $bookings;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\TeacherExclusion", mappedBy="school")
     */
    protected $exclusions;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\TeacherShiftOffer", mappedBy="school")
     */
    protected $shiftOffers;

    /**
     * @ORM\OneToMany(targetEntity="ClassCover\BookingBundle\Entity\TeacherUnavailability", mappedBy="school")
     */
    protected $unavailableTeachers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teachers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bookings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->exclusions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->shiftOffers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->unavailableTeachers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return School
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     *
     * @return School
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return School
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return School
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set additionalEmail
     *
     * @param string $additionalEmail
     *
     * @return School
     */
    public function setAdditionalEmail($additionalEmail)
    {
        $this->additionalEmail = $additionalEmail;

        return $this;
    }

    /**
     * Get additionalEmail
     *
     * @return string
     */
    public function getAdditionalEmail()
    {
        return $this->additionalEmail;
    }

    /**
     * Set addressStreet
     *
     * @param string $addressStreet
     *
     * @return School
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;

        return $this;
    }

    /**
     * Get addressStreet
     *
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     *
     * @return School
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     *
     * @return School
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;

        return $this;
    }

    /**
     * Get addressState
     *
     * @return string
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * Set addressCountry
     *
     * @param string $addressCountry
     *
     * @return School
     */
    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;

        return $this;
    }

    /**
     * Get addressCountry
     *
     * @return string
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    /**
     * Set addressPostCode
     *
     * @param string $addressPostCode
     *
     * @return School
     */
    public function setAddressPostCode($addressPostCode)
    {
        $this->addressPostCode = $addressPostCode;

        return $this;
    }

    /**
     * Get addressPostCode
     *
     * @return string
     */
    public function getAddressPostCode()
    {
        return $this->addressPostCode;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return School
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return School
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return School
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set studentNumber
     *
     * @param string $studentNumber
     *
     * @return School
     */
    public function setStudentNumber($studentNumber)
    {
        $this->studentNumber = $studentNumber;

        return $this;
    }

    /**
     * Get studentNumber
     *
     * @return string
     */
    public function getStudentNumber()
    {
        return $this->studentNumber;
    }

    /**
     * Set principal
     *
     * @param string $principal
     *
     * @return School
     */
    public function setPrincipal($principal)
    {
        $this->principal = $principal;

        return $this;
    }

    /**
     * Get principal
     *
     * @return string
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Set casualTeacherAdmin
     *
     * @param string $casualTeacherAdmin
     *
     * @return School
     */
    public function setCasualTeacherAdmin($casualTeacherAdmin)
    {
        $this->casualTeacherAdmin = $casualTeacherAdmin;

        return $this;
    }

    /**
     * Get casualTeacherAdmin
     *
     * @return string
     */
    public function getCasualTeacherAdmin()
    {
        return $this->casualTeacherAdmin;
    }

    /**
     * Set casualTeacherAdminPhone
     *
     * @param string $casualTeacherAdminPhone
     *
     * @return School
     */
    public function setCasualTeacherAdminPhone($casualTeacherAdminPhone)
    {
        $this->casualTeacherAdminPhone = $casualTeacherAdminPhone;

        return $this;
    }

    /**
     * Get casualTeacherAdminPhone
     *
     * @return string
     */
    public function getCasualTeacherAdminPhone()
    {
        return $this->casualTeacherAdminPhone;
    }

    /**
     * Set casualTeacherAdminEmail
     *
     * @param string $casualTeacherAdminEmail
     *
     * @return School
     */
    public function setCasualTeacherAdminEmail($casualTeacherAdminEmail)
    {
        $this->casualTeacherAdminEmail = $casualTeacherAdminEmail;

        return $this;
    }

    /**
     * Get casualTeacherAdminEmail
     *
     * @return string
     */
    public function getCasualTeacherAdminEmail()
    {
        return $this->casualTeacherAdminEmail;
    }

    /**
     * Set accessType
     *
     * @param integer $accessType
     *
     * @return School
     */
    public function setAccessType($accessType)
    {
        $this->accessType = $accessType;

        return $this;
    }

    /**
     * Get accessType
     *
     * @return integer
     */
    public function getAccessType()
    {
        return $this->accessType;
    }

    /**
     * Set howDidYouHearText
     *
     * @param string $howDidYouHearText
     *
     * @return School
     */
    public function setHowDidYouHearText($howDidYouHearText)
    {
        $this->howDidYouHearText = $howDidYouHearText;

        return $this;
    }

    /**
     * Get howDidYouHearText
     *
     * @return string
     */
    public function getHowDidYouHearText()
    {
        return $this->howDidYouHearText;
    }

    /**
     * Set timezoneId
     *
     * @param integer $timezoneId
     *
     * @return School
     */
    public function setTimezoneId($timezoneId)
    {
        $this->timezoneId = $timezoneId;

        return $this;
    }

    /**
     * Get timezoneId
     *
     * @return integer
     */
    public function getTimezoneId()
    {
        return $this->timezoneId;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     *
     * @return School
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set user
     *
     * @param \ClassCover\AppBundle\Entity\User $user
     *
     * @return School
     */
    public function setUser(\ClassCover\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ClassCover\AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add teacher
     *
     * @param \ClassCover\SchoolBundle\Entity\RegisteredTeachers $teacher
     *
     * @return School
     */
    public function addTeacher(\ClassCover\SchoolBundle\Entity\RegisteredTeachers $teacher)
    {
        $this->teachers[] = $teacher;

        return $this;
    }

    /**
     * Remove teacher
     *
     * @param \ClassCover\SchoolBundle\Entity\RegisteredTeachers $teacher
     */
    public function removeTeacher(\ClassCover\SchoolBundle\Entity\RegisteredTeachers $teacher)
    {
        $this->teachers->removeElement($teacher);
    }

    /**
     * Get teachers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeachers()
    {
        return $this->teachers;
    }

    /**
     * Add booking
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherBooking $booking
     *
     * @return School
     */
    public function addBooking(\ClassCover\BookingBundle\Entity\TeacherBooking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherBooking $booking
     */
    public function removeBooking(\ClassCover\BookingBundle\Entity\TeacherBooking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * Add exclusion
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherExclusion $exclusion
     *
     * @return School
     */
    public function addExclusion(\ClassCover\BookingBundle\Entity\TeacherExclusion $exclusion)
    {
        $this->exclusions[] = $exclusion;

        return $this;
    }

    /**
     * Remove exclusion
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherExclusion $exclusion
     */
    public function removeExclusion(\ClassCover\BookingBundle\Entity\TeacherExclusion $exclusion)
    {
        $this->exclusions->removeElement($exclusion);
    }

    /**
     * Get exclusions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExclusions()
    {
        return $this->exclusions;
    }

    /**
     * Add shiftOffer
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherShiftOffer $shiftOffer
     *
     * @return School
     */
    public function addShiftOffer(\ClassCover\BookingBundle\Entity\TeacherShiftOffer $shiftOffer)
    {
        $this->shiftOffers[] = $shiftOffer;

        return $this;
    }

    /**
     * Remove shiftOffer
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherShiftOffer $shiftOffer
     */
    public function removeShiftOffer(\ClassCover\BookingBundle\Entity\TeacherShiftOffer $shiftOffer)
    {
        $this->shiftOffers->removeElement($shiftOffer);
    }

    /**
     * Get shiftOffers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShiftOffers()
    {
        return $this->shiftOffers;
    }

    /**
     * Add unavailableTeacher
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherUnavailability $unavailableTeacher
     *
     * @return School
     */
    public function addUnavailableTeacher(\ClassCover\BookingBundle\Entity\TeacherUnavailability $unavailableTeacher)
    {
        $this->unavailableTeachers[] = $unavailableTeacher;

        return $this;
    }

    /**
     * Remove unavailableTeacher
     *
     * @param \ClassCover\BookingBundle\Entity\TeacherUnavailability $unavailableTeacher
     */
    public function removeUnavailableTeacher(\ClassCover\BookingBundle\Entity\TeacherUnavailability $unavailableTeacher)
    {
        $this->unavailableTeachers->removeElement($unavailableTeacher);
    }

    /**
     * Get unavailableTeachers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUnavailableTeachers()
    {
        return $this->unavailableTeachers;
    }
}
