<?php

namespace ClassCover\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ClassCoverUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
