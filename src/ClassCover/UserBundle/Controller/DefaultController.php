<?php

namespace ClassCover\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ClassCoverUserBundle:Default:index.html.twig');
    }
}
