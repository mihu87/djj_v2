<?php

namespace ClassCover\AdminBundle\Controller;

use FOS\UserBundle\Model\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\SecurityExtraBundle\Annotation\Secure;
use ClassCover\SchoolBundle\Entity\School;
use ClassCover\SchoolBundle\Entity\SchoolRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use ClassCover\CyoBundle\Entity\Teacher;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ClassCoverAdminBundle:Default:index.html.twig');
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     */
    public function dashboardAction()
    {
        return $this->render('ClassCoverAdminBundle:Default:dashboard.html.twig');
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     */
    public function centresAction()
    {
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getEntityManager();

        $schools = $doctrine->getRepository('ClassCoverSchoolBundle:School')->findAll();

        return $this->render('ClassCoverAdminBundle:Default:centres.html.twig', ['schools' => $schools]);
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createCentreAction(Request $request)
    {

        if ($request->isMethod('post')) {

            $data = $request->request->all();

            $emailValidator = new Email();
            $emailValidator->strict = true;
            $emailValidator->checkMX = true;

            $constraints = new Collection([
                'centre_name' => [
                    new NotBlank()
                ],
                'centre_sms_name' => [
                    new NotBlank()
                ],
                'centre_email' => [
                    new NotBlank(),
                    $emailValidator     
                ],
                'centre_phone' => [
                    new NotBlank(),
                ]
            ]);

            $errors = [];
            /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
            $violations = $this->get('validator')->validateValue($data, $constraints);
            if (count($violations) !== 0) {
                foreach ($violations as $violation) {
                    /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
                    $errors[] = $violation->getPropertyPath() . ' : ' . $violation->getMessage();
                }
                return $this->render('ClassCoverAdminBundle:Default:create-centre.html.twig', ['errors' => $errors]);
            }

            /** @var PhoneNumberUtil $phoneUtil */
            $phoneUtil = $this->container->get('libphonenumber.phone_number_util');

            $pN = $phoneUtil->parse($data['centre_phone'], "AU");
            $i8n = $phoneUtil->format($pN, PhoneNumberFormat::INTERNATIONAL);
            $i8n = str_replace(" ", "", $i8n);

            $data['centre_phone'] = $i8n;

            /** @var SchoolService $schoolService */
            $schoolService = $this->get('school_service');

            $result = $schoolService->createSchool(
                $data['centre_name'],
                $data['centre_sms_name'],
                $data['centre_email'],
                $data['centre_phone']
            );

            if ($result) {
                return $this->redirectToRoute('class_cover_admin_school');
            } else {
                $errors = ['Internal error, contact system administrator.'];
                return $this->render('ClassCoverAdminBundle:Default:create-centre.html.twig', ['errors' => $errors]);
            }

        }

        return $this->render('ClassCoverAdminBundle:Default:create-centre.html.twig');
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     */
    public function casualsAction()
    {
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getEntityManager();

        $casuals = $doctrine->getRepository('ClassCoverCyoBundle:Teacher')->findAll();

        return $this->render('ClassCoverAdminBundle:Default:casuals.html.twig', ['casuals' => $casuals]);
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     */
    public function announcementsAction()
    {
        return $this->render('ClassCoverAdminBundle:Default:announcements.html.twig');
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     */
    public function usersAction()
    {
        return $this->render('ClassCoverAdminBundle:Default:users.html.twig');
    }

    /**
     * @param Request $request
     * @param $userId
     * @return RedirectResponse
     */
    public function loginAsAction(Request $request, $userId, $path = null)
    {
        $adminUser = $this->get('security.token_storage')->getToken()->getUser();
        $session = $session = $this->get('session');
        $session->set('adminUserId', $adminUser->getId());

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getEntityManager();

        /** @var UserManager $userManager */
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->findUserBy(['id' => $userId]);

        if (!$user) {

            throw new UsernameNotFoundException("User not found");

        } else {

            $token = new UsernamePasswordToken($user, null, "main", $user->getRoles());
            $this->get("security.token_storage")->setToken($token); //now the user is logged in
            //now dispatch the login event
            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
            $roles = $user->getRoles();

            if($user->hasRole('ROLE_ADMIN')) {

                return $this->redirectToRoute('class_cover_admin_homepage');

            } elseif ($user->hasRole('ROLE_SCHOOL')) {

                /** @var School $school */
                $school = $em->getRepository('ClassCoverSchoolBundle:School')->findOneBy(['user' => $user->getId()]);
                if ($school !== null) {
                    $session->set('school', $school);
                    $session->set('loggedUserName', $school->getName());
                }
                return $this->redirectToRoute('class_cover_school_homepage');

            } elseif($user->hasRole('ROLE_TEACHER')) {

                /** @var Teacher $teacher */
                $teacher = $em->getRepository('ClassCoverCyoBundle:Teacher')->findOneBy(['user' => $user->getId()]);
                if ($teacher !== null) {
                    $session->set('teacher', $teacher);
                    $session->set('loggedUserName', $teacher->getFirstName() . ' ' . $teacher->getSurname());
                }
                return $this->redirectToRoute('class_cover_teacher_homepage');

            }

            return $this->redirectToRoute('class_cover_admin_homepage');
        }
    }
}
