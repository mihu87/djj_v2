<?php

namespace ClassCover\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Announcement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCover\AdminBundle\Entity\AnnouncementRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Announcement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="type", type="string", columnDefinition="ENUM('all', 'teacher', 'school') NOT NULL default 'all'")
     */
    protected $type;

    /**
     * @ORM\Column(name="date_time_start", type="datetime", nullable=false)
     */
    protected $dateTimeStart;

    /**
     * @ORM\Column(name="date_time_end", type="datetime", nullable=false)
     */
    protected $dateTimeEnd;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $headLine;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $smsText;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdOn;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Announcement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set dateTimeStart
     *
     * @param \DateTime $dateTimeStart
     *
     * @return Announcement
     */
    public function setDateTimeStart($dateTimeStart)
    {
        $this->dateTimeStart = $dateTimeStart;

        return $this;
    }

    /**
     * Get dateTimeStart
     *
     * @return \DateTime
     */
    public function getDateTimeStart()
    {
        return $this->dateTimeStart;
    }

    /**
     * Set dateTimeEnd
     *
     * @param \DateTime $dateTimeEnd
     *
     * @return Announcement
     */
    public function setDateTimeEnd($dateTimeEnd)
    {
        $this->dateTimeEnd = $dateTimeEnd;

        return $this;
    }

    /**
     * Get dateTimeEnd
     *
     * @return \DateTime
     */
    public function getDateTimeEnd()
    {
        return $this->dateTimeEnd;
    }

    /**
     * Set smsText
     *
     * @param string $smsText
     *
     * @return Announcement
     */
    public function setSmsText($smsText)
    {
        $this->smsText = $smsText;

        return $this;
    }

    /**
     * Get smsText
     *
     * @return string
     */
    public function getSmsText()
    {
        return $this->smsText;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     *
     * @return Announcement
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedOnValue()
    {
        $this->createdOn = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function setypeValueIfUnknown()
    {
        if (empty($this->type)) {
            $this->type = 'all';
        }
    }

    /**
     * Set headLine
     *
     * @param string $headLine
     *
     * @return Announcement
     */
    public function setHeadLine($headLine)
    {
        $this->headLine = $headLine;

        return $this;
    }

    /**
     * Get headLine
     *
     * @return string
     */
    public function getHeadLine()
    {
        return $this->headLine;
    }
}
