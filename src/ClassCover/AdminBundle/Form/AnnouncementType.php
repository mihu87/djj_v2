<?php

namespace ClassCover\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AnnouncementType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', ['label' => 'Applies To', 'choices' => ['all' => "All", 'school' => "Centre", 'teacher' => "Casual"]])
            ->add('dateTimeStart', null, ['label' => 'Appearing From'])
            ->add('dateTimeEnd', null, ['label' => 'End'])
            ->add('headLine', null, ['label' => 'Name/Headline'])
            ->add('smsText', null, ['label' => 'Content'])
            //->add('createdOn')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ClassCover\AdminBundle\Entity\Announcement'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'classcover_adminbundle_announcement';
    }
}
