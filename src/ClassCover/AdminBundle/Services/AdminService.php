<?php
namespace ClassCover\AdminBundle\Services;

use ClassCover\AdminBundle\Entity\Admin;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use ClassCover\AppBundle\Entity\User;

use ClassCover\CyoBundle\Services\TeacherService;
use ClassCover\SchoolBundle\Services\SchoolService;

/**
 * Class AdminService
 * @package ClassCover\AdminBundle\Services
 */
class AdminService
{

    const DEFAULT_PASSWORD = 'qwerty';
    const DEFAULT_ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $firstName
     * @param $lastName
     * @param $email
     * @param $mobile
     * @return bool
     */
    public function createAdmin($firstName, $lastName, $email, $mobile)
    {
        $doctrine = $this->container->get('doctrine');

        /** @var EntityManager $em */
        $em = $doctrine->getEntityManager();

        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->createUser();

        $user->setUsername(strtolower($firstName . '.' . $lastName));
        $user->setEmail($email);
        $user->setPlainPassword(self::DEFAULT_PASSWORD);

        // Add all roles
        $user->addRole(TeacherService::DEFAULT_ROLE_TEACHER);
        $user->addRole(SchoolService::DEFAULT_ROLE_SCHOOL);
        $user->addRole(self::DEFAULT_ROLE_ADMIN);
        $user->setEnabled(true);

        $userManager->updateUser($user);

        $admin = new Admin();
        $admin->setFirstName($firstName);
        $admin->setSurname($lastName);
        $admin->setEmail($email);
        $admin->setPhone($mobile);
        $admin->setUser($user);

        $em->persist($admin);
        $em->flush();

        return true;
    }
}
