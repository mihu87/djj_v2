<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Vresh\TwilioBundle\VreshTwilioBundle(),
            new Jhg\NexmoBundle\JhgNexmoBundle(),
            new ClassCover\UserBundle\ClassCoverUserBundle(),
            new ClassCover\SchoolBundle\ClassCoverSchoolBundle(),
            new ClassCover\CyoBundle\ClassCoverCyoBundle(),
            new ClassCover\AdminBundle\ClassCoverAdminBundle(),
            new ClassCover\AppBundle\ClassCoverAppBundle(),
            new ClassCover\BookingBundle\ClassCoverBookingBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Misd\PhoneNumberBundle\MisdPhoneNumberBundle(),
            new FOS\UserBundle\FOSUserBundle()
        );

        if (in_array($this->getEnvironment(), ['dev', 'test', 'stage'])) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new CoreSphere\ConsoleBundle\CoreSphereConsoleBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        if ($this->getEnvironment() == "prod") {
            $loader->load($this->getRootDir().'/config/config.yml');
        } else {
            $loader->load($this->getRootDir().'/config/'.$this->getEnvironment().'/config.yml');
        }

    }
}
